## Installation:

Clone this repository to root directory of your local domain name. Let's say your local domain name is http://localhost/

In this repository you will find:

1. [database dump](https://bitbucket.org/knowledgecity/wd-testtask/src/fc169d9d96e0ba344216eab22090f807624ea901/sql/knowledgecity_testtask.sql?at=master) which you will need in order to work on the task
1. [API](https://bitbucket.org/knowledgecity/wd-testtask/src/fc169d9d96e0ba344216eab22090f807624ea901/api/?at=master). In this folder copy file *sample.override.config.php* to *override.config.php* and configure your database credential (see #1) that is all you need to make API work. API will be available at http://localhost/api/v2 If this url outputs the same result as https://api.knowledgecity.com/v2 that means you have configured the API properly.
1. [Front-end file](https://bitbucket.org/knowledgecity/wd-testtask/src/fc169d9d96e0ba344216eab22090f807624ea901/front-end/index.html?at=master&fileviewer=file-view-default) which makes request to the API, pulls data and shows the data on the page.

## The task:
As it is now the *index.html* file pulls list of all courses which are in the database and shows all courses with some data.

The task is to enhance the *index.html* file to let it output additional course data which it does not output now. 

The additional data which should appear on the page is: 

1. course description (db field `course_details`.`description`)
1. meta title (db field `course_details`.`meta_title`)
1. meta keywords (db field `course_details`.`meta_keywords`)
1. meta description (db field `course_details`.`meta_description`). 

In order to complete the task you will need to modify API resource as well as front-end JavaScript.

All code must be committed and pushed to your own branch (not master). To submit result of the task create a pull-request in bitbucket from your branch to master (do not merge it). 