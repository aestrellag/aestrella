<?php

/**
 * Global autoload module for external libs
 *
 */

set_include_path
(
    __DIR__.'/vendors'.PATH_SEPARATOR.
    get_include_path()
);

spl_autoload_register(function($class)
{
    $class = '/'.str_replace('\\', '/', $class).'.php';

    foreach(explode(PATH_SEPARATOR, get_include_path()) as $path)
    {
        if(is_file($path.$class))
        {
            include_once $path.$class;
            return;
        }
    }
});