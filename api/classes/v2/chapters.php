<?php

class chapters
{
    public $parent = null;

    private $dataTableName = 'course_chapters';

    function __construct()
    {
    }

    public function getCourseChaptersByID($courseID = '',$DATA)
    {
        try {
            // hook up needed classes ///////////////
            $this->parent->hookClass('v2/courses');//
            $this->parent->hookClass('v2/lessons');//
            $this->parent->hookClass('v2/mui');//
            /////////////////////////////////////

            $this->parent->EXTEND_FIELDS[] = 'details';
            $courseID = $this->parent->class->courses->getSingleCourseWithDetails($courseID);

            if(empty($courseID))
                throw new Exception(null,404);

            $lang = isset($DATA['lang']) ? $DATA['lang'] : $courseID['default_lang'];

            $condition['course_id'] = "'" . $courseID['course_ID'] . "'";

            $outputFields = array(
                '*' => array("view_order")
            );
            $extends = array('*' => '*');

            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));
            $orderCondition = $this->parent->orderCondition("view_order");
            $chapters = DataBase::select($this->dataTableName, $condition, $orderCondition);

            $chaptersGUID = [];
            $chaptersGUIDMUI = "";
            foreach ($chapters as $value){
                $chaptersGUID[] = $value['id'];
                $chaptersGUIDMUI .= ",".$value['id'];
            }


            $muiCollection = $this->parent->class->mui->getTranslationByCode(trim($chaptersGUIDMUI,","), $lang);
            $mui = [];
            foreach ($muiCollection as $value) {
                $mui[$value['key']] = $value['value'];
            }

            if (in_array("lessons", $this->parent->EXTEND_FIELDS)) {
                $lessonCollection = $this->parent->class->lessons->getLessonsByChapterCollection($courseID['course_ID'], $chaptersGUID);
                $_chapterLessons = [];
                foreach ($lessonCollection as $key => $value) {
                    $chapteGUID = $value['course_chapter_id'];
                    unset($value['course_chapter_id']);
                    $_chapterLessons[$chapteGUID][] = $value;
                }
            }

            foreach ($chapters as $key => $value){
                $chapters[$key]['title'] = $mui[$value['id']];
                $chapters[$key]['chapter_id'] = $value['id'];
                if (in_array("lessons", $this->parent->EXTEND_FIELDS)) {
                    $chapters[$key]['lessons'] = $_chapterLessons[$value['id']];
                }
            }

            return $chapters;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function getSingleCourseChapterByID($courseID = '',$chapterGUID = '', $DATA)
    {
        try {

            // hook up needed classes ///////////////
            $this->parent->hookClass('v2/courses');//
            $this->parent->hookClass('v2/mui');//
            /////////////////////////////////////////

            $this->parent->EXTEND_FIELDS[] = 'details';
            $courseID = $this->parent->class->courses->getSingleCourseWithDetails($courseID);


            $lang = isset($DATA['lang']) ? $DATA['lang'] : $courseID['default_lang'];
            $condition = array(
                'course_id' => 'cd.id',
                'ch.id' => "'" . $chapterGUID . "'",
                'cd.id' => "'".$courseID['course_ID']."'"
            );
            $outputFields = array(
                '*' => array("ch.view_order","ch.id")
            );
            $extends = array('*' => '*');
            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));
            $orderCondition = $this->parent->orderCondition("ch.view_order");

            $chapter = current(DataBase::select('courses cd, course_chapters ch', $condition, $orderCondition, false));

            if(empty($chapter))
                throw new Exception(null,404);

            $mui = current($this->parent->class->mui->getTranslationByCode($chapter['id'], $lang));
            $chapter['title'] = $mui['value'];

            if (in_array("lessons", $this->parent->EXTEND_FIELDS)) {
                $this->parent->hookClass('v2/lessons');
                $chapter['lessons'] = $this->parent->class->lessons->getLessonsByChapter($courseID['course_ID'], $chapter['id']);
            }

            return $chapter;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function createCourseNewChapter($courseID = '', $DATA = array())
    {
        try {
            // hook up needed classes ///////////////
            $this->parent->hookClass('v2/courses');//
            $this->parent->hookClass('v2/lessons');//
            $this->parent->hookClass('v2/mui');//
            /////////////////////////////////////////

            $this->parent->EXTEND_FIELDS[] = 'details';
            $courseID = $this->parent->class->courses->getSingleCourseWithDetails($courseID);
            $lang = isset($DATA['lang']) ? $DATA['lang'] : $courseID['default_lang'];

            if(empty($courseID))
                throw new Exception(null,404);

            if(empty($DATA['title']))
                throw new Exception("Field 'title' is required.",412);

            $chatpterGUID = UserHelper::CreateGUID();
            $acceptPostFields = array(
                "*id" => $chatpterGUID,
                "*course_id" => $courseID['course_ID'],
                "*view_order" => (int)$DATA['view_order'],
            );

            $Fields = $this->parent->preparePostFields($acceptPostFields);
            if (!$Fields) {
                return null;
            }

            $insert_id = DataBase::insert($this->dataTableName, $Fields);

            if (!$insert_id) {
                throw new Exception(null, 500);
            }

            foreach ($courseID['available_langs'] as $_lang) {
                $this->parent->class->mui->setTranslation($chatpterGUID, trim($DATA['title']), $_lang);
            }

            return $chatpterGUID;

        } catch (Exception $e) {

            if (!is_null($e->getMessage())) {
                $this->parent->RESPONSE = array("message" => $e->getMessage());
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function updateSingleCourseChapterByID($courseID = '', $chapterGUID = '', $DATA = array())
    {
        try {
            // hook up needed classes ///////////////
            $this->parent->hookClass('v2/courses');//
            $this->parent->hookClass('v2/mui');//
            /////////////////////////////////////////

            $this->parent->EXTEND_FIELDS[] = 'details';
            $courseID = $this->parent->class->courses->getSingleCourseWithDetails($courseID);
            $lang = isset($DATA['lang']) ? $DATA['lang'] : $courseID['default_lang'];

            if(empty($courseID))
                throw new Exception(null,404);

            $acceptPostFields = array();
            $acceptPostFields['course_id'] = $courseID['course_ID'];
                        
            if (isset($DATA['view_order'])) {
                $acceptPostFields['view_order'] = (int)$DATA['view_order'];
            }

            if (isset($DATA['title'])) {
                $title = $this->parent->class->mui->updateTranslationByKey($chapterGUID, trim($DATA['title']), $lang);
            }

            $Fields = $this->parent->preparePostFields($acceptPostFields);
            if (!$Fields) {
                if (!$title)
                    return true;
                throw new Exception("Unknown error occured.",500);
            }

            $success = DataBase::update($this->dataTableName, $Fields," `id`='" . $chapterGUID . "' && `course_id`= '" .$courseID['course_ID'] ."'");
            if (!$success)
                throw new Exception("Unknown error occured.", 500);

            return true;
        } catch (Exception $e) {

            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return false;
        }


    }

    public function deleteSingleCourseChapterByID($courseID = '',$chapterGUID = '')
    {
        try {

            // hook up needed classes ///////////////
            $this->parent->hookClass('v2/courses');//
            $this->parent->hookClass('v2/lessons');//
            $this->parent->hookClass('v2/mui');//
            /////////////////////////////////////////

            $this->parent->EXTEND_FIELDS[] = 'details';
            $courseID = $this->parent->class->courses->getSingleCourseWithDetails($courseID);

            if(empty($courseID))
                throw new Exception(null,404);

            $this->parent->class->lessons->_deleteLessonsByChapter($courseID['course_ID'],$chapterGUID);
            $this->parent->class->mui->deleteTranslation($chapterGUID);
            $deleted = DataBase::delete($this->dataTableName, " `id`='" . $chapterGUID . "' && `course_id`= '" . $courseID['course_ID'] ."'");
            return $deleted;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }


    public function saveAllChaptersbyCourseID($courseID = '', $DATA){
    try {

            // hook up needed classes //////////////////////
            $this->parent->hookClass('v2/courses');       //
            $this->parent->hookClass('v2/lessons');       //
            $this->parent->hookClass('v2/portalsCourses');//
            ////////////////////////////////////////////////

            $this->parent->EXTEND_FIELDS[] = 'details';
            $courseID = $this->parent->class->courses->getSingleCourseWithDetails($courseID);

            if(empty($courseID)) {
                throw new Exception(null,404);
	        }

	        $courseID        = $courseID['course_ID'];

            $data            = json_decode($DATA['data'], true);

            if( empty($data) ) {
		        throw new Exception('Data is empty', 405);
	        }

            $deletedLesson                    = [];

            foreach ($data as $ChapterKey => $ChapterValue){

                //add new chapter and lessons
                if(filter_var($ChapterValue['new'], FILTER_VALIDATE_BOOLEAN)){

		            $new_chapterGUID =  $this->parent->class->chapters->createCourseNewChapter($courseID, $ChapterValue);

		            foreach($ChapterValue['lessons'] as $lesson){
                        $this->parent->class->lessons->createChapterLesson($courseID, $new_chapterGUID, $lesson);
                    }
                    continue;
                }

                //delete chapter and lessons
                if(filter_var($ChapterValue['delete'], FILTER_VALIDATE_BOOLEAN) && UserHelper::ValidGUID($ChapterValue['id'])){
                    $this->parent->class->lessons->_deleteLessonsByChapter($courseID, $ChapterValue['id']);
                    $this->parent->class->chapters->deleteSingleCourseChapterByID($courseID, $ChapterValue['id']);

                    foreach ($ChapterValue['lessons'] as $lessonToDelete) {
                        $deletedLesson[]  = $lessonToDelete['id'];
                    }
                    continue;


                //update chapter and lesson
                } elseif ( UserHelper::ValidGUID($ChapterValue['id']) ){

		             $this->parent->class->chapters->updateSingleCourseChapterByID($courseID, $ChapterValue['id'], $ChapterValue);

                    foreach($ChapterValue['lessons'] as $lesson){
			            //add new lesson
			            if(filter_var($lesson['new'], FILTER_VALIDATE_BOOLEAN)){
			                $this->parent->class->lessons->createChapterLesson($courseID, $ChapterValue['id'], $lesson);
			            }

			            //delete lesson
			            if(filter_var($lesson['delete'], FILTER_VALIDATE_BOOLEAN)){
			                $this->parent->class->lessons->deleteLessonByID($ChapterValue['id'],$lesson['id']);
                            $deletedLesson[]          = $lesson['id'];
			            }

			            //update  lesson
                        $tmp = $this->parent->class->lessons->updateLessonByID($courseID, $ChapterValue['id'],$lesson['id'], $lesson);
                    }
                }

                $lang   = $ChapterValue['lang'];
            }

            $this->parent->class->portalsCourses->syncContentEnumerationTable('lesson');

            //statisticsAdjustment
            $this->parent->class->chapters->deleteLessonsProgress($courseID, $deletedLesson);

            $this->parent->class->courses->updateSingleCourseByID($courseID, array('is_published_course' => 0));

            $this->parent->class->courses->updateSingleCourseDetailByLang($courseID,$lang, ['publish_date'=> time()]);

            return true;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

  
    public function deleteLessonsProgress($courseID, $lessons){

        // hook up needed classes /////////////////////
        $this->parent->hookClass('v2/studentsStats');//
        ///////////////////////////////////////////////

        try{

            $deleted = DataBase::delete('`course_lessons_progress`', " `course_id`='" . $courseID . "' && `lesson_id` IN ('" . implode("','", $lessons) ."')");

            $query = "SELECT `student_id`  FROM `course_general_stat` 
                      WHERE `course_id` LIKE '".$courseID."' AND watchedAll_date IS NULL 
                      AND reset_date is NULL GROUP BY student_id";

            if(!DataBase::getInstance()->exec_query($query)){
                throw new Exception('DataBase error', 500);
            }

            $students       = DataBase::getInstance()->fetchAll();

            $updated        = [];

            foreach ($students as $student) {

                 $updated[]     = $this->parent->class->studentsStats->updateCourseGeneralStats($student['student_id'], $courseID);

            }

            return ['updated' => $updated];

        } catch (Exception $e) {

            return array(
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            );
        }

    }



}