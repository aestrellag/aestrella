<?php

class courses
{
    var $allCourseLessons = null;

    public $parent = null;

    private $dataTableName = 'courses';

    function __construct(){}

    public function getList(array $filters = []): array
    {
        $availableFilters           = ['id', 'course_format', 'default_lang', 'is_promoted', 'is_discontinued', 'is_published'];
        $availableSorting           = ['username', 'last_name'];

        $condition                  = [];

        foreach($filters as $key => $filter)
        {
            if(in_array($key, $availableFilters))
            {
                if(is_array($filter))
                {
                    $values         = [];

                    foreach($filter as $value)
                    {
                        $values[]   = DataBase::getInstance()->quote($value);
                    }

                    if(!empty($values))
                    {
                        $condition[$key] = "c.`$key` IN (".implode(',', $values).")";
                    }
                }
                else
                {
                    $condition[$key] = "c.`$key`=".DataBase::getInstance()->quote($filter);
                }
            }
        }

        if(isset($filters['search']))
        {
            $search                 = addslashes($filters['search']);
            $search_fields          = ['c.id', 'cd.title', 'cd.description', 'cd.city', 'cd.address'];

            $or                     = [];

            foreach($search_fields as $field)
            {
                $or[]               = "$field LIKE '%$search%'";
            }

            $condition['search']    = '('.implode(' OR ', $or).')';
        }

        $categories                 = '';
        $categories_joins           = '';
        $group_by                   = '';
        $lang = ($filters['lang'] ? $filters['lang'] : 'en');
            
        // filter by portal
        if($filters['portal_id'])
        {
            $portal_id              = DataBase::getInstance()->quote($filters['portal_id']);
            /*
            $subquery               = "SELECT pcc.course_id FROM portal_categories_courses as pcc
INNER JOIN portal_categories as pc ON (pc.id = pcc.portalCategory_id AND pc.portal_id = $portal_id)";
            $condition['portal_id'] = "c.id IN ($subquery)";
            */
            $categories_joins       = "INNER JOIN portal_categories_courses as pcc ON (pcc.course_id = c.id)
INNER JOIN portal_categories as pc ON (pc.id = pcc.portalCategory_id AND pc.portal_id = $portal_id)
INNER JOIN portal_categories_data as pcd ON(pcd.portalCategory_id = pc.id AND pcd.lang = '".$lang."')
";
            $categories             = 'pcc.portalCategory_id, pcd.title as category';
            $group_by               = 'GROUP BY c.id';
        }

        $sorting                    = '';

        if(!empty($this->parent->ORDER_CONDITION['field']))
        {
            if($this->parent->ORDER_CONDITION['field'] === 'name')
            {
                $this->parent->ORDER_CONDITION['field'] = 'CONCAT(`first_name`,`last_name`)';
                $sorting            = ' ORDER BY '.$this->parent->ORDER_CONDITION['field']. ' '.$this->parent->ORDER_CONDITION['direction'];
            }
            else
            {
                $sorting            = ' ORDER BY `'.$this->parent->ORDER_CONDITION['field']. '` '.$this->parent->ORDER_CONDITION['direction'];
            }
        }

        $limit                      = '';
        $sql_calc                   = '';

        if($this->parent->LIMIT > 0)
        {
            $limit                  = [0, $this->parent->LIMIT];

            if($this->parent->START !== null)
            {
                $limit[0]           = $this->parent->START;
            }

            $limit                  = "LIMIT {$limit[0]}, {$limit[1]}";
            $sql_calc               = 'SQL_CALC_FOUND_ROWS';
        }

        $condition                  = empty($condition) ? '' : 'WHERE '.implode(' AND ', $condition);

        $query                      = "SELECT $sql_calc cd.*, c.*, $categories FROM `courses` as c
LEFT JOIN course_details as cd ON(cd.course_id = c.id AND cd.lang = '".$lang."') 
$categories_joins
$condition
$group_by       
$sorting $limit
";

        if(!DataBase::getInstance()->exec_query($query))
        {
            throw new Exception('DataBase Server error', 500);
        }

        $results                    = DataBase::getInstance()->fetchAll();

        if(!is_array($results))
        {
            throw new Exception('DataBase error', 500);
        }

        $total_count                = null;

        if(!empty($sql_calc))
        {
            $total_count            = DataBase::getTotalCount();
        }

        $this->parent->ORDER_CONDITION = null;

        // save start and limit
        $start                      = $this->parent->START;
        $limit                      = $this->parent->LIMIT;

        $this->parent->START        = null;
        $this->parent->LIMIT        = null;

        foreach($results as &$course)
        {
            // todo
        }

        $this->parent->START    = $start;
        $this->parent->LIMIT    = $limit;
        $this->parent->TOTAL    = $total_count;

        return $results;
    }

    public function getAllCourses()
    {
        // hook up needed classes ////////////////
        $this->parent->hookClass('v2/teachers');//
        //////////////////////////////////////////

        try {

            $condition              = array();

            if (isset($this->parent->DATA['is_promoted']))
            {
                $condition['is_promoted'] = (int)filter_var($this->parent->DATA['is_promoted'],FILTER_VALIDATE_BOOLEAN);
            }

            if (isset($this->parent->DATA['is_discontinued']))
            {
                $condition['is_discontinued'] = (int)filter_var($this->parent->DATA['is_discontinued'],FILTER_VALIDATE_BOOLEAN);
            } else {
                $condition['is_discontinued'] = 0;
            }

            foreach($condition as $key => &$item)
            {
                $item                   = '`'.$key.'` = '.$item;
            }

            $condition                  = empty($condition) ? '' : 'WHERE '.implode(' AND ', $condition);

            $lesson_count               = '';

            if (in_array("no_of_lessons", $this->parent->EXTEND_FIELDS))
            {
                $lesson_count =
                    ', (SELECT COUNT(l.`id`)
                FROM `course_chapter_lessons` as l
                INNER JOIN `course_chapters` as ch ON (ch.id = l.course_chapter_id)
                WHERE `course_id` = c.id) as `no_of_lessons`';
            }

            $query                  = "SELECT c.* $lesson_count FROM courses as c $condition";

            if(!DataBase::getInstance()->exec_query($query))
            {
                throw new Exception('Database exception', 500);
            }

            $courseObj              = DataBase::getInstance()->fetchAll();

            $teacher                = $this->parent->class->teachers->getTeachersByCourses($courseObj);
            $details                = $this->getAllDetailsByCourse($courseObj);

            $result = array();
            foreach ($courseObj as $course) {

                $resDefault = array(
		    "is_published" => $course['is_published'],
                    "course_ID" => $course['id'],
                    "location" => sprintf("%s://%s/v2/courses/%s", PROTOCOL, HTTP_HOST, UrlParser::makeID($course['id'])),
                    "course_name" => array( "default_lang" => $details[$course['id']][$course['default_lang']]['title'],"en" => $details[$course['id']]['en']['title'] ),
                    "course_description" => array( "default_lang" => $details[$course['id']][$course['default_lang']]['description'],"en" => $details[$course['id']]['en']['description'] ),
                    "meta_title" => array( "default_lang" => $details[$course['id']][$course['default_lang']]['meta_title'],"en" => $details[$course['id']]['en']['meta_title'] ),
                    "meta_keywords" => array( "default_lang" => $details[$course['id']][$course['default_lang']]['meta_keywords'],"en" => $details[$course['id']]['en']['meta_keywords'] ),
                     "meta_description" => array( "default_lang" => $details[$course['id']][$course['default_lang']]['meta_description'],"en" => $details[$course['id']]['en']['meta_description'] ),

                    "available_langs" => array_keys($details[$course['id']])
                );

                $author = array();
                if (in_array("author", $this->parent->EXTEND_FIELDS)) {
                    $author["author"] = $teacher[$course['id']];
                }

                $detailsExtend = array();
                if (in_array("details", $this->parent->EXTEND_FIELDS)) {
                    $detailsExtend['details'] = $details[$course['id']];
                }

                unset($course['id']);
                if(!is_array($resDefault)) $resDefault = array();
                if(!is_array($course)) $course = array();
                if(!is_array($author)) $author = array();
                if(!is_array($detailsExtend)) $detailsExtend = array();
                if(!is_array($intro)) $intro = array();
                if(!is_array($runtime)) $runtime = array();
                $result[] = array_merge($resDefault, $course, $author, $detailsExtend, $intro, $runtime);
            }

            return $result;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }

    }

    public function getAllCoursesByLang($data)
    {
	try {
        // hook up needed classes ////////////////
        $this->parent->hookClass('helper');     //
        $this->parent->hookClass('v2/teachers');//
        //////////////////////////////////////////

        if (empty($data) || !$data['lang'] ) {
		 throw new Exception(null,404);
	    }

// 	    $DATAlang = $this->parent->class->languages->getLangbyCodeISO6391($data['lang']);
//
//             if(empty($DATAlang))
//                 throw new Exception(null, 404);


	     DataBase::getInstance()->exec_query("
		SELECT `c`.* 
		FROM `courses` as `c` , `course_details` as `cd` 
		WHERE `cd`.`lang` = '".$data['lang']."' 
		AND `c`.`id` = `cd`.`course_id`");

	    $courseObj= DataBase::getInstance()->fetchAll();

	    $details = $this->getAllDetailsByCourse($courseObj);

        
        $teacher                = $this->parent->class->teachers->getTeachersByCourses($courseObj, false, $data['lang'] );
        

        $coursesIntro           = $this->getCoursesIntro();
        $coursesRuntime         = $this->getCoursesTrt($data['lang'], 'hours');

        $allCourseIDs        = [];
        foreach ($courseObj as $item) {
            $allCourseIDs[]  = $item['id'];
        }

        $courseCEIDs         = $this->parent->class->helper->getContentEnumerationIDs($allCourseIDs);

	    $result = array();
            foreach ($courseObj as $course) {

                $resDefault = array(
                    "course_ID" => $course['id'],
                    "title"     => $details[$course['id']][$data['lang']]['title'],    
                    "course_name" => array( "default_lang" => $details[$course['id']][$course['default_lang']]['title'],"en" => $details[$course['id']]['en']['title'] ),
                    "available_langs" => array_keys($details[$course['id']])
                );

                $course['ceid']     = $courseCEIDs[$course['id']];


                $detailsExtend = array();
                if (in_array("details", $this->parent->EXTEND_FIELDS)) {
                    $detailsExtend['details'] = $details[$course['id']];
                }

                $author = array();
                if (in_array("author", $this->parent->EXTEND_FIELDS)) {
                    $author["author"] = $teacher[$course['id']];
                }

                $intro = array();
                if (in_array("intro", $this->parent->EXTEND_FIELDS)) {
                    $intro['intro'] = $coursesIntro[$course['id']];
                }

                $runtime = array();
                if (in_array("trt", $this->parent->EXTEND_FIELDS)) {
                    $runtime['trt'] = $coursesRuntime[$course['id']];
                }

                unset($course['id']);
                $result[] = array_merge($resDefault, $course, $detailsExtend, $author, $intro, $runtime);
            }

            return $result;


	} catch (Exception $e) {
            $this->parent->outException($e);
        }
    }

    public function getCourseLessons($courseID)
    {
        try {
            if(empty($courseID))
            {
                throw new Exception(null,404);
            }


            /*
                SELECT l.* FROM `course_chapter_lessons` as l
                INNER JOIN `course_chapters` as ch ON (ch.id = l.course_chapter_id)
                INNER JOIN `courses` as c ON (c.id = ch.course_id)
                WHERE c.id='BUS1061'
                ORDER BY ch.view_order,l.view_order
             */

            DataBase::getInstance()->exec_query('SELECT l.* FROM `course_chapter_lessons` as l
                INNER JOIN `course_chapters` as ch ON (ch.id = l.course_chapter_id)
                INNER JOIN `courses` as c ON (c.id = ch.course_id)
                WHERE c.id='.DataBase::getInstance()->quote($courseID).'
                ORDER BY ch.view_order, l.view_order');

            return DataBase::getInstance()->fetchAll();

        } catch (Exception $e) {
            $this->parent->outException($e);
        }
    }

    public function getSingleCourseWithDetails($courseID = '', $showGUID = false, $data = [])
    {
        // hook up needed classes ////////////////
        $this->parent->hookClass('v2/teachers');//
        //////////////////////////////////////////

        try {

            $orderCondition = $this->parent->orderCondition();
            $outputFields = array(
                '*' => array(
                    'id',
                    'default_lang',
                    'course_format'
                )
            );
            $extends = array('*' => array('is_promoted', 'is_discontinued', 'promote_text'));

            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));

            $condition['id'] = "'" . $courseID . "'";
            if (isset($this->parent->DATA['is_promoted']))
                $condition['is_promoted'] = (int)filter_var($this->parent->DATA['is_promoted'],FILTER_VALIDATE_BOOLEAN);
            if (isset($this->parent->DATA['is_discontinued'])){
                $condition['is_discontinued'] = (int)filter_var($this->parent->DATA['is_discontinued'],FILTER_VALIDATE_BOOLEAN);
            } else {
                $condition['is_discontinued'] = 0;
            }

            $courseObj = DataBase::select($this->dataTableName, $condition, $orderCondition);

            if(empty($courseObj))
                throw new Exception(null,404);

            $showAll            = (in_array("published", $this->parent->EXTEND_FIELDS)) ? false : true;
            $details            = $this->getAllDetailsByCourse($courseObj, $showAll);

            $result = array();
            foreach ($courseObj as $course) {

                $lang = isset($this->parent->DATA["lang"]) ? $this->parent->DATA["lang"] : $course['default_lang'];
                $chapters = $this->getChaptersByCourseID($course['id']);
                $chaptersID = array();
                foreach ($chapters as $ch) {
                    $chaptersID[] = $ch['id'];
                }
                $lessonsObj = $this->getLessonsByChapterID($chaptersID);

                $resDefault = array(
                    "course_ID" => $course['id'],
                    "course_name" => array( "default_lang" => $details[$course['id']][$course['default_lang']]['title'],"en" => $details[$course['id']]['en']['title'] ),
                    "available_langs" => array_keys($details[$course['id']])

                );

                $author = array();
                if (in_array("author", $this->parent->EXTEND_FIELDS)) {
                    $teacher = $this->parent->class->teachers->getTeachersByCourses($courseObj);
                    $author["author"] = $teacher[$course['id']];
                }

                $no_of_lessons = array();
                if (in_array("no_of_lessons", $this->parent->EXTEND_FIELDS)) {
                    $no_of_lessons['no_of_lessons'] = count($lessonsObj);
                }

                $detailsExtend = array();
                if (in_array("details", $this->parent->EXTEND_FIELDS)) {
                    $detailsExtend['details'] = $details[$course['id']];
                }

                if(isset($data['portal_id']))
                {
                    $this->parent->hookClass('v2/portals');
                    $info           = $this->parent->class->portals->getCourse($data['portal_id'], $course['id'], $lang);
                    $resDefault['portalCategory_id'] = $info['portalCategory_id'];
                }

                if(!$showGUID)
                    unset($course['id']);

                $result[] = array_merge($resDefault, $course, $no_of_lessons, $author, $detailsExtend);
            }

            return current($result);

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function getSingleCourseByLang($courseID = '', $langCODE = '', $DATA = array(), $showGUID = false)
    {
        // hook up needed classes ////////////////
        $this->parent->hookClass('v2/teachers');//
        //////////////////////////////////////////

        try {

            $orderCondition = $this->parent->orderCondition();
            $outputFields = array(
                '*' => array(
                    'id',
                    'default_lang',
                    'course_format'
                )
            );
            $extends = array('*' => array('is_promoted', 'is_discontinued', 'promote_text', 'is_published'));

            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));

            $condition['id'] = "'" . $courseID . "'";
            if (isset($DATA['is_promoted']))
                $condition['is_promoted'] = (int)filter_var($DATA['is_promoted'],FILTER_VALIDATE_BOOLEAN);
            if (isset($DATA['is_discontinued'])){
                $condition['is_discontinued'] = (int)filter_var($DATA['is_discontinued'],FILTER_VALIDATE_BOOLEAN);
            } else {
                $condition['is_discontinued'] = 0;
            }

            $course = current(DataBase::select($this->dataTableName, $condition, $orderCondition));

            if(empty($course))
                throw new Exception(null,404);

            $details = $this->getSingleDetailsByCourseLang($course,$langCODE);

            if(empty($details))
                throw new Exception(null,404);

            $result = array();

            $chapters = $this->getChaptersByCourseID($course['id']);
            $chaptersID = array();
            foreach ($chapters as $ch) {
                $chaptersID[] = $ch['id'];
            }
            $lessonsObj = $this->getLessonsByChapterID($chaptersID);

            $resDefault = array(
                "course_ID" => $course['id'],
                "course_name" => $details['title'],
                "is_published" => $course['is_published']
            );

            $author = array();
            if (in_array("author", $this->parent->EXTEND_FIELDS)) {
                $teacher = $this->parent->class->teachers->getTeachersByCourses(array($course), false, $langCODE);
                $author["author"] = $teacher[$course['id']];
            }

            $no_of_lessons = array();
            if (in_array("no_of_lessons", $this->parent->EXTEND_FIELDS)) {
                $no_of_lessons['no_of_lessons'] = count($lessonsObj);
            }

            $detailsExtend = array();
            if (in_array("details", $this->parent->EXTEND_FIELDS)) {
                $detailsExtend['details'][$langCODE] = $details;
            }

            if (in_array("eBooks", $this->parent->EXTEND_FIELDS)) {
                $detailsExtend['eBooks'][$langCODE] = $this->getCourseEBook($course['id'], $langCODE);
            }

            if (in_array("available_langs", $this->parent->EXTEND_FIELDS)) {
                $detailsExtend['available_langs'] = $this->getCourseAvailableLangs($course['id']);
            }

            if(!$showGUID)
                unset($course['id']);

            $result[] = array_merge($resDefault, $course, $no_of_lessons, $author, $detailsExtend);


            return current($result);

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function getSingleCourse($courseID = '')
    {
        try {

            $orderCondition = $this->parent->orderCondition();
            $condition['id'] = "'" . $courseID . "'";

            $course = current(DataBase::select($this->dataTableName, $condition, $orderCondition));

            if(empty($course))
                throw new Exception(null,404);

            return $course;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function createSingleCourseByID($courseID = '', $DATA = array())
    {
        try {

            // hook up needed classes //////////////////
            $this->parent->hookClass('v2/languages'); //
            $this->parent->hookClass('v2/courseQuiz');//
            ////////////////////////////////////////////

            $courseObj = $this->getSingleCourseWithDetails($courseID);

            if(!empty($courseObj))
                throw new Exception(sprintf("Course %s already exists", $courseID),409);

            $DATAlang = $this->parent->class->languages->getLangbyCodeISO6391($DATA['default_lang']);
            $lang = empty($DATAlang) ? "en" : $DATA['default_lang'];

            $acceptPostFields = array(
                "*id" => $courseID,
                "*default_lang" =>  $lang,
                "is_promoted" => (int)filter_var($DATA['is_promoted'], FILTER_VALIDATE_BOOLEAN),
                "is_discontinued" => (int)filter_var($DATA['is_discontinued'], FILTER_VALIDATE_BOOLEAN),
                "promote_text" => $DATA['promote_text'],
                "course_format" => in_array($DATA['course_format'],['lessons','scorm','class']) ? $DATA['course_format'] : 'lessons',
                "owner_account_id"         => $DATA['owner_account_id'] ? $DATA['owner_account_id'] : null
            );

            $Fields = $this->parent->preparePostFields($acceptPostFields);
            if (!$Fields) {
                return null;
            }

            $insert_id = DataBase::insert($this->dataTableName, $Fields);

            if (!$insert_id) {
                throw new Exception(null, 500);
            }

            return $courseID;

        } catch (Exception $e) {

            if (!is_null($e->getMessage())) {
                $this->parent->RESPONSE = array("message" => $e->getMessage());
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function createSingleCourseDetailByLang($courseID = '', $langCODE = '', $DATA = array())
    {
        try {

            // hook up needed classes //////////////////
            $this->parent->hookClass('v2/languages');///
            $this->parent->hookClass('v2/courseQuiz');//
            $this->parent->hookClass('v2/lessons');//
            $this->parent->hookClass('v2/portalsCourses');//
            ////////////////////////////////////////////

            $courseDetailsObj = $this->getSingleCourseByLang($courseID, $langCODE);

            if(!empty($courseDetailsObj))
                throw new Exception(sprintf("Course %s with %s language already exists", $courseID, $langCODE),409);

            $DATAlang = $this->parent->class->languages->getLangbyCodeISO6391($langCODE);
            if(empty($DATAlang))
                throw new Exception(null, 404);

            if(empty($DATA['title']))
            {
                $DATA['title'] = '';
            }

            $acceptPostFields = array(
                "*id" => UserHelper::CreateGUID(),
                "*course_id"        => $courseID,
                "*lang"             => $langCODE,
                "*title"            => (string) trim($DATA['title']),
                "tagline"           => (string) trim($DATA['tagline']),
                "description"       => (string) trim($DATA['description']),
                "has_manual"        => (int)filter_var($DATA['has_manual'], FILTER_VALIDATE_BOOLEAN),
                "meta_title"        => (string) trim($DATA['meta_title']),
                "meta_description"  => (string) trim($DATA['meta_description']),
                "meta_keywords"     => (string) trim($DATA['meta_keywords']),
                "publish_date"      => isset($DATA['publish_date']) ? (int)$DATA['publish_date'] : time() ,
                "has_vo"            => (int)filter_var($DATA['has_vo'], FILTER_VALIDATE_BOOLEAN),
                "has_subs"          => (int)filter_var($DATA['has_subs'], FILTER_VALIDATE_BOOLEAN),
                "subs_edited"       => (int)filter_var($DATA['subs_edited'], FILTER_VALIDATE_BOOLEAN),

                'date'              => $DATA['date'] ?? null,
                'time'              => $DATA['time'] ?? null,
                'city'              => $DATA['city'] ?? null,
                'address'           => $DATA['address'] ?? null,
                'class_capacity'    => $DATA['class_capacity'] ?? null,
                'properties'        => $DATA['properties'] ? json_encode($DATA['properties']) : null
            );

            $Fields = $this->parent->preparePostFields($acceptPostFields);
            if (!$Fields) {
                throw new Exception(null, 412);
            }

            $insert_id = DataBase::insert("`course_details`", $Fields);

            // if defined portal_id and course_format = class
            if(array_key_exists('portal_id', $DATA))
            {
                $this->addClassToPortal($courseID, $DATA['portal_id'], $langCODE, $DATA['portalCategory_id'] ?? null);
            }

            $this->parent->class->courseQuiz->_createCourseNewQuizGUID($courseID,$langCODE);

            if (!$insert_id) {
                throw new Exception(null, 500);
            }

            if(isset($DATA['eBooks']) && count($DATA['eBooks'])){
                foreach ($DATA['eBooks'] as $eBook){
                    $eBook['lang']      = $langCODE;
                    $this->updateCourseEBook($eBook['id'], $insert_id, $eBook);
                }
            }

            $this->parent->class->lessons->copyLessons($courseID, $langCODE);

            $this->parent->class->portalsCourses->syncContentEnumerationTable('quiz');

            return $langCODE;

        } catch (Exception $e) {

            if (!is_null($e->getMessage())) {
                $this->parent->RESPONSE = array("message" => $e->getMessage());
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function createCourseWithDetails($DATA = array())
    {
        try {

            // clone block
            if(!empty($DATA['clone_id']))
            {
                return $this->cloneCourse($DATA);
            }

            // hook up needed classes //////////////////////
            $this->parent->hookClass('v2/teachers');      //
            $this->parent->hookClass('v2/portalsCourses');//
            ////////////////////////////////////////////////

            $acceptPostFields = array(
                "*id" => (string)$DATA['course_id'],
                "*default_lang" =>  (string)$DATA['default_lang'],
                "*lang" => (string)$DATA['lang'],
                "*title" => (string)$DATA['title'],
            );

            $Fields = $this->parent->preparePostFields($acceptPostFields);
            if (!$Fields) {
                return null;
            }

            $courseObj = $this->createSingleCourseByID($DATA['course_id'],$DATA);

            if(empty($courseObj))
                throw new Exception(sprintf("Course %s already exists", $DATA['course_id']),409);

            if(isset($DATA['author'])){
              $this->parent->class->teachers->createTeacherCourseRel($DATA['author'],$DATA['course_id']);
            }

            $courseDetailsObj = $this->createSingleCourseDetailByLang($DATA['course_id'], $DATA['lang'], $DATA);

            // add class to portal if portal id defined
            /*
            if(isset($DATA['portal_id']) && $DATA['course_format'] === 'class')
            {
                $this->addClassToPortal($courseObj, $DATA['portal_id'], $DATA['lang'], $DATA['portalCategory_id'] ?? null);
            }
            */

            if(empty($courseDetailsObj))
                return array( "id" => $DATA['course_id'],"lang" => null );

            $this->parent->class->portalsCourses->syncContentEnumerationTable('course');

            return array( "id" => $DATA['course_id'],"lang" => $DATA['lang'] );

        } catch (Exception $e) {

            if (!is_null($e->getMessage())) {
                $this->parent->RESPONSE = array("message" => $e->getMessage());
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function cloneCourse(array $DATA)
    {
        $this->parent->hookClass('v2/teachers');
        $this->parent->hookClass('v2/portalsCourses');

        // get clone data
        $clone_id           = DataBase::getInstance()->quote($DATA['clone_id']);

        // 1. Get exists course
        $course             = current(DataBase::select($this->dataTableName, ['id' => $clone_id]));

        if(empty($course))
        {
            throw new Exception("Course id $clone_id is not found", 404);
        }

        // 2. Generate new name for new course
        // try to find others courses with this prefix
        // (delete "-")
        $prefix             = $DATA['clone_id'];
        $len                = strlen($prefix) + 1;
        $query              = "SELECT MAX(CAST(SUBSTR(`id`, $len) as UNSIGNED)) as `count` FROM `{$this->dataTableName}` WHERE `id` LIKE '$prefix%'";

        if(!DataBase::getInstance()->exec_query($query))
        {
            throw new Exception('DataBase error', 500);
        }

        $results            = DataBase::getInstance()->fetchAll();

        if(empty($results))
        {
            $results        = 1;
        }
        else
        {
            $results        = intval($results[0]['count']) + 1;
        }

        if($results < 10)
        {
            $results        = '0'.strval($results);
        }
        else
        {
            $results        = strval($results);
        }

        $course_id          = $prefix . $results;
        unset($course['id']);

        // 3. Clone data
        $this->createSingleCourseByID($course_id, $course);

        $details            = $this->getAllDetailsByCourse([['id' => $DATA['clone_id']]]);

        $details            = reset($details);

        // get portalCategory_id
        $category           = DataBase::select('portal_categories_courses', ['course_id' => $clone_id]);

        $portalCategory_id  = null;

        if(!empty($category))
        {
            $portalCategory_id = $category[0]['portalCategory_id'];
        }

        foreach($details as $lang => $detail)
        {
            if(!empty($DATA['portal_id']))
            {
                $detail['portal_id'] = $DATA['portal_id'];
            }

            if(!empty($portalCategory_id))
            {
                $detail['portalCategory_id'] = $portalCategory_id;
            }

            $this->createSingleCourseDetailByLang($course_id, $lang, $detail);
        }

        $teacher            = $this->parent->class->teachers->getTeachersByCourses([['id' => $DATA['clone_id']]]);

        if(!empty($teacher))
        {
            $teacher        = reset($teacher);
            $this->parent->class->teachers->createTeacherCourseRel($teacher['teacher_id'], $course_id);
        }

        $this->parent->hookClass('v2/ftp');

        // copy course image
        $filePath           = "opencontent/courses/previews/";

        // $copyImage
        $copyImage          = "opencontent/courses/previews/{$DATA['clone_id']}.jpg";
        $tmp_file           = DOCUMENT_ROOT.'/tmp/ftp-upload';
        mkdir($tmp_file);

        $tmp_file           = $tmp_file.'/'.$course_id.'.jpg';
        unlink($tmp_file);

        $this->parent->class->ftp->downloadFile($copyImage, $tmp_file);

        if(is_file($tmp_file))
        {
            $this->uploadCourseFile($tmp_file, $filePath);
        }

        $course['id']       = $course_id;

        $this->parent->class->portalsCourses->syncContentEnumerationTable('course');

        return $course;
    }

    public function updateSingleCourseByID($courseID = '', $DATA = array())
    {
        try {
            // hook up needed classes /////////////////
            $this->parent->hookClass('v2/languages');//
            $this->parent->hookClass('v2/teachers');///
            $this->parent->hookClass('v2/portals');///
            ///////////////////////////////////////////

            $successMsg = "Course has been successfully updated";

            $courseObj = $this->getSingleCourse($courseID);

            if(empty($courseObj))
                throw new Exception(null,404);

            $acceptPostFields = array();
            if (isset($DATA['default_lang'])) {
                $DATAlang = $this->parent->class->languages->getLangbyCodeISO6391($DATA['default_lang']);
                if(!empty($DATAlang))
                    $acceptPostFields['default_lang'] = $DATAlang['codeISO6391'];
            }
            if (isset($DATA['is_promoted'])) {
                $acceptPostFields['is_promoted'] = (int)filter_var($DATA['is_promoted'], FILTER_VALIDATE_BOOLEAN);
            }
            if (isset($DATA['is_discontinued'])) {
                $acceptPostFields['is_discontinued'] = (int)filter_var($DATA['is_discontinued'], FILTER_VALIDATE_BOOLEAN);
            }
            if (isset($DATA['promote_text'])) {
                $acceptPostFields['promote_text'] = (string)$DATA['promote_text'];
            }
            if (isset($DATA['is_published_course'])) {
                $acceptPostFields['is_published'] = (int)filter_var($DATA['is_published_course'], FILTER_VALIDATE_BOOLEAN);
            }
            if (isset($DATA['course_format']) && in_array($DATA['course_format'],['lessons','scorm','class'])  ) {
                $acceptPostFields['course_format'] = $DATA['course_format'];
            }

            if(!empty($acceptPostFields))
            {
                $Fields = $this->parent->preparePostFields($acceptPostFields);
                if (!$Fields) {
                    throw new Exception("Unknown error occured.",500);
                }

                $success = DataBase::update($this->dataTableName, $Fields," `id`='" . $courseID . "'");

                if (!$success)
                {
                    throw new Exception("Unknown error occured.", 500);
                }
            }

            if(isset($DATA['portalCategory_id']))
            {
                $this->moveCourseToPortalCategory($courseObj['id'], $DATA['portal_id'], $DATA['portalCategory_id']);
            }

            if(isset($DATA['author'])) {
                $this->parent->class->teachers->updateTeacherCourseRel($DATA['author'],$DATA['course_id']);
            }

            if(isset($DATA['is_published_course']) && $DATA['is_published_course'] == 1)
            {
	            $this->parent->self_resource("/v2/publish/courses/0".$courseID."/", 'POST', false, array());
                $successMsg = "Course has been successfully published";

                $DATA['is_update_portal'] = 1;
	        }

	        if(!empty($DATA['is_update_portal']))
            {
                $allPortals = $this->getAllPortalsByCourseID($courseID);

                  $value      = [];

                foreach($allPortals as $portal)
                {
                    $dataJson       =   ['data' => json_encode(['is_published' => 0])];
                    $this->parent->class->portals->updatePortalbyGUID($portal['portal_id'], $dataJson);
                    $value[]        = "'{$portal['portal_id']}'";
                }

                if(!empty($value))
                {
                  $value            = implode(',', $value);
                  // update portal state
                    $sql_q          = "UPDATE `portals` SET is_published = 0 WHERE id IN ($value)";

                    if(!DataBase::getInstance()->exec_query($sql_q))
                    {
                        throw new Exception('DataBase error', 500);
                    }
                }
            }

	        if(isset($DATA['is_published_portal']) && isset($DATA['portal_id']))
            {
                $this->parent->self_resource("/v2/publish/portals/".$DATA['portal_id']."/navigation", 'POST', false, array());
                $this->parent->self_resource("/v2/publish/portals/".$DATA['portal_id']."/courselist", 'POST', false, array());
            }

            return $successMsg;

        } catch (Exception $e) {

            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return false;
        }


    }

    public function updateSingleCourseDetailByLang($courseID = '', $langCODE = '', $DATA = array())
    {
        try {
            // hook up needed classes //////////////////
            $this->parent->hookClass('v2/languages');///
            $this->parent->hookClass('v2/courseQuiz');//
//            $this->parent->hookClass('v2/ftp');//
            ////////////////////////////////////////////

            $courseDetailObj = $this->getSingleDetailsByCourseLang( array("id" => $courseID) ,$langCODE);

//            //deleting long term cache file, because new will be with new name
//            $ceid            = $this->getCEIDByCourseID($courseID);
//            $url             = sprintf("contentcache/%s/%s-%s.json", $courseDetailObj['lang'], $ceid, $courseDetailObj['publish_date']);
//            $deleted         =  $this->parent->class->ftp->deleteFile($url);

            if(empty($courseDetailObj))
                throw new Exception(null,404);

            $acceptPostFields = array();

            if (isset($DATA['title'])) {
                $acceptPostFields['title'] = (string) trim($DATA['title']);
            }
            if (isset($DATA['tagline'])) {
                $acceptPostFields['tagline'] = (string) trim($DATA['tagline']);
            }
            if (isset($DATA['description'])) {
                $acceptPostFields['description'] = (string) trim($DATA['description']);
            }
            if (isset($DATA['has_manual'])) {
                $acceptPostFields['has_manual'] = (int)filter_var($DATA['has_manual'], FILTER_VALIDATE_BOOLEAN);
            }
            if (isset($DATA['meta_title'])) {
                $acceptPostFields['meta_title'] = (string) trim($DATA['meta_title']);
            }
            if (isset($DATA['meta_description'])) {
                $acceptPostFields['meta_description'] = (string) trim($DATA['meta_description']);
            }
            if (isset($DATA['meta_keywords'])) {
                $acceptPostFields['meta_keywords'] = (string) trim($DATA['meta_keywords']);
            }
            if (isset($DATA['publish_date'])) {
                $acceptPostFields['publish_date'] = (string)$DATA['publish_date'];
            }
            if (isset($DATA['has_vo'])) {
                $acceptPostFields['has_vo'] = (int)filter_var($DATA['has_vo'], FILTER_VALIDATE_BOOLEAN);
            }
            if (isset($DATA['has_subs'])) {
                $acceptPostFields['has_subs'] = (int)filter_var($DATA['has_subs'], FILTER_VALIDATE_BOOLEAN);
            }
            if (isset($DATA['subs_edited'])) {
                $acceptPostFields['subs_edited'] = (int)filter_var($DATA['subs_edited'], FILTER_VALIDATE_BOOLEAN);
            }

            if (isset($DATA['is_published'])) {
                $acceptPostFields['is_published'] = (int)filter_var($DATA['is_published'], FILTER_VALIDATE_BOOLEAN);
            }

            if (isset($DATA['city'])) {
                $acceptPostFields['city'] = (string)$DATA['city'];
            }
            if (isset($DATA['address'])) {
                $acceptPostFields['address'] = (string)$DATA['address'];
            }
            if (isset($DATA['date'])) {
                $acceptPostFields['date'] = (string)$DATA['date'];
            }
            if (isset($DATA['time'])) {
                $acceptPostFields['time'] = (string)$DATA['time'];
            }
            if (isset($DATA['class_capacity'])) {
                $acceptPostFields['class_capacity'] = (int)$DATA['class_capacity'];
            }
            if (isset($DATA['properties'])) {
                $acceptPostFields['properties'] = json_encode($DATA['properties']);
            }

            $Fields = $this->parent->preparePostFields($acceptPostFields);
            if (!$Fields) {
                throw new Exception("Unknown error occured.",500);
            }

            $success = DataBase::update("`course_details`", $Fields," `course_id`='" . $courseID . "' && `lang`='" .$langCODE. "'");

            $this->parent->class->courseQuiz->_createCourseNewQuizGUID($courseID,$langCODE);

            if(isset($DATA['eBooks']) && count($DATA['eBooks'])){
                foreach ($DATA['eBooks'] as $key => $eBook){
                    
                    $eBook['lang']      = $langCODE;
                    
                    if(!empty($eBook['action']) && $eBook['action'] == 'delete') {
                        $this->deleteCourseEBook($eBook['id'], $courseID, $eBook);
                        continue;
                    }
                    $this->updateCourseEBook($eBook['id'], $courseID, $eBook);
                }
            }

            if (!$success)
                throw new Exception("Unknown error occured.", 500);



            return $success;
        } catch (Exception $e) {

            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return false;
        }

    }

    public function getChaptersByCourseID($courseID = array())
    {
        try {

            // hook up needed classes ///////////
            $this->parent->hookClass('v2/mui');//
            /////////////////////////////////////

            if (!is_array($courseID)) {
                $courseID = array($courseID);
            }
            $lang = isset($this->parent->DATA['lang']) ? $this->parent->DATA['lang'] : 'en';
            if (!empty($courseID)) {
                $condition = array();
                $condition['course_id'] = "IN,(" . "'" . implode("','", $courseID) . "'" . ")";
                $orderCondition = $this->parent->orderCondition("view_order");
                $chapters = DataBase::select("course_chapters", $condition, $orderCondition);

                foreach ($chapters as $key => $value){
                    $mui = current($this->parent->class->mui->getTranslationByCode($value['id'], $lang));
                    $chapters[$key]['title'] = $mui['value'];
                }

                return $chapters;

            } else {
                throw new Exception();
            }

        } catch (Exception $e) {
            return null;
        }

    }

    private function getLessonsByChapterID($chaptersID = array())
    {
        try {

            // hook up needed classes ///////////
            $this->parent->hookClass('v2/mui');//
            /////////////////////////////////////

            if (!is_array($chaptersID)) {
                $chaptersID = array($chaptersID);
            }

            if (!empty($chaptersID)) {
                $condition = array();
                $condition['course_chapter_id'] = "IN,(" . "'" . implode("','", $chaptersID) . "'" . ")";
                $orderCondition = $this->parent->orderCondition();
                $lessons = DataBase::select("course_chapter_lessons", $condition, $orderCondition);

                foreach ($lessons as $key => $value){
                    $mui = current($this->parent->class->mui->getTranslationByCode($value['id'], $lang));
                    $lessons[$key]['title'] = $mui['value'];
                }

                return $lessons;

            } else {
                throw new Exception();
            }

        } catch (Exception $e) {
            return null;
        }

    }

    public function getAllDetailsByCourse($course = array(), $showNotPublishedCourses = true)
    {

        try {

            $outputFields = array(
                '*' => '*'
            );
            $extends = array();

            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));

            $condition = array();
            foreach ($course as $val) {
                $condition['course_id'] .= "'" . $val['id'] . "',";
            }
            $condition['course_id'] = "IN,(" . rtrim($condition['course_id'], ",") . ")";

            if(!$showNotPublishedCourses){
                $condition['is_published'] = '1';
            }

            $orderCondition = $this->parent->orderCondition();
            $courseDetails = DataBase::select("course_details", $condition, $orderCondition);

            $return = array();
            foreach ($courseDetails as $detail) {
                if (in_array("no_of_lessons", $this->parent->EXTEND_FIELDS)) {
                    $chapters = $this->getChaptersByCourseID($detail['course_id']);
                    $chaptersID = array();
                    foreach ($chapters as $ch) {
                        $chaptersID[] = $ch['id'];
                    }
                    $lessonsObj = $this->getLessonsByChapterID($chaptersID);
                    $detail['no_of_lessons'] = count($lessonsObj);
                }

                if(!empty($detail['properties']))
                {
                    $detail['properties'] = json_decode($detail['properties'], true);
                }

                $return[$detail{'course_id'}][$detail['lang']] = $detail;
                unset($return[$detail{'course_id'}]['lang']);
            }

            return $return;

        } catch (Exception $e) {
            return null;
        }


    }

    public function getDetailsByCourseLang($course = [], $langCODE)
    {

        $course_id              = DataBase::getInstance()->quote($course['id']);
        $lang                   = DataBase::getInstance()->quote($langCODE);
        $default_lang           = DataBase::getInstance()->quote($course['default_lang']);

        $query                  = "SELECT * FROM `course_details` 
WHERE course_id = $course_id AND `lang` IN ($lang, $default_lang)";

        if(!DataBase::getInstance()->exec_query($query))
        {
            throw new Exception('DataBase error', 500);
        }

        $result                 = DataBase::getInstance()->fetchAll();

        if(!is_array($result) || empty($result))
        {
            return null;
        }

        if(count($result) == 2)
        {
            if($result[0]['lang'] === $langCODE)
            {
                return $result[0];
            }
            else
            {
                return $result[1];
            }
        }

        return $result[0];
    }

    public function getSingleDetailsByCourseLang($course = [], $langCODE)
    {
        try {


            $outputFields = array(
                '*' => '*'
            );
            $extends = array();

            // default_lang
            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));

            $condition['course_id'] = "'" . $course['id'] . "'";
            $condition['lang'] = "'" . $langCODE . "'";
            $orderCondition = $this->parent->orderCondition();
            $courseDetail = current(DataBase::select("course_details", $condition, $orderCondition));

            if(!empty($courseDetail['properties']))
            {
                $courseDetail['properties'] = json_decode($courseDetail['properties'], true);
            }

            return $courseDetail;

        } catch (Exception $e) {
            return null;
        }

    }

    public function getCoursesNames($lang = 'en', $showIsDiscontinuedCourses = false, $showNotPublishedCourses = false){
        static $cacheResult = array();
        $cacheResultKey = $lang.'-'.($showIsDiscontinuedCourses ? '1' : '0');
        if(isset($cacheResult[$cacheResultKey])) return $cacheResult[$cacheResultKey];


        $this->getAllCourseLessons();

        try {
            if(!$lang)
                throw new Exception(null,404);

            $orderCondition = $this->parent->orderCondition('c.id');
            $outputFields = array(
                '*' => array(
                    'c.id',
                    // 'cd.title',
                    // 'cd.description',
                    'c.is_promoted',
                    'c.course_format',
                    'td.full_name',
                    'ce.id as  `ceid`',
                    // 'ccl.runtime_seconds'
                )
            );
            $extends = array();
            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));
            $condition = array(
                "c.id" => "`cd`.`course_id`",
                "cd.lang" => "'".$lang."'",
                "cd.is_published" => 1,
                "c.is_discontinued" => 0,
                "tc.teacher_id" => "`td`.`teacher_id`",
                "tc.course_id" => "`cd`.`course_id`",
                "td.lang" => "'en'",
                "ce.content_id" => "`c`.`id`",
            );
            if($showNotPublishedCourses){
                unset($condition['cd.is_published']);
            }
            if($showIsDiscontinuedCourses){
                unset($condition['c.is_discontinued']);
            }

            $selectFromTables = array(
                "courses c",
                "course_details cd",
                "teachers_data td",
                "teachers_courses tc",
                "content_enumeration ce",
            );

            $courseList = DataBase::select(implode(", ", $selectFromTables), $condition, $orderCondition, false);

            $allCourseIds = array();
            foreach ($courseList as $course) {
                $allCourseIds[] = array('id'=>$course['id']);
            }

            $course_details = $this->getAllDetailsByCourse($allCourseIds, $showNotPublishedCourses);

            $return = array();
            foreach ($courseList as $course) {
                if(!isset($return[$course['id']])){

                    $return[$course['id']] = array(
                        'name' => $course_details[$course['id']][$lang]['title'],
                        'author' => array(),
                        'totalLessons' => 0,
                        'trt' => array(),
                        'description' => $course_details[$course['id']][$lang]['description'],
                        'is_promoted' => $course['is_promoted'],
                        'course_format' => $course['course_format'],
                        'available_langs' => array_keys($course_details[$course['id']]),
                        'ceid' => $course['ceid'],
                    );
                }
                $return[$course['id']]['author'][] = $course['full_name'];
            }

            // retrieving lessons info
            foreach ($return as $courseId => $courseInfo) {
                $courseLessons = $this->allCourseLessons[$courseId][$lang];
                foreach ($courseLessons as $lessonInfo) {
                    $return[$courseId]['totalLessons']++;
                    $return[$courseId]['trt'][$lang] += $lessonInfo['runtime_seconds'];
                }
            }
            $cacheResult[$cacheResultKey] = $return;
            return $cacheResult[$cacheResultKey];

        } catch (Exception $e) {
            return array(
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            );
        }
    }

    function getNewSubCategories($category, $lang, $deflang, $publishedCourses){
        $result = array();
        $subCategories = array();
        if (!empty($tmpcourses = $publishedCourses[$category['id']][$lang])) $courses = $tmpcourses;
        if(is_array($category['children'])){
            foreach($category['children'] as $childrenCategory){


                if (($childrenCategory['data'][$lang]) == null)
                {
                    $subCategoryName = $childrenCategory['data'][$deflang]['title'];
                }
                else { $subCategoryName = $childrenCategory['data'][$lang]['title']; }

                if (!empty($tmpsubs =  $this->getNewSubCategories($childrenCategory, $lang, $deflang, $publishedCourses))) $subCategories[$subCategoryName] = $tmpsubs;
                if (!empty($tmpcourses = $subCategories[$subCategoryName]['courses'])) $subCategories[$subCategoryName]['courses'] =  $tmpcourses;
                if (!empty($tmpcategory = $childrenCategory['data'][$lang]['title'])) $subCategories[$subCategoryName]['category'] =  $tmpcategory;
            }
        }

        if ($cat = $category['data'][$lang]['title']) $result['category'] = $cat;
        if ($tag = $category['data'][$lang]['tag']) $result['tagline'] = $tag;
        if (!empty($tag)) $result['tagline'] = $tag;
        if (!empty($courses)) $result['courses'] = $courses;
        if (!empty($subCategories)) $result['subs'] = $subCategories;

        if ((empty($subCategories)) AND (empty($courses))) return null;
        return $result;

    }

    function getAllCourseLessons(){
        if(is_array($this->allCourseLessons)) return true;

        $this->allCourseLessons = array();

        $extends = array();
        $orderCondition = $this->parent->orderCondition('c.id');
        $outputFields = array(
            '*' => array(
                'c.id as courseId',
                'ccld.runtime_seconds',
                'ccld.file',
                'ccld.lang',
                'ccl.id as lessonId',
                'ccl.lesson_type'
            )
        );

        $selectFromTables = array(
            "courses c",
            "course_chapters cc",
            "course_chapter_lessons ccl",
            "course_chapter_lessons_data ccld",
        );

        $condition = array(
            "cc.id" => "`ccl`.`course_chapter_id`",
            "ccld.is_hidden" => "0",
            "ccl.id" => "`ccld`.`course_chapter_lesson_id`",
            "cc.course_id" => "`c`.`id`",
            "c.is_discontinued" => "0",

        );

        DataBase::selectFields($this->parent->outputFields($outputFields, $extends));
        $allCourseLessons = DataBase::select(implode(", ", $selectFromTables), $condition, $orderCondition, false);

        foreach($allCourseLessons as $lessonItem){
            $courseId = $lessonItem['courseId'];
            $lang = $lessonItem['lang'];

            $file = $lessonItem['file'];
            $runtime_seconds = $lessonItem['runtime_seconds'];
            $lessonId = $lessonItem['lessonId'];

            if(!is_array($this->allCourseLessons[$courseId][$lang]))
                $this->allCourseLessons[$courseId][$lang] = array();

            $this->allCourseLessons[$courseId][$lang][] = array(
                'lessonId' => $lessonId,
                'file' => $file,
                'runtime_seconds' => $runtime_seconds,
            );
        }

        return true;
    }

    public function getPopularCoursesByDay($time){

        $date = new DateTime;
        $date->setTimestamp( $time );
        $date->setTime( 0, 0, 0);
        $timestamp_start = $date->getTimestamp();

        $date->setTime( 23, 59, 0);
        $timestamp_end = $date->getTimestamp();

        DataBase::getInstance()->exec_query("SELECT `course_id`, COUNT(*) as 'count' FROM `course_lessons_progress` WHERE `begin_time` > '". ($timestamp_start - 1) ."' AND `begin_time` < '". ($timestamp_end + 1) ."' GROUP BY `course_id`");
        $results = DataBase::getInstance()->fetchAll();

        $result['date'] = $date->format('Y-m-d');
        foreach ($results as $val){

            $result['stats'][] = [ "course" => $val['course_id'], "total" => $val['count']  ];
        }

        return $result;

    }

    public function getAllPortalsByCourseID($courseID){


// 	$courseID = $this->getSingleCourseWithDetails($courseID);
//
// 	if(empty($courseObj))
//                 throw new Exception(null,404);
//
        try {

        DataBase::getInstance()->exec_query(
	  "SELECT DISTINCT pc.`portal_id`
	   FROM `portal_categories_courses` pcc
	   JOIN `portal_categories` pc ON pcc.`portalCategory_id` = `pc`.`id` 
	   WHERE `course_id` = '".$courseID."'");


	  $results = DataBase::getInstance()->fetchAll();

	  if (!is_array($results)) {
            throw new Exception(null, 405);
	  }


	  return $results;

         } catch (Exception $e) {
            return array(
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            );
        }

    }

    public function uploadCourseFiles($courseID){

        $course                     = $this->getSingleCourseWithDetails($courseID);

        if(empty($course))
        {
            throw new \Exception('Course is not found', 401);
        }

        $uploadFiles = [];

        foreach($_FILES as $name => $file)
        {

            $filePath = null;

            if(!empty($_FILES[$name]['error']))
            {
                throw new Exception('Upload error: '.$_FILES[$name]['error'], 501);
            }

            if($name == 'coursePreview') {

                $uploadFiles['coursePreview'] = [];
                $filePath           = "opencontent/courses/previews/";
                $filePathResized    = "opencontent/courses/previews/" . $courseID ."/";

                $convertedImages    = $this->changeImageResolutionAndType($_FILES[$name]['tmp_name'], $courseID, $_FILES[$name]['type'], 'jpg');

                foreach ($convertedImages as $resolution => $convertedImage) {

                    if($resolution == 'source') {
                        $uploadFiles['coursePreview'][]      = $this->uploadCourseFile($convertedImage, $filePath);
                        continue;
                    }

                    $this->uploadCourseFile($convertedImage, $filePathResized);
                }
            }

            $lang                           = $_POST['lang'] ? $_POST['lang'] : 'en';
            $ebookID                        = $_POST['ebookID'] ? $_POST['ebookID'] : null;

            if($name == 'ebookPreview') {
                $uploadFiles['ebookPreview']    = [];

                switch(strtolower($_FILES[$name]['type']))
                {
                    case 'image/gif':   $type     = 'gif'; break;
                    case 'image/jpeg':  $type     = 'jpg'; break;
                    case 'image/png':   $type     = 'png'; break;

                    default: throw new Exception("Bad file type $name: must be image (gif, jpeg, png, ico)", 501);
                }
                $filePath                       = "opencontent/courses/ebook/previews/" . $courseID ."/" . $lang. "/";

                $upload             = $this->uploadCourseFile($_FILES[$name]['tmp_name'], $filePath, $_FILES[$name]['name']);

                $upload['id']       = $this->updateCourseEBook($ebookID, $courseID, ['preview' =>$_FILES[$name]['name'], 'lang' =>  $lang]);

                $uploadFiles['ebookPreview'] = $upload;
            }

            if($name == 'ebookFile') {

                $uploadFiles['ebookFile']       = [];

                if(strtolower($_FILES[$name]['type']) != 'application/pdf') {
                    throw new Exception("Bad file type $name: must be pdf", 501);
                }
                $filePath                       = "opencontent/courses/ebook/files/" . $courseID ."/" . $lang. "/";

                $upload             = $this->uploadCourseFile($_FILES[$name]['tmp_name'], $filePath, $_FILES[$name]['name']);

                $upload['id']       = $this->updateCourseEBook($ebookID, $courseID, ['file' =>$_FILES[$name]['name'], 'lang' =>  $lang]);

                $uploadFiles['ebookFile']     = $upload;
            }
        }

        return $uploadFiles;
    }


    public function uploadCourseFile($sourceFile, $filePath, $filename=null)
    {
        /////////////////////////////////////
        $this->parent->hookClass('v2/ftp');//
        /////////////////////////////////////

        if(empty($sourceFile)){
            return null;
        }

        $filename   = !is_null($filename) ? $filename : array_pop(explode('/', $sourceFile));

        if(!$filename) {

            throw new Exception('File name is required', 412);
        }

        $result                     = $this->parent->class->ftp->uploadFile($sourceFile, $filePath . $filename);

        if(!$result)
        {
            throw new Exception("Error create file in the CDN", 503);
        }

        @unlink($sourceFile);

        $cacheFlyURL                = KC_CDN_PROTOCOL.'://'.KC_CDN_HOST.'/'. $filePath . $filename;

        return
            [
                'name'                  => $filename,
                'url'                   => $cacheFlyURL
            ];
    }


    public function getCourseIntroByCourseID($courseID, $lang)
    {
        try {

            if ( !isset($courseID) ) {
                throw new Exception(null, 412);
            }

            if ( !isset($lang) ) {
                throw new Exception(null, 412);
            }

            DataBase::getInstance()->exec_query("
                        SELECT `ccld`.course_chapter_lesson_id, `cc`.course_id, `ccl`.lesson_type
                        FROM 
                            `course_chapter_lessons` as `ccl` ,
                            `course_chapter_lessons_data` as `ccld`,
                            `course_chapters` as `cc`  
                        WHERE 
                        `ccl`.`course_chapter_id` = `cc`.`id`
                        AND `cc`.view_order = 1
                        AND `ccl`.`id` = `ccld`.`course_chapter_lesson_id`
                        AND `ccl`.view_order = 1
                        AND `cc`.course_id = '".$courseID."'
                        AND `ccld`.lang = '".$lang."'");

            $lesson     = current(DataBase::getInstance()->fetchAll());

            return $lesson;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }

    }


    /**
     * Change image resolution and type. Accept jpg, gif, png
     * @string  $sourceFile
     * @param $outputFilename string
     * @param $typeInput string image/jpg
     * @param $typeOutput string jpg
     * @return mixed array
     * @throws Exception
     */
    public function changeImageResolutionAndType($sourceFile, $outputFilename, $typeInput, $typeOutput, $q = 100)
    {
        /////////////////////////////////////
        $this->parent->hookClass('v2/ftp');//
        /////////////////////////////////////

        $resolutions = [128, 160, 240, 320, 400, 640, 720, 800, 1280, 1600];

        $tmpFolder           = 'tmp/';

        $size                = getimagesize($sourceFile);

        if(!is_array($size)) {
            throw new Exception('Image size is undefined', 412);
        }

        switch(strtolower($size['mime']))
        {
            case 'image/gif':
                $source   = imagecreatefromgif($sourceFile);
                break;

            case 'image/jpeg':
                $source   = imagecreatefromjpeg($sourceFile);
                break;

            case 'image/png':
                $source   = imagecreatefrompng($sourceFile);
                break;

            default: throw new Exception("Bad file type must be image (gif, jpeg, png, ico)", 501);
        }

        $resolutions[]      = $size[0].'-source';

        $files              = [];

        foreach ($resolutions as $resolution) {

            $height         = intval($resolution / ($size[0]/$size[1]));

            $outputFile     = $tmpFolder . $resolution . '.' . $typeOutput;

            $fileSize       = $resolution;

            if( count(explode('-', $resolution)) > 1 ) {
                $fileSize       = 'source';
                $outputFile     = $tmpFolder . $outputFilename . '.' . $typeOutput;
            }

            $resource       = imagecreatetruecolor($resolution, $height);

            $resize         = imagecopyresampled($resource, $source, 0,0,0,0, $resolution, $height, $size[0],$size[1] );

            if(!$resize) {
                continue;
            }

            switch(strtolower($typeOutput))
            {
                case 'gif':
                    imagegif($resource, $outputFile);
                    break;

                case 'jpg':
                    imagejpeg($resource, $outputFile, $q);
                    break;

                case 'png':
                    imagepng($resource, $outputFile);
                    break;

                default: throw new Exception("Bad file type must be image (gif, jpeg, png, ico)", 501);
            }

            @imagedestroy($resource);

            $files[$fileSize] = $outputFile;

        }

        @imagedestroy($source);

        return $files;

    }


    /**
     * Get courseCEID by courseID
     * @str   $courseID
     * @return integer
     * @throws Exception
     */
    public function getCEIDByCourseID($courseID)
    {
        $condition              = [];
        $condition['content_id'] = "'" . $courseID . "'";
        $ceid = current(DataBase::select('`content_enumeration`', $condition, []));

        if(!$ceid) throw new Exception(null,404);

        return $ceid['id'];
    }


     /**
     * Get courseID by courseCEID
     * @int   $courseCEID
     * @return array
     * @throws Exception
     */
    public function getCourseByCEID($courseCEID)
    {
        $rowCE          = current(DataBase::select('`content_enumeration`', ['id' => $courseCEID], []));

        if(!$rowCE) throw new Exception(null,404);

        $result         = ['courseID' => $rowCE['content_id'],'courseCEID' => $courseCEID];

        if($rowCE['content_type'] == 'lesson') {
            DataBase::getInstance()->exec_query("
                        SELECT `cc`.`course_id`, `cc`.`id` 
                        FROM `course_chapters` as `cc`,
                        `course_chapter_lessons` as `ccl` 
                        WHERE 
                        `cc`.`id` = `ccl`.`course_chapter_id`
                        AND 
                        `ccl`.`id` =  '" . $rowCE['content_id'] . "'");

            $rowCC      = current(DataBase::getInstance()->fetchAll());

            if(!$rowCC) throw new Exception(null,404);

            DataBase::getInstance()->exec_query("
                        SELECT `id`
                        FROM `content_enumeration`
                        WHERE 
                        `content_id` = '" . $rowCC['course_id'] . "'");

            $courseCE      = current(DataBase::getInstance()->fetchAll());

            $result         = ['courseID' => $rowCC['course_id'], 'courseCEID' =>$courseCE['id'], 'lesson' => $rowCE['content_id'] ];
        }

        return $result;
    }


    /**
     * Get $contentCEID by contentID
     * @string   $сontentID
     * @return integer or NULL
     * @throws Exception
     */
    public function getCourseCEID($contentID)
    {
        DataBase::getInstance()->exec_query("
                        SELECT `id`
                        FROM `content_enumeration`
                        WHERE 
                       `content_id` = '" . $contentID . "'");

         $contentCEID   = current(DataBase::getInstance()->fetchAll());

        return $contentCEID['id'] ? $contentCEID['id'] : NULL;
    }


    /**
     * Create  data for file longTermCache on CDN
     * @return string
     * @throws Exception
     */
    public function  createLongTermCacheFile()
    {

        /////////////////////////////////////
        $this->parent->hookClass('v2/ftp');//
        /////////////////////////////////////

        DataBase::getInstance()->exec_query("SELECT * FROM `content_enumeration` WHERE `content_type`  = 'course'");
        $courseCEIDs        = DataBase::getInstance()->fetchAll();

        if(!$courseCEIDs) throw new Exception(null,404);

        $courseIDCEIDs      = [];
        foreach ($courseCEIDs as $courseCEID){

            $courseIDCEIDs[$courseCEID['content_id']] =  $courseCEID['id'];
        }


        DataBase::getInstance()->exec_query("SELECT `course_id`, `lang`, `publish_date` FROM `course_details`");
        $items              = DataBase::getInstance()->fetchAll();

        if(!$items) throw new Exception(null,404);

        $langs             = [];
        foreach ($items as $course){

            $langs[$course['lang']][ $courseIDCEIDs[$course['course_id']] ] = $course['publish_date'];
        }


        $cacheFilePath      = 'contentcache/long-term-cache.js';
        $ftpResult          = $this->parent->class->ftp->uploadContent('ltCache='.json_encode($langs), $cacheFilePath);


        return $ftpResult ? $cacheFilePath : null;
    }


    public function addClassToPortal($course_id, $portal_id, $lang, $portalCategory_id = null)
    {
        // 1. Try find exists category for course
        if($portalCategory_id === null)
        {
            // get portalCategory_id
            $category           = DataBase::select('portal_categories_courses', ['course_id' => $course_id]);

            $portalCategory_id  = null;

            if(!empty($category))
            {
                $portalCategory_id = $category[0]['portalCategory_id'];
                return $category[0];
            }
        }

        if($portalCategory_id === null)
        {
            return [];
        }

        // 2. Find category for Class
        if($portalCategory_id === null)
        {
            $category                   = $this->getClassCategory($portal_id);

            if(empty($category))
            {
                $category               = $this->createClassCategory($portal_id);
            }
        }
        else
        {
            $category                   = ['id' => $portalCategory_id];
        }


        // 2. Add course to category
        $data                       =
        [
            'id'                    => UserHelper::CreateGUID(),
            'course_id'             => $course_id,
            'portalCategory_id'     => $category['id'],
            'lang'                  => $lang,
            'order'                 => 0
        ];

        if(!DataBase::insert('portal_categories_courses', $data))
        {
            throw new Exception('DataBase error', 500);
        }

        return $data;
    }

    public function getClassCategory($portal_id)
    {
        $portal_id                  = DataBase::getInstance()->quote($portal_id);
        $query                      = "SELECT * FROM `portal_categories` as pc 
WHERE json_filename = 'classes' AND portal_id = $portal_id";

        if(!DataBase::getInstance()->exec_query($query))
        {
            throw new Exception('Server database error', 500);
        }

        $category                   = DataBase::getInstance()->fetchAll();

        if(empty($category))
        {
            return null;
        }

        return $category[0];
    }

    public function createClassCategory($portal_id)
    {
        $data                       =
        [
            'id'                    => UserHelper::CreateGUID(),
            'portal_id'             => $portal_id,
            'order'                 => 0,
            'json_filename'         => 'classes'
        ];

        if(!DataBase::insert('portal_categories', $data))
        {
            throw new Exception('DataBase error', 500);
        }

        $category                   = $data;

        $data                       =
        [
            'id'                    => UserHelper::CreateGUID(),
            'portalCategory_id'     => $category['id'],
            'title'                 => 'Classes',
            'lang'                  => 'en',
            'description'           => '',
            'tag'                   => '',
            'metaTitle'             => '',
            'metaDescription'       => '',
            'metaKeywords'          => ''
        ];

        if(!DataBase::insert('portal_categories_data', $data))
        {
            throw new Exception('DataBase error', 500);
        }

        return $category;
    }

    public function moveCourseToPortalCategory($course_id, $portal_id, $portalCategory_id)
    {
        // 1. Find course
        $category                   = $this->findCoursePortalCategory($course_id, $portal_id);

        if(empty($category))
        {
            return;
        }

        // 2. delete
        $course_id                  = DataBase::getInstance()->quote($course_id);
        $portalCategory_id          = DataBase::getInstance()->quote($portalCategory_id);

        $category                   = '"'.implode('","', $category).'"';

        // 1. update
        $query                      = "UPDATE `portal_categories_courses` SET `portalCategory_id` = $portalCategory_id
WHERE course_id = $course_id AND `id` IN ($category)";

        if(!DataBase::getInstance()->exec_query($query))
        {
            throw new Exception('DataBase error', 500);
        }
    }

    public function findCoursePortalCategory($course_id, $portal_id)
    {
        $course_id                  = DataBase::getInstance()->quote($course_id);
        $portal_id                  = DataBase::getInstance()->quote($portal_id);


        DataBase::getInstance()->exec_query(
            "SELECT `pcc`.`id` 
FROM `courses` as c
INNER JOIN `portal_categories` as pc ON(`pc`.`portal_id` = $portal_id)
INNER JOIN `portal_categories_courses` as pcc ON(`pcc`.`course_id` = `c`.`id` AND `pcc`.`portalCategory_id` = `pc`.`id`)
WHERE `c`.`id` = $course_id");

        $results                    = DataBase::getInstance()->fetchAll();

        if(empty($results))
        {
            return null;
        }

        $ids                        = [];

        foreach($results as $result)
        {
            $ids[]                  = $result['id'];
        }

        return $ids;
    }

    public function deleteCourseByID($course_id, array $data)
    {
        // 1. get course
        $course                     = $this->getSingleCourse($course_id);

        if(empty($course))
        {
            throw new Exception("Course id $course_id is not found", 404);
        }

        if($course['course_format'] !== 'class')
        {
            throw new Exception("Course $course_id is not class", 412);
        }

        if(empty($data['portal_id']))
        {
            throw new Exception("portal_id is not defined", 412);
        }

        // check for portal_id belong to course_id
        $query                      = "SELECT `pc`.`portal_id` FROM `portal_categories_courses` as pcc 
INNER JOIN portal_categories as pc ON (`pc`.`id` = `pcc`.`portalCategory_id`)
WHERE `pcc`.`course_id` = '{$course['id']}'";

        if(!DataBase::getInstance()->exec_query($query))
        {
            throw new Exception('DataBase exception', 500);
        }

        $results                    = DataBase::getInstance()->fetchAll();

        foreach($results as $result)
        {
            if($result['portal_id'] !== $data['portal_id'])
            {
                throw new Exception("The course $course_id is not belong to portal {$data['portal_id']}", 412);
            }
        }

        // select course_details
        $results                    = DataBase::select('course_details', ['course_id' => "'$course_id'"]);

        foreach($results as $result)
        {
            if(!DataBase::delete('course_quizzes', "course_details_id='{$result['id']}'"))
            {
                throw new Exception('DataBase exception', 500);
            }
        }

        // course_quizzes

        // 2. Delete course
        if(!DataBase::delete('course_details', "course_id='$course_id'"))
        {
            throw new Exception('Couldn\'t delete course detils', 500);
        }

        if(!DataBase::delete('course_general_stat', "course_id='$course_id'"))
        {
            throw new Exception('Couldn\'t delete course general stat', 500);
        }

        if(!DataBase::delete('course_lists_content', "course_id='$course_id'"))
        {
            throw new Exception('Couldn\'t remove course from lists', 500);
        }

        if(!DataBase::delete('portal_categories_courses', "course_id='$course_id'"))
        {
            throw new Exception('Couldn\'t delete course from categories', 500);
        }

        // teachers
        if(!DataBase::delete('teachers_courses', "course_id='$course_id'"))
        {
            throw new Exception('Couldn\'t delete teach information', 500);
        }

        if(!DataBase::delete('courses', "id='$course_id'"))
        {
            throw new Exception('Couldn\'t delete course', 500);
        }

        return true;
    }

    public function getCourseEBook($courseID, $lang)
    {
        try {
            $orderCondition = $this->parent->orderCondition();
            $condition['course_id'] = "'" . $courseID . "'";
            $condition['lang']      = "'" . $lang . "'";

            $eBooks              = DataBase::select('`course_ebooks`', $condition, $orderCondition);

            return $eBooks;

        } catch (Exception $e) {

            if (!is_null($e->getMessage())) {
                $this->parent->RESPONSE = array("message" => $e->getMessage());
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function insertCourseEBook($courseID, array $DATA)
    {
        try {

            // hook up needed classes ////////////////
            $this->parent->hookClass('v2/portalsCourses');//
            //////////////////////////////////////////

            if(empty($DATA['lang'])) {
                throw new Exception(null, 412);
            }

            $acceptPostFields = array(
                "*id" =>    UserHelper::CreateGUID(),
                "course_id" => $courseID,
                "lang"      => $DATA['lang'],
                "name"      => $DATA['name'] ?  $DATA['name'] : 'Default Name',
                "quote"     => $DATA['quote'] ?  $DATA['quote'] : '',
                "brief"     => $DATA['brief'] ?  $DATA['brief'] : '',
                "preview"   => $DATA['preview'] ?  $DATA['preview'] : '',
                "file"      => $DATA['file'] ?  $DATA['file'] : '',
            );

            $Fields         = $this->parent->preparePostFields($acceptPostFields);
            $insertID       = DataBase::insert('`course_ebooks`', $Fields);

            if (!$insertID) {
                throw new Exception(null, 500);
            }
            //$this->parent->class->portalsCourses->syncContentEnumerationTable('ebook');

            return $Fields['id'];

        } catch (Exception $e) {

            if (!is_null($e->getMessage())) {
                $this->parent->RESPONSE = array("message" => $e->getMessage());
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }

    }

    public function updateCourseEBook($id, $courseID, array $DATA)
    {
        if(empty($id)) {
            return $this->insertCourseEBook($courseID, $DATA);
        }

        $condition['id']        = "'" . $id . "'";
        $condition['course_id'] = "'" . $courseID . "'";
        $condition['lang']      = "'" . $DATA['lang'] . "'";

        $eBook                  = current(DataBase::select('`course_ebooks`', $condition, []));
        if(empty($eBook)) {
            throw new Exception(null, 404);
        }

        if(!empty($DATA['name'])) {
            $acceptPostFields['name'] = $DATA['name'];
        }
        if(!empty($DATA['brief'])) {
            $acceptPostFields['brief'] = $DATA['brief'];
        }
        if(!empty($DATA['quote'])) {
            $acceptPostFields['quote'] = $DATA['quote'];
        }
        if(!empty($DATA['preview'])) {
            $acceptPostFields['preview'] = $DATA['preview'];
        }
        if(!empty($DATA['file'])) {
            $acceptPostFields['file']    = $DATA['file'];
        }

        $Fields             = $this->parent->preparePostFields($acceptPostFields);

        $success = DataBase::update("`course_ebooks`", $Fields,
                " `id`='" . $eBook['id'] . "' && `course_id`= '" . $courseID . "'");
        if (!$success) {
           return false;
        }
        return $success;
    }

    public function deleteCourseEBook($id, $courseID, $DATA)
    {
        try {

            /////////////////////////////////////
            $this->parent->hookClass('v2/ftp');//
            /////////////////////////////////////

            if(empty($id)) {
                throw new Exception(null,404);
            }

            $orderCondition         = $this->parent->orderCondition();
            $condition['id']        = "'" . $id . "'";
            $condition['lang']      = "'" . $DATA['lang']  . "'";
            $condition['course_id'] = "'" . $courseID . "'";

            $eBook                  = current(DataBase::select('`course_ebooks`', $condition, $orderCondition));

            if( empty($eBook) ) {
                throw new Exception(null,404);
            }

            if(!empty($eBook['preview'])) {
                $data['path']   = 'opencontent/courses/ebook/preview/'.$courseID.'/'. $eBook['lang'].'/'.$eBook['preview'];
                $this->parent->class->ftp->deleteFile($data['path']);
            }

            if(!empty($eBook['file'])) {
                $data['path']   = 'opencontent/courses/ebook/files/'.$courseID.'/'. $eBook['lang'].'/'.$eBook['file'];
                $this->parent->class->ftp->deleteFile($data['path']);
            }

            $response          = DataBase::delete("`course_ebooks`",
                " `id`='" . $DATA['id'] . "' && `course_id`='" . $courseID . "'");

            return $response;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function getCoursesIntro()
    {
        DataBase::getInstance()->exec_query("
                       SELECT `ccl`.id, `cc`.course_id, `ccl`.lesson_type 
                       FROM `course_chapter_lessons` as `ccl` 
                       LEFT JOIN `course_chapters` as `cc` ON (`ccl`.`course_chapter_id` = `cc`.`id`) 
                       WHERE `ccl`.is_preview = '1'");

        $allCoursesIntroGUIDs  = DataBase::getInstance()->fetchAll();

        $coursesIntroGUIDs = [];
        foreach ($allCoursesIntroGUIDs as $courseIntro) {
            $coursesIntroGUIDs[$courseIntro['course_id']] = [
                'guid'          => $courseIntro['id'],
                'lesson_type'   => $courseIntro['lesson_type']
            ];
        }

        return $coursesIntroGUIDs;
    }

    public function getCoursesTrt($lang, $format = null)
    {

        // hook up needed classes ////////////////
        $this->parent->hookClass('helper');     //
        //////////////////////////////////////////
        $coursesTrt     = [];

        if(empty($lang)) {
            return $coursesTrt;
        }

        DataBase::getInstance()->exec_query("
                        SELECT `cc`.course_id, `ccl`.id, `ccld`.`lang`, `ccld`.`runtime_seconds`
                        FROM `course_chapter_lessons_data` as `ccld`, `course_chapter_lessons` as `ccl`, `course_chapters` as `cc`
                        WHERE `cc`.id = `ccl`.`course_chapter_id`
                        AND `ccl`.id = `ccld`.`course_chapter_lesson_id`
                        AND `ccld`.`lang` = '".$lang."'");

        $allCoursesLessonsTRT  = DataBase::getInstance()->fetchAll();

        $count                 = count($allCoursesLessonsTRT);

        foreach ($allCoursesLessonsTRT as $lessonInfo) {

            $coursesTrt[$lessonInfo['course_id']]['totalLessons']++;
            $coursesTrt[$lessonInfo['course_id']]['trt']         += $lessonInfo['runtime_seconds'];
        }


        if(!empty($format) && $format == 'hours') {
            foreach($coursesTrt as $courseID => $details) {
                $coursesTrt[$courseID]['hours'] = $this->parent->class->helper->trt2string($details['trt'], 2, $lang);
            }
        }
        return $coursesTrt;
    }


    public function getCourseAvailableLangs($courseID)
    {
        $available_langs = [];

        if(empty($courseID)) {
            return $available_langs;
        }

        DataBase::getInstance()->exec_query("SELECT `lang` FROM `course_details` WHERE `course_id` = '".$courseID."' ");
        $results = DataBase::getInstance()->fetchAll();

        foreach ($results as $result) {
            $available_langs[] =  $result['lang'];
        }

        return $available_langs;

    }
    
    /**
     * Select courses by owner_account_id
     * @param $accountId
     * @return mixed
     */
    public function getCoursesByAccountOwner($accountId, array $filters = []){

        $sqlLimit                   = '';

        if($this->parent->LIMIT){
            $start                      = $this->parent->START ?? 0;
            $sqlLimit                   = "LIMIT {$start}, {$this->parent->LIMIT}";
        }

        $condition                  = [];
        $group_by                   = '';

        $accountId                  = DataBase::getInstance()->quote($accountId);
        $condition[]                = "(courses.owner_account_id = $accountId)";

        if(isset($filters['search']))
        {
            $search                 = addslashes('%'.$filters['search'].'%');
            $condition['search']    = "course_details.title LIKE '$search'";
            $group_by               = 'GROUP BY course_details.course_id';
        }

        if(isset($filters['course']))
        {
            $condition['course']    = "course_details.course_id = '".$filters['course']."'";
        }

        $sqlWhere                   = empty($condition) ? '' : 'WHERE ' . join(' AND ', $condition);

        $sql                        = " SELECT SQL_CALC_FOUND_ROWS courses.id as course_id, course_details.title 
                                       FROM courses
                                       JOIN course_details ON course_details.course_id = courses.id 
                                       AND course_details.lang = courses.default_lang
                                       $sqlWhere  $group_by  $sqlLimit";

        DataBase::query($sql);
        $courses                    = DataBase::getInstance()->fetchAll();
        if(!empty($sqlLimit)){
            $this->parent->TOTAL    = DataBase::getTotalCount();
        }

        return $courses;
    }
}
