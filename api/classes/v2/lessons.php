<?php

class lessons
{
    public $parent = null;

    private $dataTableName = 'course_chapter_lessons';

    function __construct(){}

    public function getLessonsByChapter($courseID = '', $chapterID = '')
    {
        try {
            // hook up needed classes ////////////////
            $this->parent->hookClass('v2/chapters');//
            $this->parent->hookClass('v2/courses'); //
            $this->parent->hookClass('v2/mui');     //
            $this->parent->hookClass('v2/rev_api'); //
            //////////////////////////////////////////

            if(empty($chapterID))
                throw new Exception(null,404);

            $courseID = $this->parent->class->courses->getSingleCourseWithDetails($courseID);

            if(empty($courseID))
                throw new Exception(null,404);

            $lang = isset($this->parent->DATA['lang']) ? $this->parent->DATA['lang'] : $courseID['default_lang'];

            $condition['course_chapter_id'] = "'" . $chapterID . "'";

            $outputFields = array(
                '*' => array("view_order","lesson_type", "lesson_subtype", "is_open", "is_preview")
            );
            $extends = array('*' => '*');

            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));
            $orderCondition = $this->parent->orderCondition("view_order");
            $lessons = DataBase::select($this->dataTableName, $condition, $orderCondition);

            foreach ($lessons as $key => $value) {
                $mui = current($this->parent->class->mui->getTranslationByCode($value['id'], $lang));
                $lessons[$key]['title'] = $mui['value'];
                $data = $this->getLessonDataByGUID($value['id'], $lang);
                $lessons[$key] = array_merge($lessons[$key], $data);
                if($data['is_hidden']){
                    if( !filter_var( $this->parent->DATA['show_hiden'] , FILTER_VALIDATE_BOOLEAN) )
                        unset($lessons[$key]);
                }
            }

            // append order data to lessons info if rev_api requested
            if (in_array("rev_api", $this->parent->EXTEND_FIELDS))
            {
                $lessons            = $this->parent->class->rev_api->appendDataToLessons($lessons);
            }

            return $lessons;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function getLessonsByChapterCollection($courseID = '', $chaptersGUID = array())
    {
        try {
            // hook up needed classes ////////////////
            $this->parent->hookClass('v2/chapters');//
            $this->parent->hookClass('v2/courses'); //
            $this->parent->hookClass('v2/mui');     //
            //////////////////////////////////////////

            if(empty($chaptersGUID))
                throw new Exception(null,404);

            $courseID = $this->parent->class->courses->getSingleCourseWithDetails($courseID);

            if(empty($courseID))
                throw new Exception(null,404);

            $lang = isset($this->parent->DATA['lang']) ? $this->parent->DATA['lang'] : $courseID['default_lang'];


            $condition['course_chapter_id'] = "IN,('".implode("', '", $chaptersGUID)."')";

            $outputFields = array(
                '*' => array("course_chapter_id","view_order","lesson_type","is_open", "is_preview")
            );
            $extends = array('*' => '*');

            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));
            $orderCondition = $this->parent->orderCondition("view_order");
            $lessons = DataBase::select($this->dataTableName, $condition, $orderCondition);

            if(empty($lessons))
                return array();

            $lessonsGUIDMUI = "";
            foreach ($lessons as $value){
                $lessonsGUIDMUI .= ",".$value['id'];
            }

            $muiCollection = $this->parent->class->mui->getTranslationByCode(trim($lessonsGUIDMUI,","), $lang);
            $mui = [];
            foreach ($muiCollection as $value) {
                $mui[$value['key']] = $value['value'];
            }

            $dataCollection = $this->getLessonDataByGUIDCollection(explode(",",trim($lessonsGUIDMUI,",")), $lang);


            foreach ($lessons as $key => $value) {
                $lessons[$key]['title'] = $mui[$value['id']];
                $lessons[$key] = array_merge($lessons[$key], $dataCollection[$value['id']]);
                if(is_null($lessons[$key])){
                    unset($lessons[$key]);
                    continue;
                }
                if($data['is_hidden']){
                    if( !filter_var( $this->parent->DATA['show_hiden'] , FILTER_VALIDATE_BOOLEAN) )
                        unset($lessons[$key]);
                }
            }

            return $lessons;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function getLessonByID($courseID = '',$chapterID = '', $lessonsGUID = '' )
    {
        try {
            // hook up needed classes ////////////////
            $this->parent->hookClass('v2/chapters');//
            $this->parent->hookClass('v2/courses'); //
            $this->parent->hookClass('v2/mui');     //
            //////////////////////////////////////////

            if(empty($chapterID) || !UserHelper::ValidGUID($lessonsGUID))
                throw new Exception(null,404);


            $courseID = $this->parent->class->courses->getSingleCourseWithDetails($courseID);

            if(empty($courseID))
                throw new Exception(null,404);

            $lang = isset($this->parent->DATA['lang']) ? $this->parent->DATA['lang'] : $courseID['default_lang'];

            $condition['id'] = "'" . $lessonsGUID . "'";
            $condition['course_chapter_id'] = "'" . $chapterID . "'";

            $outputFields = array(
                '*' => array("view_order","lesson_type")
            );
            $extends = array('*' => '*');

            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));
            $orderCondition = $this->parent->orderCondition("view_order");
            $lesson = current(DataBase::select($this->dataTableName, $condition, $orderCondition));

            if(empty($lesson))
                throw new Exception(null,404);

            $mui = current($this->parent->class->mui->getTranslationByCode($lesson['id'], $lang));
            $lesson['title'] = $mui['value'];
            $data = $this->getLessonDataByGUID($lesson['id'], $lang);
            $lesson = array_merge($lesson, $data);
            if($data['is_hidden']){
                if( !filter_var( $this->parent->DATA['show_hidden'] , FILTER_VALIDATE_BOOLEAN) )
                   return null;
            }

            return $lesson;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function getLessonsCollection($courseID = ''){

        /**
         * My be change this is_hidden
         */

            $condition = array(
                'ch.course_id' => "'" . $courseID . "'",
                'ls.course_chapter_id' => 'ch.id',
            );
            $outputFields = array(
                '*' => array("ls.id")
            );
            $extends = array('*' => '*');
            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));
            $orderCondition = $this->parent->orderCondition("ls.view_order");

            return DataBase::select('course_chapter_lessons ls, course_chapters ch', $condition, $orderCondition, false);

    }

    public function createChapterLesson($courseID = '', $chapterGUID = '', $DATA = array())
    {
        try {
            // hook up needed classes ////////////////
            $this->parent->hookClass('v2/chapters');//
            $this->parent->hookClass('v2/courses'); //
            $this->parent->hookClass('v2/mui');     //
            //////////////////////////////////////////

            $acceptPostFieldsChecker = array(
                "*view_order" => (int)$DATA['view_order'],
                "*title" => trim($DATA['title']),
                "*file" => (string)$DATA['file'],
            );

            $Fields = $this->parent->preparePostFields($acceptPostFieldsChecker);
            if (!$Fields) {
                return null;
            }

            $chapter = $this->parent->class->chapters->getSingleCourseChapterByID($courseID,$chapterGUID,true);
            $courseID = $this->parent->class->courses->getSingleCourseWithDetails($courseID);

            if(empty($courseID) || empty($chapter))
                throw new Exception(null,404);

            $lang = isset($this->parent->DATA['lang']) ? $this->parent->DATA['lang'] : $courseID['default_lang'];
            $lessonGUID = UserHelper::CreateGUID();

            $subtype    = null;
            switch ($DATA['lesson_type']) {
                case 'scorm':
                    $type       = 'scorm';
                    break;
                case 'quiz':
                    $type       = 'html';
                    $subtype    = 'quiz';
                    break;
                case 'pdf':
                    $type       = 'html';
                    $subtype    = 'pdf';
                    break;
                case 'image':
                    $type       = 'html';
                    $subtype    = 'image';
                    break;
                default:
                    $type       = 'video';
                    break;
            }

            $acceptPostFields = array(
                "*id" => $lessonGUID,
                "*course_chapter_id" => $chapter['id'],
                "*view_order" => (int)$DATA['view_order'],
                "lesson_type" => $type,
                "lesson_subtype" => $subtype,
                "is_open"     => $DATA['is_open'] === '' ? NULL : (int)$DATA['is_open'],
                "is_preview"  => !empty($DATA['is_preview']) && !empty($DATA['is_open']) ? 1 : 0,
            );

            $Fields = $this->parent->preparePostFields($acceptPostFields);
            if (!$Fields) {
                return null;
            }

            $insert_id = DataBase::insert($this->dataTableName, $Fields);

            if (!$insert_id) {
                throw new Exception(null, 500);
            }

            foreach ($courseID['available_langs'] as $_lang) {
                $this->parent->class->mui->setTranslation($lessonGUID, trim($DATA['title']), $_lang);
                $this->createLessonDataByGUID($lessonGUID, $_lang, $DATA);
            }

            return $lessonGUID;

        } catch (Exception $e) {

            if (!is_null($e->getMessage())) {
                $this->parent->RESPONSE = array("message" => $e->getMessage());
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function updateLessonByID($courseID = '', $chapterGUID = '', $lessonsGUID = '', $DATA = array())
    {
        try {
            // hook up needed classes ////////////////
            $this->parent->hookClass('v2/chapters');//
            $this->parent->hookClass('v2/courses'); //
            $this->parent->hookClass('v2/mui');     //
            //////////////////////////////////////////

            if(empty($chapterGUID) || !UserHelper::ValidGUID($lessonsGUID))
                throw new Exception(null,404);

            $courseID = $this->parent->class->courses->getSingleCourseWithDetails($courseID);

            if(empty($courseID))
                throw new Exception(null,404);

            $lang = isset($DATA['lang']) ? $DATA['lang'] : $courseID['default_lang'];

            $acceptPostFields = array();
            if (isset($DATA['view_order'])) {
                $acceptPostFields['view_order'] = (int)$DATA['view_order'];
            }
            if (isset($DATA['lesson_type'])) {
                $acceptPostFields['lesson_type'] = (string)$DATA['lesson_type'];
            }
            if (isset($DATA['lesson_subtype'])) {
                $acceptPostFields['lesson_subtype'] = (string)$DATA['lesson_subtype'];
            }
            if (isset($DATA['is_open'])) {
                $acceptPostFields['is_open'] = $DATA['is_open'] === '' ?  NULL : (int)$DATA['is_open'];
            }
            $acceptPostFields['is_preview']     = 0;
            if (!empty($DATA['is_preview']) && !empty($DATA['is_open']) ) {
                $acceptPostFields['is_preview'] = 1;
            }
            $Fields = $this->parent->preparePostFields($acceptPostFields);
            $success = DataBase::update($this->dataTableName, $Fields," `id`='" . $lessonsGUID . "' && `course_chapter_id`= '" . $chapterGUID ."'");
            $data = $this->updateLessonDataByGUID($lessonsGUID, $lang, $DATA);
            if(isset($DATA['title']))
                $mui = $this->parent->class->mui->updateTranslationByKey($lessonsGUID, trim($DATA['title']), $lang);

            $o = ($success || $mui || $data);
            if(!$o)
                throw new Exception(null,500);
            return $o;
        } catch (Exception $e) {

            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return false;
        }


    }

    public function deleteLessonByID($chapterGUID = '',$lessonsGUID = '')
    {
        try {
            // hook up needed classes ////////////////
            $this->parent->hookClass('v2/mui');     //
            //////////////////////////////////////////

            $this->deleteAllLessonDataByGUID($lessonsGUID);
            $this->parent->class->mui->deleteTranslation($lessonsGUID);
            $deleted = DataBase::delete($this->dataTableName, " `id`='" . $lessonsGUID . "' && `course_chapter_id`= '" . $chapterGUID ."'");
            return $deleted;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function _deleteLessonsByChapter($courseID = '', $chapterID = '')
    {
        try {
            $deleted = false;
            $ls =  $this->getLessonsByChapter($courseID, $chapterID);
            foreach ($ls as $key => $value) {
                if($this->deleteLessonByID($chapterID,$value['id']))
                    $deleted = true;
            }
            return $deleted;
        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    private function getLessonDataByGUID($lessonGUID = '' , $lang = ''){

            $condition['course_chapter_lesson_id'] = "'" . $lessonGUID . "'";
            $condition['lang'] = "'" . $lang . "'";

            $outputFields = array('*' => '*');
            $extends = array('*' => '*');

            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));
            $data = current(DataBase::select("course_chapter_lessons_data", $condition, $orderCondition));

            unset($data['id']);
            unset($data['course_chapter_lesson_id']);

            return $data;
    }

    private function getLessonDataByGUIDCollection($lessonsGUID = array() , $lang = ''){

            $condition['lang'] = "'" . $lang . "'";
            $condition['course_chapter_lesson_id'] = "IN,('".implode("', '", $lessonsGUID)."')";
            $condition['is_hidden'] = "0";

            if (in_array("hiddenlessons", $this->parent->EXTEND_FIELDS)) {
		$condition['is_hidden'] = "IN,('".implode("', '", [0,1])."')";
            }

            $outputFields = array('*' => '*');
            $extends = array('*' => '*');

            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));
            $data = DataBase::select("course_chapter_lessons_data", $condition, $orderCondition);


            $_data = [];
            foreach ($data as $key => $value) {
                $lessonGUID = $value['course_chapter_lesson_id'];
                unset($value['id']);
                unset($value['course_chapter_lesson_id']);
                $_data[$lessonGUID] = $value;
            }

            return $_data;
    }

    private function createLessonDataByGUID($lessonGUID = '' , $lang = '', $DATA = array()){

            $acceptPostFields = array(
                "*id" => UserHelper::CreateGUID(),
                "*course_chapter_lesson_id" => $lessonGUID,
                "*lang" => $lang,
                "runtime_seconds" => (int)$DATA['runtime_seconds'],
                "*file" => $DATA['file'],
                "is_hidden" => (int)filter_var( $DATA['is_hidden'] , FILTER_VALIDATE_BOOLEAN),
                "videoAspectRatio" => $DATA['videoAspectRatio'],

            );

            $Fields = $this->parent->preparePostFields($acceptPostFields);
            if (!$Fields) {
                return null;
            }

            $insert_id = DataBase::insert("course_chapter_lessons_data", $Fields);

            if (!$insert_id) {
                throw new Exception(null, 500);
            }

            return $lessonGUID;
    }

    private function updateLessonDataByGUID($lessonGUID = '' , $lang = '', $DATA = array()){

        if(empty($lang) || !UserHelper::ValidGUID($lessonGUID))
            return false;

        $acceptPostFields = array();
        if (isset($DATA['runtime_seconds'])) {
            $acceptPostFields['runtime_seconds'] = (int)$DATA['runtime_seconds'];
        }
        if (isset($DATA['file'])) {
            $acceptPostFields['file'] = (string)$DATA['file'];
        }
        if (isset($DATA['is_hidden'])) {
            $acceptPostFields['is_hidden'] = (int)filter_var( $DATA['is_hidden'] , FILTER_VALIDATE_BOOLEAN);
        }
        if (isset($DATA['videoAspectRatio'])) {
                $acceptPostFields['videoAspectRatio'] = (string)$DATA['videoAspectRatio'];
        }
        if (isset($DATA['videoUploaded'])) {
            $acceptPostFields['videoUploaded'] = $DATA['videoUploaded'];
        }
        if (isset($DATA['captionsUploaded'])) {
            $acceptPostFields['captionsUploaded'] = $DATA['captionsUploaded'];
        }

        $Fields = $this->parent->preparePostFields($acceptPostFields);
        if (!$Fields) {
            return false;
        }

        return DataBase::update("course_chapter_lessons_data", $Fields," `course_chapter_lesson_id`='" . $lessonGUID . "' && `lang`= '" . $lang ."'");
    }

    private function deleteLessonDataByGUIDAndLang($lessonGUID = '' , $lang = ''){

        return DataBase::delete("course_chapter_lessons_data", " `course_chapter_lesson_id`='" . $lessonGUID . "' && `lang`= '" . $lang ."'");

    }

    private function deleteAllLessonDataByGUID($lessonGUID = ''){

        return DataBase::delete("course_chapter_lessons_data", " `course_chapter_lesson_id`='" . $lessonGUID . "'");

    }

    public function copyLessons($courseID = '' , $newLang = ''){

        // hook up needed classes ////////////////
        $this->parent->hookClass('v2/chapters');//
        $this->parent->hookClass('v2/courses'); //
        $this->parent->hookClass('v2/mui');     //
        //////////////////////////////////////////

        if(empty($newLang))
            throw new Exception(null,404);

        $courseID = $this->parent->class->courses->getSingleCourseWithDetails($courseID);

        if(empty($courseID))
            throw new Exception(null,404);

        $data = $this->parent->class->chapters->getCourseChaptersByID($courseID['course_ID']);

        foreach ($data as $key => $chapter) {
            $this->parent->class->mui->setTranslation($chapter['id'], $chapter['title'], $newLang);

            unset($this->parent->DATA['lang']);
            $lessons = $this->getLessonsByChapter($courseID['course_ID'], $chapter['id']);

            foreach ($lessons as $key => $lesson) {
                $this->parent->class->mui->setTranslation($lesson['id'], $lesson['title'], $newLang);
                $data1 =  $this->getLessonDataByGUID($lesson['id'],$courseID['default_lang']);

                $a_data = [
                    'runtime_seconds' => $data1['runtime_seconds'],
                    'file' => $data1['file'],
                    'is_hidden' => $data1['is_hidden'],
                    'videoAspectRatio' => $data1['videoAspectRatio']
                ];
                $this->createLessonDataByGUID($lesson['id'], $newLang, $a_data);

            }
        }
    }

    public function getLessonIsOpen($lessonGUID, $courseID, $userGUID) {

        /////////////////////////////////////////
        $this->parent->hookClass('v2/portals');//
        /////////////////////////////////////////

        $is_open            = NULL;
        $view_order         = NULL;

        DataBase::getInstance()->exec_query('SELECT l.id, l.is_open FROM `course_chapter_lessons` as l
                INNER JOIN `course_chapters` as ch ON (ch.id = l.course_chapter_id)
                INNER JOIN `courses` as c ON (c.id = ch.course_id)
                WHERE c.id='.DataBase::getInstance()->quote($courseID).'
                ORDER BY ch.view_order, l.view_order');

        $courseChapterLessons = DataBase::getInstance()->fetchAll();

        foreach($courseChapterLessons as $item => $lesson) {
            if($lesson['id'] == $lessonGUID) {
                $view_order  = $item + 1;
                $is_open     = $lesson['is_open'];
                break;
            }
        }

        //open or closed lesson according `is_open`
        if( !is_null($is_open)) {
            return $is_open == 1;
        }

        //open or closed lesson according `openLessons`, default 3
        $openLessons         = 3;
        if(!is_null($userGUID)) {
            $portalGUID      = $this->parent->class->portals->getPortalGUIDByUserGUID($userGUID);
            $portalConfig    = $this->parent->class->portals->getPortalConfig($portalGUID['portal_id']);
            $openLessons     = $portalConfig['openLessons'] || $portalConfig['openLessons'] == 0 ?  $portalConfig['openLessons'] : $openLessons;
        }

        if( !is_null($view_order) && $view_order <= $openLessons ) {
            return true;
        }

        return false;
    }


    public function checkLessonAccess($lessonGUID, $courseID, $DATA)
    {
        //to exclude old portals
        if(isset($DATA['studentID']) || (MONITORINGTOKEN && $DATA['monitoringToken'] == MONITORINGTOKEN)) {
            return true;
        }

        //////////////////////////////////////////////
        $this->parent->hookClass('v2/students');    //
        $this->parent->hookClass('v2/learningPath');//
        //////////////////////////////////////////////

        $access              = false;
        $userHasAccess       = false;
        $userGUID            = UserHelper::getCurrentUserApiGUID();
        $is_open             = $this->getLessonIsOpen($lessonGUID, $courseID, $userGUID);

        if(!is_null($userGUID)) {
            $student         = $this->parent->class->students->getStudentByUserApiGUID($userGUID);
            $userHasAccess = $this->parent->class->learningPath->hasStudentAccessToCourse($student['id'], $courseID);
        }

        if ($userHasAccess || $is_open) {
            $access = true;
        }

        return $access;
    }
    
    public function uploadLessonFiles($courseID, $lessonGUID,  $DATA){

        $this->parent->hookClass('v2/courses');

        $courses        = $this->parent->class->courses->getSingleCourse($courseID);

        if(empty($courses)){

            throw new \Exception('Course is not found', 412);
        }

        if(!isset($DATA['lang']) || !isset($DATA['fileName'])){

            throw new \Exception('Language or filename not found', 412);
        }

        $uploadedFiles      = [];


        foreach($_FILES as $name => $file){

            switch(strtolower($_FILES[$name]['type']))
            {
                case 'text/vtt':
                    $filePath  = '/opencontent/courses/captions/vtt/'.$courseID.'/'.$DATA['lang'].'/'.$DATA['fileName'];
                    break;

                case 'video/mp4':
                    $filePath  = '/Protected/courses/'.$courseID.'/'.$DATA['lang'].'/'.$DATA['fileName'].'/'.$DATA['fileName'];
                    break;
                default: throw new Exception("Bad file type $name: must be (vtt, mp4)", 501);
            }


            if(!isset($filePath)){

                throw new \Exception(null, 412);
            }


            $result        = $this->uploadLessonFile($name, $filePath);

            if(isset($result['type']) && $result['type'] == 'vtt') {

                $this->updateLessonDataByGUID($lessonGUID, $DATA['lang'], ['captionsUploaded' => date('Y-m-d H:i:s', time())]);
            }


            if(isset($result['type']) && $result['type'] == 'mp4') {

                $data['videoUploaded']     = date('Y-m-d H:i:s', time());

                if(!is_null($result['meta']['trt'])){
                    $data['runtime_seconds']   = $result['meta']['trt'];
                }

                if(!is_null($result['meta']['aspectRatio'])){
                    $data['videoAspectRatio']   = $result['meta']['aspectRatio'];
                }

                $this->updateLessonDataByGUID($lessonGUID, $DATA['lang'], $data);
            }


            $uploadedFiles[] = $result;

        }

        return $uploadedFiles;
    }

    protected function uploadLessonFile($name, $filePath)
    {
        if(empty($_FILES[$name]))
        {
            return null;
        }

        if(!empty($_FILES[$name]['error']))
        {
            throw new Exception('Upload error: '.$_FILES[$name]['error'], 501);
        }

        switch(strtolower($_FILES[$name]['type']))
        {
            case 'text/vtt':   $type      = 'vtt'; break;
            case 'video/mp4':  $type      = 'mp4'; break;
            default: throw new Exception("Bad file type $name: must be (vtt, mp4)", 501);
        }

        $this->parent->hookClass('v2/ftp');

        $result                     = $this->parent->class->ftp->uploadFile($_FILES[$name]['tmp_name'], $filePath.'.'.$type);

        $metaData                   = $this->getVideoFileData($_FILES[$name]['tmp_name']);

        if(!$result)
        {
            throw new Exception("Error create file in the CDN", 503);
        }

        $cacheFlyURL                = KC_CDN_PROTOCOL.'://'.KC_CDN_HOST.$filePath.'.'.$type;

        return
            [
                'name'                  => $name,
                'type'                  => $type,
                'url'                   => $cacheFlyURL,
                'meta'                  => $metaData
            ];
    }

    protected function getVideoFileData($filePath){

        exec("ffmpeg -i ".$filePath." 2>&1 | grep -E 'Stream|Duration'", $output);

        $output = implode(' ', $output);


        preg_match("/\d{2}:\d{2}:\d{2}.\d{2}/", $output, $duration);

        $timeSeconds = null;

        if (isset($duration[0]) && $duration[0] != '') {

            sscanf($duration[0], "%d:%d:%d", $hours, $minutes, $seconds);
            $timeSeconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;

        }


        preg_match("/DAR \d{1,2}:\d{1}/", $output, $aspectRatio);

        $aspectRatioValue = null;

        if (isset($aspectRatio[0]) && $aspectRatio[0] != '') {

            $aspectRatioValue	 = trim(str_replace('DAR', '', $aspectRatio[0]));
        }

        return ['trt' => $timeSeconds, 'aspectRatio' => $aspectRatioValue];
    }


    public function getLessonFiles($courseID, $lessonGUID, $DATA)
    {

        if(!isset($DATA['lang'])) {
            throw new Exception('Lesson lang not define', 412);
        }

        $outputFields = array(
            '*' => array(
                'id',
                'file',
                'videoUploaded',
                'captionsUploaded'
            )
        );

        $extends                 = [];
        $condition               = [];
        $condition['lang']       = "'" . $DATA['lang'] . "'";
        $condition['course_chapter_lesson_id']         = "'" . $lessonGUID . "'";

        DataBase::selectFields($this->parent->outputFields($outputFields, $extends));

        $lesson                  = current(DataBase::select("course_chapter_lessons_data", $condition));

        $result                  = [];

        if(!is_null($lesson['videoUploaded'])){
            $result['video']['uploaded'] = $lesson['videoUploaded'];
            $result['video']['url']      = KC_CDN_PROTOCOL.'://'.KC_CDN_HOST .'/Protected/courses/'. $courseID .'/'
                                           . $DATA['lang'] .'/'. $lesson['file'] . '/' .$lesson['file'] .'.mp4';
        }


        if(!is_null($lesson['captionsUploaded'])){
            $result['vtt']['uploaded']  = $lesson['captionsUploaded'];
            $result['vtt']['url']       = KC_CDN_PROTOCOL.'://'.KC_CDN_HOST. '/opencontent/courses/captions/vtt/'
                                          . $courseID .'/'. $DATA['lang'] .'/'
                                          . $lesson['file'] .'.vtt';
        }

        return $result;

    }

    public function getVideoLessonForConvert() {

        $resolutions = [1080, 720, 480, 360, 240, 'MP3'];

        foreach ($resolutions as $resolution) {

            $query   = "SELECT `cc`.`course_id`, `ccld`.`lang`, `ccld`.`file`, `ccld`.`course_chapter_lesson_id`  
                    FROM 
                      `course_chapters` `cc`, 
                      `course_chapter_lessons` `ccl`, 
                      `course_chapter_lessons_data` `ccld`
                    WHERE 
                      `cc`.`id` = `ccl`.`course_chapter_id`
                    AND
                      `ccl`.`id` = `ccld`.`course_chapter_lesson_id`
                    AND
                       `ccld`.`videoUploaded` IS NOT NULL
                    AND
                       `ccld`.`x$resolution`  IS NULL
                    ORDER BY RAND()
                    LIMIT 1";

            if(!DataBase::getInstance()->exec_query($query))
            {
                throw new Exception('DataBase error', 500);
            }

            $lesson       = current(DataBase::getInstance()->fetchAll());

            if(!$lesson) {

                continue;
            }

            $lesson['resolution']   = $resolution;

            return $lesson;
        }

    }

    public function changeLessonVideoConvertStatus($lessonGUID, $DATA) {

        if(!isset($DATA['resolution']) || !isset($DATA['lang'])){
            throw new Exception('Precondition fail', 412);
        }

        $acceptPostFields                 = [];
        $acceptPostFields['x'.$DATA['resolution']]       = isset($DATA['date']) ? $DATA['date']  : date('Y-m-d H:i:s');

        $Fields = $this->parent->preparePostFields($acceptPostFields);

        if (!$Fields) {
            throw new Exception("Unknown error occured.",500);
        }

        $success   = DataBase::update("course_chapter_lessons_data", $Fields, "`course_chapter_lesson_id`='" . $lessonGUID . "' && `lang` = '".$DATA['lang']."'");

        return $success;
    }

    /**
     * Get Lessons available video resolution 
     * @param $lesson
     * @return array
     * @throws Exception
     */
    public function getLessonVideoResolutions($lesson) {

        if(empty($lesson['id']) || empty($lesson['lang'])){
            throw new Exception('Precondition fail', 412);
        }
        $aResolutions = ['x240', 'x360', 'x480', 'x720', 'x1080', 'xMP3'];
        $query   = "SELECT `x240`, `x360`, `x480`, `x720`, `x1080`, `xMP3`  
                    FROM `course_chapter_lessons_data` 
                    WHERE `course_chapter_lesson_id` = '".$lesson['id']."'
                    AND `lang` = '".$lesson['lang']."'
                    AND `is_hidden` = 0
                   ";

        if(!DataBase::getInstance()->exec_query($query)) {
            throw new Exception('DataBase error', 500);
        }
        
        $lessonResolutions   = current(DataBase::getInstance()->fetchAll());
        $result              = [];
        $noVideo             = strtotime('0000-00-00 00:00:00');
        foreach ($aResolutions as $resolution) {
            $res = $lessonResolutions[$resolution];  
            if(!is_null($res) && strtotime($res) != $noVideo ) {
                $result[] = $resolution;
            }
        }

        return $result;
    }

}
