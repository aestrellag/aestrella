<?php

class mui
{
    public $parent = null;

    private $dataTableName = 'mui';

    function __construct()
    {
    }

    public function getTranslationByCode($code = '', $lang = '', $portal_id = '', $order = 'asc')
    {
        try {

            if (!$lang) {
                return null;
            }

            if($code !== 'all') {

                $codes = explode(',', trim($code));
                foreach ($codes as $k => $code) {
                    if(!trim($code)) unset($codes[$k]);
                    else $codes[$k] = trim($code);
                }

                if (!is_array($codes) || !sizeof($codes)) {
                    return null;
                }
            }

            if(strtolower($order) != 'asc') $order = 'desc';
            // $orderCondition = $this->parent->orderCondition('order', $order);
            $orderCondition = "`order` ".$order.", `portal_id` ASC";

            $outputFields = array(
                '*' => array(
                    'key',
                    'type',
                    'value'
                )
            );
            $extends = array('*' => array('group','lang'));

            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));

            if(isset($this->parent->DATA['groups']))
            {
                $groups             = explode(',', $this->parent->DATA['groups']);
                $condition          = array
                (
                    'lang'          => "'".$lang."'",
                    'group'         => "IN,('".implode("', '", $groups)."')"
                );
            }
            else
            {
                $condition          = array(
                    'lang'          => "'".$lang."'",
                    'key'           => "IN,('".implode("', '", $codes)."')"
                );
            }

            $where                  = '';

            if(isset($this->parent->DATA['excludeGroup'])) {

                $where     = " && (`group` = 'page' || `group` NOT LIKE '".$this->parent->DATA['excludeGroup']."%')";
            } elseif(isset($this->parent->DATA['group'])) {
                $condition['group'] = "'{$this->parent->DATA['group']}'";
            }

            if(isset($this->parent->DATA['portal_id'])) {

                $condition['portal_id'] = "'{$this->parent->DATA['portal_id']}'";

            } else {

                DataBase::addNativeWhereCondition($where." && (`portal_id` is NULL || `portal_id` = '".addslashes($portal_id)."')");
            }

            $result = DataBase::select($this->dataTableName, $condition, $orderCondition, false);
            if($this->parent->DATA['nl2br']){
                foreach ($result as $key => $value) {
                    $result[$key]['value'] = nl2br($value['value']);
                }
            }

            // generation nested format
            if(isset($this->parent->DATA['nested'])) {
                $result             = $this->formatMuiResults($result);
            }

            return $result;


        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return $this->parent->RESPONSE;
        }

    }

    public function setTranslation($key = '', $value = '', $lang = '', $portal_id = NULL, $group = NULL, $type = 'text', $for_portals = 0)
    {
        try {

            $muiGUID = UserHelper::CreateGUID();
            $acceptPostFields = array(
                "*id" => $muiGUID,
                "*key" => $key,
                "*value" => $value,
                "*lang" => $lang,
                "group" => $group,
                "portal_id" => $portal_id,
                "type" => $type,
                "for_portals" => $for_portals,
            );

            $Fields = $this->parent->preparePostFields($acceptPostFields);
            if (!$Fields) {
                return null;
            }

            $insert_id = DataBase::insert($this->dataTableName, $Fields);

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return $this->parent->RESPONSE;
        }

    }

    public function updateTranslationByKey($key = '', $value = '', $lang = '', $data = null)
    {
        try {

            $portalCondition = '';
            if(!empty($data) && isset($data['portal_id']) ) {
                $portalCondition = " AND `portal_id` = '" . $data['portal_id'] . "' ";
            }

            $acceptPostFields['value'] = $value;
            $Fields = $this->parent->preparePostFields($acceptPostFields);
            if (!$Fields) {
                throw new Exception("Unknown error occured.",500);
            }

            $success = DataBase::update($this->dataTableName, $Fields," `key`='" . $key . "' && `lang`= '" . $lang ."'" .$portalCondition);
            if (!$success)
                throw new Exception("Unknown error occured.", 500);

            return true;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return $this->parent->RESPONSE;
        }

    }

    public function updateTranslationByKeyAndPortal($portalGUID, $langCODE, $DATA)
    {
        if (!$portalGUID) {
            throw new Exception('$portalGUID is undefined', 412);
        }

        if (!$langCODE) {
            throw new Exception('LangCode is undefined', 412);
        }

        if (!$DATA || empty($DATA)) {
            throw new Exception('Data is not valid', 412);
        }

        $portalGUID  = $portalGUID == 'default' ? NULL :  $portalGUID;

        $updated = []; $failed = []; $created = []; $deleted = [];

        foreach($DATA['data']['mui'] as $key => $value ){

           $success = $this->updateTranslationByKey($key, $value, $langCODE, ['portal_id' => $portalGUID]);

            if (!$success) {

                $failed[] = $key;
                continue;
            }

            $updated[] = $key;

        }

        foreach($DATA['data']['new'] as $key => $value ){

            $success = $this->setTranslation($value['key'], $value['value'], $langCODE, $portalGUID, $value['group'], $value['type'], 1);

            if (!$success) {

                $failed[] = $key;
                continue;
            }

            $created[] = $key;

        }

        foreach($DATA['data']['delete'] as $key => $value ){

            $success = $this->deleteTranslation($key, $langCODE, $portalGUID);

            if (!$success) {

                $failed[] = $key;
                continue;
            }

            $deleted[] = $key;

        }


        return ['updated' => $updated, 'created' => $created, 'deleted' => $deleted,   'failed' => $failed];

    }

    public function deleteTranslation($key = '', $lang = null, $portalGUID = null)
    {
        try {


            $condition = "`key`='" .$key. "'";

            if(!is_null($lang)){

                $condition .= " AND `lang`='" .$lang. "'";

            }

            if(!is_null($portalGUID)){

                $condition .= " AND `portal_id`='" .$portalGUID. "'";

            }

            return DataBase::delete($this->dataTableName, $condition);

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return $this->parent->RESPONSE;
        }

    }

    public function copyPortalMui($portalGUIDAcceptor, $portalGUIDSource, $languages = array(), $group = null)
    {
        if (!$portalGUIDAcceptor) {
            throw new Exception('portalGUIDAcceptor is undefined', 412);
        }

        if (!$portalGUIDSource) {
            throw new Exception('portalGUIDSource is undefined', 412);
        }

        if ($portalGUIDSource == 'default') {
            $portalGUIDSource = null;
        }

        $outputFields = array(
            '*' => array(
                'id',
                'group',
                'key',
                'type',
                'value'
            )
        );
        $extends    = [];

        $copied     = [];


        if(!is_array($languages)) {

            $languages = array(
                array('lang' => $languages )
            );
        }

        foreach ($languages as $lang ) {

            $condition              = array();
            $condition['lang']      = "'" . $lang['lang'] . "'";

            if(!is_null($group)) {
                $condition['group']      = "'" . $group . "'";
            }

            if(is_null($portalGUIDSource)) {

                $condition['for_portals']      = 1;
                $condition['portal_id']        = "IS, NULL";

            } else {
                $condition['portal_id']         = "'" . $portalGUIDSource . "'";
            }

            $orderCondition         = null;

            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));
            $portalAcceptorMUI = DataBase::select("`mui`", $condition, $orderCondition);

            if(!count($portalAcceptorMUI)) {
                continue;
            }

            foreach($portalAcceptorMUI as $i => $row){

                $this->setTranslation($row['key'], $row['value'], $lang['lang'], $portalGUIDAcceptor, $row['group'], $row['type']);
            }

            $copied[] = $lang['lang'];
        }

        return $copied;
    }

    /**
     * The method converts an array of nested arrays
     *
     * @param $raw
     *
     * @return array
     */
    protected function formatMuiResults($raw) {

        //exclude duplicates from list
        $list                       = [];

        foreach($raw as $item){
            $list[$item['key']]     = $item;
        }

        $tree                       = [];

        //build tree from list
        foreach($list as $item) {

            $path                   = explode('-', $item['key']);

            $node                   =& $tree;

            foreach($path as $key)
            {
                if(!is_array($node))
                {
                    $node           = [$node];
                }

                if(!isset($node[$key]))
                {
                    $node[$key]     = [];
                }

                $node               =& $node[$key];
            }

            if(isset($item['type']) && $item['type'] === 'json')
            {
                $value              = json_decode($item['value']);
            }
            else
            {
                $value              = $item['value'];
            }

            if(!empty($node))
            {
                if(is_array($node))
                {
                    $node[0]        = $value;
                }
                else
                {
                    $node           = $value;
                }
            }
            else
            {
                $node               = $value;
            }
       	}

       	return $tree;
    }

    /**
     * Select all translation groups by portals, language, for_portal attribute.
     *
     * @param $DATA
     * @return mixed
     * @throws Exception
     */
    public function getAllMuiGroups($DATA)
    {
        $where = 'WHERE 1';

        if(isset($DATA['portal_id'])) {

            $where .= " AND `portal_id` = '". $DATA['portal_id'] ."'" ;

        }
        if(isset($DATA['lang'])) {

            $where .= " AND `lang` = '". $DATA['lang'] ."'" ;

        }
        if(isset($DATA['for_portals'])) {

            $where .= " AND `for_portals` = '". $DATA['for_portals'] ."'" ;

        }

        $query                      = "SELECT DISTINCT `group` FROM `mui` " . $where;

        if(!DataBase::getInstance()->exec_query($query))
        {
            throw new Exception('DataBase error', 500);
        }

        $result                     = DataBase::getInstance()->fetchAll();

        return $result;
    }

    /**
     * Select translations with portal_id NULL by lang, groups,for_portal attribute.
     *
     * @param $DATA - can consist lang, groups, for_portals
     * @return array
     */
    public function getDefaultMui($DATA)
    {
       try {

           $where = ' WHERE `portal_id` is NULL ';


           if(isset($DATA['lang'])) {

               $where .= " AND `lang` = '". $DATA['lang'] ."'" ;

           }

           if(isset($DATA['groups']))
           {

               if(is_array($DATA['groups'])){
                   $group = array_map(function($item){
                       return "'$item'";
                   }, $DATA['groups']);
                   $where .= " AND `group` IN (".implode(",", $group).")";
               }else{
                   $where .= " AND `group` IN ('".implode("','", [$DATA['groups']])."')";
               }

           }

           if(isset($DATA['for_portals'])) {

               $where .= " AND `for_portals` = '". $DATA['for_portals'] ."'" ;

           }

           $query                      = "SELECT `key`,`type`, `value`, `group`, `lang` FROM `mui` " . $where;

           if(!DataBase::getInstance()->exec_query($query))
           {
               throw new Exception('DataBase error', 500);
           }

           $result                     = DataBase::getInstance()->fetchAll();

           if($this->parent->DATA['nl2br']){
                foreach ($result as $key => $value) {
                    $result[$key]['value'] = nl2br($value['value']);
                }
            }

            // generation nested format
            if(isset($this->parent->DATA['nested'])) {
                $result             = $this->formatMuiResults($result);
            }

            //group by langs
            if(in_array('multilang', $this->parent->EXTEND_FIELDS)){
               $result              = $this->groupByLang($result);
            }

            return $result;


        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return $this->parent->RESPONSE;
        }
    }

    public function getMuiByKey($key, $portalId = null){

        $muiGroup                   = strstr ( $key, '-', true);
        $muiKey                     = trim(strstr ( $key, '-'), '-');

        $condition                  = [
            '`group` = '.DataBase::getInstance()->quote($muiGroup),
            '`key` = '.DataBase::getInstance()->quote($muiKey)
        ];

        if(is_null($portalId)){
            $condition[]            = '`portal_id` IS NULL';
        }
        else{
            if(!UserHelper::ValidGUID($portalId)){
                throw new ErrorException('Invalid portal GUID', 412);
            }
            $condition[]            = "`portal_id` = '$portalId'";
        }

        $condition                  = join( ' && ', $condition);

        DataBase::query("SELECT lang, type, value FROM mui WHERE $condition");

        return DataBase::getInstance()->fetchAll();
    }

    public function updateMuiByKey($key, $data, $portalId = null){

        $muiGroup                   = strstr ( $key, '-', true);
        $muiKey                     = trim(strstr ( $key, '-'), '-');

        $condition                  = [
            '`group` = '.DataBase::getInstance()->quote($muiGroup),
            '`key` = '.DataBase::getInstance()->quote($muiKey)
        ];

        if(is_null($portalId)){
            $condition['portal_id'] = '`portal_id` IS NULL';
        }
        else{
            if(!UserHelper::ValidGUID($portalId)){
                throw new ErrorException('Invalid portal GUID', 412);
            }
            $condition['portal_id'] = "`portal_id` = '$portalId'";
        }

        if(!empty($data['translate']) && is_array($data['translate'])){

            foreach($data['translate'] as $lang => $translate){
                $lang               = DataBase::getInstance()->quote($lang);
                $translate          = DataBase::getInstance()->quote($translate);

                $updateCondition    = array_merge($condition, ["`lang` = $lang"]);

                //find available translate

                $sql = 'SELECT count(id) AS c FROM mui WHERE '.join(' && ', $updateCondition);
                DataBase::query($sql);
                $counter            = current(DataBase::getInstance()->fetchAll())['c'];
                if($counter == 0){
                    //error update,  create new record

                    $updateCondition[]              = "id = '".UserHelper::CreateGUID()."'";
                    $updateCondition[]              = "value = $translate";
                    $updateCondition[]              = "type = 'text'";

                    if(is_null($portalId)){
                        $updateCondition['portal_id']   = '`portal_id` = null';
                    }
                    else{
                        $updateCondition['portal_id']   = "`portal_id` = '$portalId'";
                    }

                    $sql = 'INSERT mui SET '.join(', ', $updateCondition);
                    if(!DataBase::query($sql)){
                        throw new ErrorException('Error create new translite', 500);
                    }
                }
                else{

                    $sql = 'UPDATE mui SET `value` = '.$translate.' WHERE '.join(' && ', $updateCondition);
                    DataBase::query($sql);
                }
            }
        }

        return true;

    }

    private function groupByLang($res){
        $out                        = [];

        foreach($res as $item){
            $key                    = $item['group'] .'-'. $item['key'];

            if(empty($out[$key])){
                $out[$key]          = $item;
                $out[$key]['key']   = $key;
                $out[$key]['value'] = [];
                unset($out[$key]['lang']);
            }

            $out[$key]['value'][$item['lang']] = $item['value'];
        }

        return $out;

    }

    public function getMUIByFilter($filter){

        $where                      = [];

        /*
         * it is not recommended to use a language filter,
         * if you want to always receive only one language, use another function
         */
        if(isset($filter['lang'])) {
            $lang                   = DataBase::getInstance()->quote($filter['lang']);
            $where[]                = "lang = $lang" ;
        }

        if(isset($filter['groups']))
        {

            if(!is_array($filter['groups'])) {
                $filter['groups'] = explode(',', $filter['groups']);
            }

            $groups = array_map(function ($item) {
                return DataBase::getInstance()->quote($item);
            }, $filter['groups']);

            switch(count($groups)){
                case 0:
                    //don't need add rule
                    break;
                case 1:
                    $where[]        = "`group` = ". join('', $groups);
                    break;
                default:
                    $where[]        = "`group` IN (". join(",", $groups) .")";
                    break;
            }

        }

        if(isset($filter['search'])){
            $search                 = DataBase::getInstance()->quote("%{$filter['search']}%");
            $where[]                = '('.join(' || ', [
                    "`value` LIKE $search",
                    "`key` LIKE $search",
                    "`group` LIKE $search"
                ]).')';
        }

        if(empty($where)){
            $where                  = '';
        }else{
            $where                  = 'WHERE '.join(' && ', $where);
        }

        $sql                        = "SELECT mui.`group`, mui.`key`, mui.`value`, mui.`lang` FROM mui
                                        INNER JOIN (SELECT `key`, `group` FROM `mui` $where) AS f 
                                        WHERE `mui`.`key` = `f`.`key` && `mui`.`group` = `f`.`group`";

        if(!DataBase::getInstance()->exec_query($sql))
        {
            return [];
        }

        $result                     = DataBase::getInstance()->fetchAll();

        //group by langs
        if(in_array('multilang', $this->parent->EXTEND_FIELDS)){
            $result              = $this->groupByLang($result);
        }

        //inset pagination count
        if(in_array('pagination', $this->parent->EXTEND_FIELDS))
        {
            $this->parent->TOTAL    = count($result);
            $result                 = array_splice($result, $this->parent->START, $this->parent->LIMIT);
        }

        return $result;
    }

    public function updateMuiCollection($collection, $portalId = null){

        if(is_array($collection)){
            foreach($collection as $key => $item){

                $muiGroup                   = DataBase::getInstance()->quote(strstr ( $key, '-', true));
                $muiKey                     = DataBase::getInstance()->quote(trim(strstr ( $key, '-'), '-'));

                foreach($item['value'] as $lang => $val){

                    $lang           = DataBase::getInstance()->quote($lang);
                    $val            = DataBase::getInstance()->quote($val);

                    $where = $fields = [
                      "`group` = $muiGroup",
                      "`key` = $muiKey",
                      "`lang` = $lang"
                    ];

                    if(empty($portalId)){
                        $where[]    = '`portal_id` IS NULL';
                    }
                    else{
                        $portalId   = DataBase::getInstance()->quote($portalId);
                        $where[]    = "`portal_id` = $portalId";
                    }

                    $where          = join(' && ', $where);

                    //find available a mui record

                    DataBase::query("SELECT id FROM mui WHERE $where");
                    $result         = DataBase::getInstance()->fetchAll();

                    if(empty($result)){
                        $fields[]   = "id = '".UserHelper::CreateGUID()."'";
                        $fields[]   = "value = $val";
                        $sql = 'INSERT mui SET '.join(', ', $fields);
                        if(!DataBase::query($sql)){
                            throw new ErrorException('Error create new translite', 500);
                        }
                    }else{
                        $muiId      = current($result)['id'];

                        DataBase::query("UPDATE mui SET value = $val WHERE id = '$muiId'");
                    }
                }
            }
        }

    }

    /**
     * @param   array       $data
     *
     * @return  array
     * @throws  Exception
     */
    public function getSingleMui(array $data): array
    {
        $portal_id                  = $data['portal_id'] ?? null;
        $id                         = $data['id'] ?? null;
        $group                      = $data['group'] ?? null;

        if(empty($portal_id) || empty($id) || empty($group))
        {
            throw new Exception(null, 412);
        }

        // get mui and name of list
        $results                    = DataBase::select
        (
            'mui',
            [
             'group'                => "'$group'",
             'key'                  => "'$id'",
             'portal_id'            => "'$portal_id'"
            ]
        );

        return $results;
    }

    /**
     * @param   array       $data
     *
     * @return  array
     * @throws  Exception
     */
    public function updateSingleMui(array $data): array
    {
        $portal_id                  = $data['portal_id']    ?? null;
        $id                         = $data['id']           ?? null;
        $mui                        = $data['mui']          ?? null;
        $group                      = $data['group']        ?? null;

        if(empty($portal_id) || empty($id) || empty($group))
        {
            throw new Exception(null, 412);
        }

        if(!is_array($mui))
        {
            throw new Exception('mui must be an array', 412);
        }

        $result                     = [];

        // update or create mui records
        foreach($mui as $lang => $item)
        {
            $where                      = ['key' => "'$id'", 'group' => "'$group'", 'lang' => "'$lang'", 'portal_id' => "'$portal_id'"];

            $mui                        = current(DataBase::select('mui', $where));

            if(empty($mui))
            {
                $new_id                 = UserHelper::CreateGUID();
                $result[]               = $new_id;

                $query                  = "INSERT INTO `mui` SET 
    `id` = '$new_id', `portal_id`='$portal_id', `lang` = '$lang', `key` = '$id', 
    `value` = '$item', `type` = 'text', `group` = '$group'";

                if(!DataBase::getInstance()->exec_query($query))
                {
                    throw new Exception('DataBase error', 500);
                }
            }
            else
            {
                $result[]               = $mui['id'];

                if(!DataBase::update('mui', ['value' => $item], "`id`='{$mui['id']}'"))
                {
                    throw new Exception('DataBase error', 500);
                }
            }

        }

        return $result;
    }
}
