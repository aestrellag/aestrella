<?php

class teachers
{
    public $parent = null;

    private $dataTableName = 'teachers';

    function __construct()
    {
    }

    public function getTeachersCollection($DATA)
    {
        try {

            $outputFields = array(
                '*' => array(
                    'id',
                    'meta_data'
                )
            );

            if (in_array("files", $this->parent->EXTEND_FIELDS)) {

                $outputFields['*'][] = 'files';
            }

            $condition          = [];

            if(!empty($DATA['account_id']))
            {
                $condition['account_id'] = DataBase::getInstance()->quote($DATA['account_id']);
            }

            $orderCondition     = $this->parent->orderCondition();
            $extends            = [];
            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));
            $teachersGUIDCollection = DataBase::select($this->dataTableName, $condition, $orderCondition);

            $outputFields = array(
                '*' => array(
                    'teacher_id',
                    'full_name',
                    'lang'
                )
            );
            $extends = array('*' => array('bio_html'));

            $teachers = array();

            if (in_array("courses", $this->parent->EXTEND_FIELDS)) {
                $allCourses = $this->getAllTeachersCourses();
            }

            $condition         =   [];

            if(!empty($DATA['lang'])) {
                $condition['lang'] = "'" . $DATA['lang'] . "'";
            }

            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));
            $result = DataBase::select('`teachers_data`', $condition, $orderCondition);

            $teachersData        = [];
            foreach ($result as $teacher){
                $teachersData[$teacher['teacher_id']][] = ['full_name' => $teacher['full_name'], 'lang' => $teacher['lang']];
            }

            foreach ($teachersGUIDCollection as $i => $val) {

                if(!empty($DATA['search'])){

                    $searched                   = [];
                    foreach ( $teachersData[$val['id']] as $langTeacher){

                        $search = strpos(strtolower($langTeacher['full_name']), strtolower($DATA['search']));
                        if ($search === false) {
                            continue;
                        }

                        $searched[] = 1;
                    }

                    if(!count($searched)){
                        unset($teachersGUIDCollection[$i]);
                        continue;
                    }


                }

                $data =  $teachersData[$val['id']];

                if (in_array("meta_data", $this->parent->EXTEND_FIELDS)) {
                    $data['meta_data']          = json_decode($val['meta_data'], true);
                }
                if (in_array("courses", $this->parent->EXTEND_FIELDS)) {
                    $data['courses']            = $allCourses[$val['id']];
                    $data['coursesCount']       = count($allCourses[$val['id']]);
                }
                if (in_array("files", $this->parent->EXTEND_FIELDS) && isset($val['files']) ) {
                    $data['files']              = $val['files'];
                }
                $teachers[$val['id']]           = $data;

            }

            if(!empty($DATA['orderby']) &&   !empty($DATA['order'])) {
                $order      = $DATA['order'] == 'desc' ? SORT_DESC : SORT_ASC;
                $teachers   = $this->teacherSortByColumn($teachers, $DATA['orderby'], $order);
            }



            return $teachers;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function getTeachersByGUID($teachersGUID = '', $DATA)
    {
        try {
            if(!UserHelper::ValidGUID($teachersGUID))
                throw new Exception(null,404);

            $condition = array();
            $condition['teacher_id'] = "'" . $teachersGUID . "'";
            if(!empty($DATA['lang']))
                $condition['lang'] = "'" . $DATA['lang'] . "'";

            $outputFields = array(
                '*' => '*'
            );
            $extends = array();

            $orderCondition = $this->parent->orderCondition('lang');
            DataBase::selectFields($this->parent->outputFields($outputFields, $extends));
            $data = DataBase::select('`teachers_data`', $condition, $orderCondition);

            if(empty($data))
                throw new Exception(null,404);

            if (in_array("courses", $this->parent->EXTEND_FIELDS)) {

                $query                      = "SELECT `tc`.`course_id`, `cd`.`lang`, `cd`.`title` 
                FROM `teachers_courses` `tc`, `course_details` `cd` 
                WHERE `tc`.`course_id` = `cd`.`course_id` 
                AND `tc`.`teacher_id` = '".$teachersGUID."'
                ORDER BY `tc`.`course_id` ASC";

                if(!DataBase::getInstance()->exec_query($query))
                {
                    throw new Exception('DataBase error', 500);
                }

                $result                     = DataBase::getInstance()->fetchAll();

                $courses        = [];
                foreach ($result as $course){
                    $courses[$course['course_id']][]    = [$course['lang'], $course['title']];
                }

                $data['courses'] = $courses;
            }

            foreach ($data as $key => $val){
                unset($data[$key]['id']);
                unset($data[$key]['teacher_id']);
            }

            return $data;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }
    }

    public function getTeachersByCourses($course = array(), $showGUID = false, $lang = null)
    {
        try {

            if (empty($course)) {
                return array();
            }

            $default_langs = array();
            $condition = array();
            $coursesId = array();
            foreach ($course as $val) {
                $condition['course_id'] .= "'" . $val['id'] . "',";
                $coursesId[] = $val['id'];
                $default_langs[$val['id']] = $val['default_lang'];
            }
            $condition['course_id'] = "IN,('" . implode("','", $coursesId) . "')";
            $orderCondition = $this->parent->orderCondition();
            $relations = DataBase::select("teachers_courses", $condition, $orderCondition);


            $conditionTeacher = array();
            foreach ($relations as $key => $val) {
                $conditionTeacher['id'] .= "'" . $val['teacher_id'] . "',";
                $relations[$key]['default_lang'] = $default_langs[$val['course_id']];
            }
            $conditionTeacher['id'] = "IN,(" . rtrim($conditionTeacher['id'], ",") . ")";
            $orderCondition = $this->parent->orderCondition();
            $teachers = DataBase::select($this->dataTableName, $conditionTeacher, $orderCondition);

            $available_teachers = array();
            foreach ($teachers as $item)
                $available_teachers[] = $item['id'];

            $teachersD = array();
            foreach ($relations as $key => $val) {
                if(in_array($val['teacher_id'],$available_teachers))
                    $conditionTeacherData = array();
                $conditionTeacherData['teacher_id'] = "'" . $val['teacher_id'] . "'";

//                if($lang !== null)
//                {
//                    $conditionTeacherData['lang'] = "'" . (is_null($lang) ? $val['default_lang'] : $lang) . "'" ;
//                }

                $orderCondition = $this->parent->orderCondition();

                $langData   = DataBase::select("`teachers_data`", $conditionTeacherData, $orderCondition);

                $default_lang = is_null($lang) ? $val['default_lang'] : $lang;
                $teacherInfo = null;

                foreach($langData as $k => $info)
                {
                    if($info['lang'] === $default_lang)
                    {
                        $teacherInfo = $info;
                        unset($langData[$k]);
                    }
                    if($info['lang'] === 'en')
                    {
                        $teacherInfoEN = $info;
                        unset($langData[$k]);
                    }
                }

                if(empty($teacherInfo))
                {
                    $teacherInfo = $teacherInfoEN;
                }

                $langs          = [];

                foreach($langData as $info)
                {
                    unset($info['teacher_id']);
                    unset($info['id']);

                    $tmpLang      = $info['lang'];

                    unset($info['lang']);

                    $langs[$tmpLang] = $info;
                }

                $teacherInfo['langs'] = $langs;

                /*
                if(!$teacherInfo['teacher_id']){
                    $conditionTeacherData['lang'] = "'" . ($conditionTeacherData['lang']!=$val['default_lang'] ? $val['default_lang'] : 'en') . "'" ;
                    $teacherInfo = current(DataBase::select("`teachers_data`", $conditionTeacherData, $orderCondition));
                }
                */

                $teachersD[] = $teacherInfo;
            }




            $res = array();
            foreach ($relations as $valRel) {
                foreach ($teachersD as $valT) {
                    if ($valRel['teacher_id'] == $valT['teacher_id']) {
                        $res[$valRel['course_id']] = $valT;
                        if (!$showGUID) {
                            unset($res[$valRel['course_id']]['id']);
                        }
                    }
                }
            }

            return $res;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            if (!empty($msg)) {
                $this->parent->RESPONSE = array("message" => $msg);
            }
            $this->parent->HTTP_CODE = $e->getCode();
            return null;
        }

    }

    public function createTeacherCourseRel($teachersGUID ,$couserID){
        $guid = UserHelper::CreateGUID();
        $acceptPostFields = array(
            "*id" => $guid,
            "*teacher_id" => $teachersGUID,
            "*course_id" => $couserID
        );

        $Fields = $this->parent->preparePostFields($acceptPostFields);
        if (!$Fields) {
            return null;
        }

        $insert_id = DataBase::insert("`teachers_courses`", $Fields);

        return $insert_id ? true : false ;
    }

    public function updateTeacherCourseRel($teachersGUID ,$couserID){

        if (!UserHelper::ValidGUID($teachersGUID) || empty($couserID)) {
            return null;
        }

        $Fields = $this->parent->preparePostFields(array("*teacher_id" => $teachersGUID));
        if (!$Fields) {
            return null;
        }

        $condition['course_id'] = "'".$couserID."'";
        $orderCondition = $this->parent->orderCondition();
        $relations = DataBase::select("teachers_courses", $condition, $orderCondition);

        if(empty($relations)){
            $insert = $this->createTeacherCourseRel($teachersGUID ,$couserID);

            return $insert ? true : false ;
        }else{
            $update = DataBase::update("`teachers_courses`", $Fields, "`course_id`='" . $couserID . "'");

            return $update ? true : false ;
        }


    }

    private function getAllTeachersCourses(){

        $courses                = [];
        $condition              = [];
        $orderCondition         = $this->parent->orderCondition();
        $coursesObject          = DataBase::select("teachers_courses", $condition, $orderCondition);

        if(empty($coursesObject)){
              $courses = [];
        }

        foreach ($coursesObject as $course){
            $courses[$course['teacher_id']][]     = $course['course_id'];
        }

        return $courses;

    }

    private function teacherSortByColumn($array, $column, $dir = SORT_ASC) {

        $orderArray                   = [];

        if($column == 'full_name'){

            foreach ($array as $key=> $row) {

                $fullName = null;

                foreach ($row as $i => $lang) {
                    if($lang['lang'] == 'en'){
                        $fullName = $lang['full_name'];
                    }
                }

                $orderArray[$key] = $fullName;
            }

        } else {

            foreach ($array as $key=> $row) {
                $orderArray[$key] = $row[$column];
            }
        }

        array_multisort($orderArray, $dir, $array);

        return $array;
    }

    public function updateTeachersByGUID($DATA){

        try {

            $created        = [];

            $updated        = [];

            foreach($DATA['data'] as $teacherGUID => $teacher){

                if( isset($teacher['action']) && $teacher['action'] == 'new' ) {

                    $created[$teacherGUID]  = $this->createTeacher($teacher);

                    continue;
                }

                if(isset($teacher['meta_data'])){

                    $metaData = [];

                    foreach ($teacher['meta_data'] as $key => $value){

                        $metaData[$key]     = $value;

                    }

                     $acceptPostFields       = array(
                         "meta_data"     => json_encode($metaData)

                     );


                     $Fields = $this->parent->preparePostFields($acceptPostFields);
                     if (!$Fields) {
                            throw new Exception(null, 500);
                     }


                     $updated = DataBase::update("`teachers`", $Fields," `id`='" . $teacherGUID . "' ");
                }

                $updated[$teacherGUID]      = $this->updateTeacherDataByGUID($teacherGUID, $teacher);

            }

            return ['updated' => $updated, 'created' => $created];

        } catch (Exception $e) {

            return array(
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            );
        }

    }

    public function createTeacher($DATA)
    {
        try {
            if(!isset($DATA['langs'])) {
                throw new Exception('Teacher`s data not found', 412);
            }

            $teacherGUID             = UserHelper::CreateGUID();

            $meta_data              = $DATA['meta_data'] ?? null;

            if(!is_array($meta_data))
            {
                $meta_data          = [];
            }

            $meta_data              = json_encode($meta_data);

            $acceptPostFields       = array(
                "*id" => $teacherGUID,
                "meta_data" => $meta_data
            );

            if(!empty($DATA['account_id']))
            {
                $acceptPostFields['account_id'] = $DATA['account_id'];
            }

            $Fields = $this->parent->preparePostFields($acceptPostFields);
            if (!$Fields) {
                throw new Exception(null, 500);
            }

            $insert_id = DataBase::insert($this->dataTableName, $Fields);

            if (!$insert_id) {
                throw new Exception(null, 500);
            }

            foreach ($DATA['langs'] as $lang => $value) {

                $inserted = $this->createTeacherData($teacherGUID,$lang, $value);

                if (!$inserted) {
                    throw new Exception(null, 500);
                }

            }

            return $teacherGUID;

        } catch (Exception $e) {

            return array(
                "message" => $e->getMessage(),
                "code" => $e->getCode()
            );
        }
    }


    public function updateTeacherDataByGUID($teacherGUID, $DATA){

        $teacher =  $this->getTeachersByGUID($teacherGUID, []);

        if(empty($teacher)) {
            throw new Exception('Teacher not found', 404);
        }

        $updated = [];         $inserted = [];

        foreach ($DATA['langs'] as $lang => $value) {


            $acceptPostFields       = array(
                "full_name"     => $value['full_name'],
                "bio_html"      => $value['biography'],
                "description"   => $value['description']
            );

            //add new language data
            if(isset($value['action']) && $value['action'] == 'new'){

                $inserted[] = $this->createTeacherData($teacherGUID,$lang, $value);
                continue;
            }

            //update language data
            $Fields = $this->parent->preparePostFields($acceptPostFields);
            if (!$Fields) {
                throw new Exception(null, 500);
            }

            $updated[$lang] = DataBase::update("`teachers_data`", $Fields," `teacher_id`='" . $teacherGUID . "' && `lang`='" . $lang . "'");

        }

        return $updated;
    }

    public function createTeacherData($teacherGUID, $lang, $DATA){

        if(!isset($lang) || !isset($DATA['full_name'])){
            throw new Exception(null, 412);
        }

        $condition['lang']          = "'" . $lang . "'";
        $condition['teacher_id']    = "'" . $teacherGUID . "'";

        $teachersData               = current(DataBase::select("teachers_data", $condition));

        if(!empty($teachersData)){
            throw new Exception(null, 412);
        }

        $id         = UserHelper::CreateGUID();

        $acceptPostFields       = array(
            "*id"           => $id,
            "teacher_id"    => $teacherGUID,
            "lang"          => $lang,
            "full_name"     => $DATA['full_name'],
            "bio_html"      => $DATA['biography'],
            "description"   => $DATA['description']
        );

        $Fields = $this->parent->preparePostFields($acceptPostFields);
        if (!$Fields) {
            throw new Exception(null, 500);
        }

        $inserted_data_id = DataBase::insert('`teachers_data`', $Fields);

        if (!$inserted_data_id) {
            throw new Exception(null, 500);
        }

        return $id;

    }


    public function uploadTeachersFiles($teacherGUID)
    {
        $teacher                     = $this->getTeachersByGUID($teacherGUID);

        if(empty($teacher))
        {
            throw new \Exception('Teacher not found', 401);
        }

        $files = [];

        foreach($_FILES as $name => $file){

            $filePath   = null;
            $randName   = null;

            if($name == 'document') {
                $randName =  UserHelper::CreateGUID();
            }

            $file = $this->uploadTeacherFile($name, $teacherGUID, $filePath, $randName);

            if($name == 'avatar') {
                $avatar 	=  $file;
                continue;
            }

            $files[$name] = $file;
        }

        $teacherFiles              = $this->getTeacherFiles($teacherGUID);

        if(!is_array($teacherFiles))
        {
            $teacherFiles          = [];
        }

        if(isset($avatar['url']))
        {
            $teacherFiles['avatar'] = $avatar['url'];
        }

        if(!isset($teacherFiles['files'])) {
            $teacherFiles['files']  = [];
        }

        if(count($files)) {
            foreach ($files as $file) {
                $teacherFiles['files'][] = $file['url'];
            }
        }


        $this->saveTeacherFiles($teacherGUID, $teacherFiles);

        return $teacherFiles;
    }

    protected function uploadTeacherFile($name, $teacherGUID, $filePath=null, $randName=null)
    {
        if(empty($_FILES[$name]))
        {
            return null;
        }

        if(!empty($_FILES[$name]['error']))
        {
            throw new Exception('Upload error: '.$_FILES[$name]['error'], 501);
        }

        switch(strtolower($_FILES[$name]['type']))
        {
            case 'image/gif':   $type     = 'gif'; break;
            case 'image/jpeg':  $type     = 'jpg'; break;
            case 'image/png':   $type     = 'png'; break;
            case 'image/x-icon': $type     = 'ico'; break;
            case 'image/vnd.microsoft.icon': $type  = 'ico'; break;

            default: throw new Exception("Bad file type $name: must be image (gif, jpeg, png, ico)", 501);
        }

        $randName   = is_null($randName) ? $name : $randName;

        $file = "opencontent/teachers/$teacherGUID/assets/images/$randName.$type";


        $this->parent->hookClass('v2/ftp');

        $result                     = $this->parent->class->ftp->uploadFile($_FILES[$name]['tmp_name'], $file);

        if(!$result)
        {
            throw new Exception("Error create file in the CDN", 503);
        }

        $cacheFlyURL                = KC_CDN_PROTOCOL.'://'.KC_CDN_HOST.'/'.$file;

        return
            [
                'name'                  => $name,
                'url'                   => $cacheFlyURL
            ];
    }

    private function getTeacherFiles($teacherGUID){

        $outputFields = array(
            '*' => array(
                'id',
                'files'
            )
        );
        $extends                 = [];
        $condition               = [];
        $condition['id']         = "'" . $teacherGUID . "'";
        $orderCondition          = $this->parent->orderCondition();

        DataBase::selectFields($this->parent->outputFields($outputFields, $extends));

        $result                  = current(DataBase::select("teachers", $condition, $orderCondition));

        $files                  = json_decode($result['files'], true);

        if(empty($files)){
            $files = [];
        }

        return $files;

    }

    private function saveTeacherFiles($teacherGUID, array $teacherFiles)
    {
        $teacherFiles              = json_encode($teacherFiles);

        if(empty($teacherFiles))
        {
            throw new Exception('teacherFiles error', 500);
        }

        $acceptPostFields          =
            [
                "*files"               => $teacherFiles
            ];

        $Fields                     = $this->parent->preparePostFields($acceptPostFields);

        if (!$Fields)
        {
            throw new Exception(null, 500);
        }


        $success = DataBase::update("`teachers`", $Fields, " `id`='" . $teacherGUID . "'");

        if (!$success) {
            throw new Exception("Error updating config for GUID: $teacherGUID", 500);
        }
    }

    public function deleteTeachersFiles($teacherGUID, $DATA)
    {

        // hook up needed classes ////////////
        $this->parent->hookClass('v2/ftp');//
        /////////////////////////////////////

        if(!count($DATA['files'])) {
            throw new Exception('teacherFiles error', 412);
        }

        $teacherFiles              = $this->getTeacherFiles($teacherGUID);

        if(!count($teacherFiles['files'])) {
            throw new Exception('Files not found', 412);
        }

        foreach($teacherFiles['files'] as $key => $file){

            $filePath    = explode('opencontent', $file);

            if(isset($filePath[1]) && in_array('/opencontent'.$filePath[1], $DATA['files'])) {

                $deleted     = $this->parent->class->ftp->deleteFile($file);

                if($deleted) {
                    unset($teacherFiles['files'][$key]);
                    $this->saveTeacherFiles($teacherGUID, $teacherFiles);
                }

            }
        }

        return $teacherFiles;

    }

}
