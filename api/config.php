<?php
// PHP Error reporting settings
ini_set('display_errors', 0);
error_reporting(0);


// API versions
if (!isset($API_VERSION)) {
    $API_VERSION = array(
        "v1" => "_api.php",
        "v2" => "_v2_api.php"
    );
}


// DataBase connection settings
@define('DB_TYPE', 'mysql');
@define('DB_HOST', 'localhost');
@define('DB_NAME', '');
@define('DB_USER', '');
@define('DB_PASS', '');

@define('DB_V2_TYPE', 'mysql');
@define('DB_V2_HOST', 'localhost');
@define('DB_V2_NAME', '');
@define('DB_V2_USER', '');
@define('DB_V2_PASS', '');
@define('DB_V2_LOGDB', '');


//Solr settings
@define("SOLR_HOST", 'localhost');
@define("SOLR_PORT", 8983);
@define("SOLR_CORE", '');

// Paths and urls
@define("SUB_ROOT", '/api');
@define("DOCUMENT_ROOT", $_SERVER['DOCUMENT_ROOT'].SUB_ROOT);
@define("OUTPUT_HANDLER_PATH", DOCUMENT_ROOT . '/output_handler');
@define("RESOURCES_PATH", DOCUMENT_ROOT . '/resources');
@define("SYS_CLASSES_PATH", DOCUMENT_ROOT . '/core');
@define("CLASSES_PATH", DOCUMENT_ROOT . '/classes');
@define("ASSETS_PATH", DOCUMENT_ROOT . '/assets');
@define("LOG_PATH", DOCUMENT_ROOT . '/logs');
@define("CACHE_PATH", DOCUMENT_ROOT . '/cache');
@define("VENDORS_PATH", DOCUMENT_ROOT . '/vendors');


// Cache settings
@define("CACHE_ENABLE", false);
@define("DB_CACHE_ENABLE", false);
@define("CACHE_TIME", 100000000);


// HTTP settings
@define("METHOD", $_SERVER['REQUEST_METHOD']);
@define("PROTOCOL", "http");
@define("HTTP_HOST", $_SERVER['HTTP_HOST']);
@define("CURRENT_HREF", PROTOCOL . '://' . HTTP_HOST . $_SERVER['REQUEST_URI']);
@define("HTTP_VERSION", "HTTP/1.1");
@define("RESULT_DEFAULT_STATUS_CODE", "200");
@define("RESULT_DEFAULT_STATUS_TEXT", "OK");
@define("RESULT_DEFAULT_CONTENT_TYPE", "application/json");
@define("RESULT_DEFAULT_OUTPUT_TYPE", "json");
@define("RESULT_DEFAULT_ENCODING", "UTF-8");


// Session and authentication settings
@define('SRV_APIKEY', 'a95bc16631ae2b6fadb455ee018da0ad'); // used for authentication in API.v1. Is not used in API.v2
@define('SESSION_TTL', '900'); //time in seconds
@define("TOKEN_TIME", 43200);


// CDN settings
// those settings are used for publish resources to upload published files to CDN via FTP
@define("KC_CDN_FTP_SERVER", "ftp.cachefly.com");
@define("KC_CDN_FTP_USERNAME", "");
@define("KC_CDN_FTP_PASSWORD", "");
@define("KC_CDN_FTP_FILEPATH", "");

@define("KC_CDN_PROTOCOL", "https");
@define("KC_CDN_HOST", "cdn0.knowledgecity.com");
@define("CACHEFLY_STATIC_APIKEY", "");


// video streaming settings
// used by resource which generating protected video url
@define("CACHEFLY_PROTECT_SECRET", "");
@define("PROTECT_LINK_DEFAULT_TTL", 60);
@define("KC_VIDEO_CDN_PROTOCOL", "https");
@define("KC_VIDEO_CDN_HOST", "cdn0.knowledgecity.com");

@define("CDN77_PROTECT_SECRET", "");

// Default HTTP response codes
if (!isset($DEFAULT_HTTP_CODES)) {
    $DEFAULT_HTTP_CODES = array(
        '200' => 'OK',
        '201' => 'Created',
        '204' => 'No Content',
        '302' => 'Found',
        '303' => 'See Other',
        '304' => 'Not Modified',
        '400' => 'Bad Request',
        '401' => 'Unauthorized',
        '402' => 'Payment Required',
        '403' => 'Forbidden',
        '404' => 'Not Found',
        '405' => 'Method Not Allowed',
        '406' => 'Not Acceptable',
        '408' => 'Request Timeout',
        '409' => 'Conflict',
        '410' => 'Gone',
        '412' => 'Precondition Failed',
        '497' => 'HTTP to HTTPS',
        '498' => 'Token expired/invalid',
        '499' => 'Token required',
        '500' => 'Internal Server Error',
        '502' => 'Bad Gateway',
        '503' => 'Service Unavailable',
        '504' => 'Gateway Timeout',
        '505' => 'HTTP Version Not Supported',
        '520' => 'Unknown Error',
    );
}


// RevAPI settings
@define('REV_API_CLIENT_KEY', '');
@define('REV_API_USER_KEY', '');


// SendGrid settings
@define('SENDGRID_API_KEY', '');
@define('EMAIL_LIVE_MODE', false); // if this is true then class v2/email will actually send messages. If this is false it will just store

// Deamon settings
// used for long running tasks, for example tasks for REV tool.
@define('PHP_EXEC', 'php');
@define('BACKGROUND_SCRIPT', __DIR__.'/background.php > /dev/null 2>/dev/null &');


// Other settings
@define("DEBUGMODE", false);

@define("MONITORINGTOKEN", null);
if(MONITORINGTOKEN && $_GET['monitoringToken'] == MONITORINGTOKEN)
    @define("MONITORING", true);
else
    @define("MONITORING", false);


// Integration settings
@define("INTEGRATION_EXAMPLE_FTP_SERVER", "kcdev.pro");
@define("INTEGRATION_EXAMPLE_FTP_USERNAME", "userabc");
@define("INTEGRATION_EXAMPLE_FTP_PASSWORD", "pass123");
@define("INTEGRATION_EXAMPLE_FTP_FILEPATH", "www/api.knowledgecity.com/background/integration/csv/exampleserver/");
@define("INTEGRATION_EXAMPLE_FTP_FILEPATH_DONE", "www/api.knowledgecity.com/background/integration/csv/exampleserver/done/");
@define("INTEGRATION_EXAMPLE_FTP_FILEPATH_FAILED", "www/api.knowledgecity.com/background/integration/csv/exampleserver/failed/");


@define("INTEGRATION_RB_PROTOCOL", "FTP");

@define("INTEGRATION_RB_FTP_SERVER", "kcdev.pro");
@define("INTEGRATION_RB_FTP_USERNAME", "userabc");
@define("INTEGRATION_RB_FTP_PASSWORD", "pass123");
@define("INTEGRATION_RB_FTP_FILEPATH", "www/api.knowledgecity.com/background/integration/csv/rbserver/");
@define("INTEGRATION_RB_FTP_FILEPATH_DONE", "www/api.knowledgecity.com/background/integration/csv/rbserver/done/");
@define("INTEGRATION_RB_FTP_FILEPATH_FAILED", "www/api.knowledgecity.com/background/integration/csv/rbserver/failed/");

@define("INTEGRATION_RB_SFTP_SERVER", "kcdev.pro");
@define("INTEGRATION_RB_SFTP_SERVER_PORT", 22);
@define("INTEGRATION_RB_SFTP_USERNAME", "userabc");
@define("INTEGRATION_RB_SFTP_KEYFILE", "pass123");
@define("INTEGRATION_RB_SFTP_KEYFILE_PASSWORD", "pass123");
@define("INTEGRATION_RB_SFTP_FILEPATH", "");
@define("INTEGRATION_RB_SFTP_FILEPATH_DONE", "done/");
@define("INTEGRATION_RB_SFTP_FILEPATH_FAILED", "failed/");