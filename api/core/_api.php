<?php

class api
{
    var $db = ''; //
    var $timeStart = 0; // time in seconds+milliseconds of begining of executing the script
    var $REQUEST_URI = array();
    var $REQUEST_URI_STRING = '';
    var $DATA = ''; // array of incoming data
    var $OUTPUT_FORMAT = ''; // json by default
    var $METHOD = ''; // request method
    var $EXTEND_FIELDS = array(); // extend output with these fields
    var $CONTENT_TYPE = ''; // content type
    var $ENCODING = ''; // encoding
    var $HTTP_HEADERS = array(); // additional headers commands
    var $HTTP_CODE = ''; // HTTP code
    var $HTTP_STATUS = ''; // HTTP status
    var $HTTP_REFERER_HOST = '';  // where the request comes from, eg www.knowledgecity.com
    var $HTTP_REFERER_SCHEME = ''; //eg 'http'
    var $HTTP_REFERER_SERVER = '';  //eg 'https://www.knowledgecity.com'
    var $REQUEST_HASH = ''; // hash request (md5 of serialized DATA)
    var $CACHEFILE_PATH = ''; // filepath to a cache file
    var $TOKEN = ''; // API token of authorized user
    var $SAVE_CACHE = false;
    var $PAGE_CACHED = false;
    var $OFFSET = 0;
    var $LIMIT = 0;
    var $PAGE = 0;
    var $ORDER_CONDITION = array(); // order condition. Two parameters: field and direction
    var $LAST_SESSION_ID = -1;
    var $no_count_usage = false; // do not write token usage time in api_session table
    var $isInternalROOTuser = false; // if TRUE then user considered to be a root
    var $class = null;

    public function __construct(
        $url = '',
        $inputData = array(),
        $subResource = true,
        $timeStart = 0,
        $as_root_user = false,
        $parent
    ) {
        global $AUTH_ALLOWED_TYPES;
        global $AUTH_BY_LOGIN;
        global $DEFAULT_HTTP_CODES;


        //info about where request comes from (not reliable)
        if (isset($_SERVER['HTTP_REFERER']) && $parts = parse_url($_SERVER['HTTP_REFERER'])) {
            $this->HTTP_REFERER_SCHEME = $parts["scheme"];
            $this->HTTP_REFERER_HOST = $parts["host"];
            $this->HTTP_REFERER_SERVER = $parts["scheme"] . "://" . $parts["host"];
        }

        // define default HTTP code as 200
        $this->HTTP_CODE = 200;

        if (!$subResource) {
            $this->db = new db(DB_TYPE, DB_HOST, DB_NAME, DB_USER, DB_PASS);
        } else {
            $this->db = $parent->db;
        }

        $this->class = new stdClass();

        $this->timeStart = $timeStart;
        $this->subResource = $subResource;
        $this->isInternalROOTuser = $as_root_user;


        // create array of incoming data
        $this->DATA = $inputData;

        // define and check token
        $this->TOKEN = $this->DATA['token'];
        unset($this->DATA['token']);


        // define if body is needed in the answer
        if (isset($this->DATA['_body']) && $this->DATA['_body'] == 'false') {
            define("OUTPUT_BODY", false);
        } else {
            define("OUTPUT_BODY", true);
        }
        unset($this->DATA['_body']);

        // format of output data
        $this->OUTPUT_FORMAT = isset($this->DATA['_output']) && $this->DATA['_output'] ? $this->DATA['_output'] : RESULT_DEFAULT_OUTPUT_TYPE;
        unset($this->DATA['_output']);

        // extend output with the given fields
        $this->EXTEND_FIELDS = isset($this->DATA['_extend']) && $this->DATA['_extend'] ? explode(',',
            $this->DATA['_extend']) : '';
        if ($this->EXTEND_FIELDS) {
            foreach ($this->EXTEND_FIELDS as $key => $value) {
                $this->EXTEND_FIELDS[$key] = trim($this->EXTEND_FIELDS[$key]);
                if (!$this->EXTEND_FIELDS[$key] || $this->EXTEND_FIELDS[$key] == 'id') {
                    unset($this->EXTEND_FIELDS[$key]);
                }
            }
            unset($this->DATA['_extend']);
        }

        // define method
        $allowed_methods = array('get', 'post', 'put', 'delete');
        $this->METHOD = isset($this->DATA['_method']) && in_array(strtolower($this->DATA['_method']),
            $allowed_methods) ? $this->DATA['_method'] : METHOD;
        $this->METHOD = strtolower($this->METHOD);
        unset($this->DATA['_method']);

        // define OFFSET
        $this->OFFSET = intval($this->DATA['_offset']);
        unset($this->DATA['_offset']);

        // define LIMIT
        $this->LIMIT = intval($this->DATA['_limit']);
        unset($this->DATA['_limit']);

        // define PAGE
        $this->PAGE = intval($this->DATA['_page']) > 1 ? intval($this->DATA['_page']) : 1;
        unset($this->DATA['_page']);

        // define order
        list($this->ORDER_CONDITION['field'], $this->ORDER_CONDITION['direction']) = explode(",",
            trim($this->DATA['_order']));
        $this->ORDER_CONDITION['field'] = trim($this->ORDER_CONDITION['field']);
        $this->ORDER_CONDITION['direction'] = strtoupper(trim($this->ORDER_CONDITION['direction']));

        $allow_directions = array('ASC', 'DESC', 'RAND');
        if (!in_array($this->ORDER_CONDITION['direction'], $allow_directions)) {
            $this->ORDER_CONDITION['direction'] = $allow_directions[0];
        }
        unset($this->DATA['_order']);

        // requested path
        list($REQUEST_URI, $QUERY) = @explode("?", trim($url, '/'));
        $REQUEST_URI = trim($REQUEST_URI, '/');
        $this->REQUEST_URI = explode('/', $REQUEST_URI);


        //rewrite (virtual/helper)URL such as /me/progress to full url
        $this->REQUEST_URI = $this->check_routing($this->REQUEST_URI);
        $this->REQUEST_URI_STRING = implode('/', $this->REQUEST_URI);//shouldn't this be imploded??? I imploded it

        // define CONTENT_TYPE
        $this->CONTENT_TYPE = RESULT_DEFAULT_CONTENT_TYPE;

        // define ENCODING
        $this->ENCODING = RESULT_DEFAULT_ENCODING;

        $this->DEFAULT_HTTP_CODES = $DEFAULT_HTTP_CODES;
        $this->AUTH_ALLOWED_TYPES = $AUTH_ALLOWED_TYPES;
        $this->AUTH_BY_LOGIN = $AUTH_BY_LOGIN;

        // run the resource
        $this->exec_resource();


    }



    ///////////////////////////////////////////////////////////////////////////////////////
    // check that token is not expired. returns true valid and not expired, or false
    ///////////////////////////////////////////////////////////////////////////////////////
    public function check_session_token($token, $ip)
    {
        if (!$token) {
            return false;
        }
        $session = current($this->db->select('api_sessions', array(
            'api_token' => "'" . $token . "'"
        )));

        if (!$session)           // didn't find session
        {
            return false;
        }

        if (!$session['date_expire'] || strtotime($session['date_expire']) > time()) {
            return true;
        }  // there's no expire date or the date is in the future

        return false;   // didn't pass any test = expired or invalid
    }

    //////////////////////////////////////////////////////////////////////////////////
    // extend the expiration if token is not yet expired. return new seconds left or 0
    //////////////////////////////////////////////////////////////////////////////////
    public function set_session_token_expire($token, $ip, $extension_seconds = 0)
    {  //this probably should be used by auth_end_session
        //note: other reasons for not granting extension, such as no seats, or subscription should
        //      be handled at the resource level.
        if (!$this->check_session_token($token, $ip)) {
            return 0;
        }

        $Fields = array(
            'date_expire' => date('Y-m-d H:i:s', time() + $extension_seconds)
        );  // 0 for now (expire, eg log out). also negative numbers would work....
        $this->db->update('api_sessions', $Fields, "`api_token`='" . $token . "'");
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // takes an array representing a url request, and checks the $NESTED_RULES to see
    // if there's a match.  So a url request like 'user/progress' might get rewritten
    // to 'countries/0{:myCountry}/citizens/{:myGovernmentID}/communityservice/progress'.
    // Any id numbers in the request, eg users/017/purchase/05  will be collected in
    // order and mapped to the matched rule by looking for tokens like {id0} {id1} {id2}
    // and replacing them with the id in the request.  Any 'tokens' in the long-form
    // url like "{:selfCountry:}" will be checked to be valid by the get_id function.
    // If no matching rule was found, then it just returns the url (as array) as is.
    // If a matching rule was found, it returns a new array representing a request
    // with the long-form url, and tokens where tokens were found, and with specific
    // ids in their new location.  In other words, any id numbers specified in the
    // original request will appear as id numbers (eg 045) in the result, but the 
    // 'tokens' will still be tokens, not id numbers.
    // Example:
    //      input:   array('progress','02015')
    //      request: progress/02015
    //      rule:    progress/_id_>users/{:me:}/progresss/{id0}
    //      result:  users/{:me:}/progress/02015
    //      return:  array('users','{:me:}','progress','02015')
    ////////////////////////////////////////////////////////////////////////////////////
    private function check_routing($routArray = array())
    {
        global $NESTED_RULES; // this array is in sys.config.php file
        //echo "\nroutArray";print_r($routArray);
        $gotIds = array();
        $normolisedRoutArray = array();
        //note, tokens like {:selfId:} should not appear in actual requests
        //so any ids found here will be actual ids like 034 or 0BUS1067
        //TODO: make it skip over tokens so long-form url's can be used (eg including tokens)
        foreach ($routArray as $k => $v) {
            $gotId = $this->get_id($v);
            if ($gotId !== false) {
                $normolisedRoutArray[$k] = '_id_';
                //$gotIds[] = $v;
                $gotIds[] = $gotId;
            } else {
                $normolisedRoutArray[$k] = $v;
            }
        }
        //the new uri, with _id_  instead of number ids (so we can find the file)
        //example: account/023 became account/_id_
        $requestedURI = implode("/", $normolisedRoutArray);
        //echo "\nrequestedURI"; print_r($requestedURI);

        //now we rewrite any short form urls to long form
        //example:  me/progress/_id_ becomes 
        //          accounts/0{:selfCompanyId:}/users/0{:selfId:}/progress/{id0}
        //and then: accounts/0{:selfCompanyId:}/users/0{:selfId:}/progress/0BUS1067
        //so that it looks as though the made the long form request, inluding the id's where needed
        foreach ($NESTED_RULES as $singleRule) {
            list($key, $value) = explode(NESTED_KEYVALUE_DELIMITER, $singleRule);
            $key = trim($key, '/');
            $value = trim($value, '/');

            if ($requestedURI == $key) {//if the request matches a rule
                foreach ($gotIds as $idKey => $idValue) {
                    $value = str_replace("{id" . $idKey . "}", $idValue, $value);
                }
                $value = explode(NESTED_VALUE_DELIMITER, $value);
                $routArray = $value;
            } else {
            }
        }
        //after the above, something like 
        //  accounts/0{:selfCompanyId:}/users/0{:selfId:}/progress/{id0}
        //will look like:
        //  accounts/0{:selfCompanyId:}/users/0{:selfId:}/progress/0BUS1067

        //echo "\nroutArray";print_r($routArray);
        return $routArray;
    }

    /////////////////////////////////////////////////////////////////////////////
    // takes an id like 0345, or a token id like {:selfCompanyId:}
    // returns an id, like 345 (assuming there's a valid session)
    /////////////////////////////////////////////////////////////////////////////
    public function get_id($idOrToken = '')
    {
        global $NESTED_RULES_TOKENS;//map of tokens to fields
        $idOrToken = trim($idOrToken);

        if ($this->is_md5($idOrToken) || $this->is_guid($idOrToken)) { //recognize md5 hash or guid as an "id" without leading zero (c'mon guys, don't make folder names 32 characters long)
            return $idOrToken;
        } elseif (is_numeric($idOrToken) && is_int($idOrToken * 1)) {
            return ''.($idOrToken * 1);
        } elseif (preg_match("/^0(.*)/i", $idOrToken)) {
            $rawId = preg_replace("/^0(.*)/i", '\\1', $idOrToken);
            //echo "\nrawId for $idOrToken:$rawId\n";
            if (array_key_exists($rawId, $NESTED_RULES_TOKENS)) {
                // get ID or some foreign key of the current user
                $rawId = $this->get_token_id($rawId);
            }
            return $rawId;
        } else {
            return false;
        }
    }

    /////////////////////////////////////////////////////////////////////////////
    // takes a token id like {:selfCompanyId:} or {:selfId:}
    // and looks up what 'table.field' that refers to ($NESTED_RULES_TOKENS)
    // then queries that table for a row matching the user's session api secret
    // and gets the id from the field specified in $NESTED_RULES_TOKENS
    //
    // returns an id, like 345 or I-XCY234K
    /////////////////////////////////////////////////////////////////////////////
    public function get_token_id($token = '{:selfId:}')
    {
        global $NESTED_RULES_TOKENS;//map of tokens to tables/fields
        if (!$token || !$NESTED_RULES_TOKENS[$token]) {
            return false;
        }
        static $userId;
        if ($userId[$token]) {
            return $userId[$token];
        }
        //echo "talking to db about $token\n";
        if ($this->TOKEN) {
            $user = current($this->db->select('api_sessions', array(
                'api_token' => "'" . $this->TOKEN . "'"
            )));
            //echo "\nuser session:";print_r($user);echo "\ndone user session"; //should be the entry in api_sessions
            list($table_name, $field_name) = explode('.', $NESTED_RULES_TOKENS[$token]);
            $userData = current($this->db->select($table_name,
                array('api_secret' => "'" . addslashes(trim($user['api_secret'])) . "'")));
            $userId[$token] = $userData[$field_name];//cache it for subsequent requests
            //print_r($userId);
            return $userId[$token];
        } else {
            return false;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // all id's have a leading zero. So text id's like I-XJXH34 will be 0I-XJXH34 when 
    // in urls like /accounts/0I-XJXH34/ so that the url elements can be identified as ids
    // The exception are md5 hashes (always 32 chars) and guids (always 36 chars) 
    //////////////////////////////////////////////////////////////////////////////////////
    public function make_id($rawId = '')
    {
        if ($this->is_md5($rawId) || $this->is_guid($rawId)) {
            return $rawId;
        } else {
            return '0' . $rawId;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // tell whether a string looks like an md5 hash 
    //////////////////////////////////////////////////////////////////////////////////////
    public function is_md5($rawId = '')
    {
        return (strlen($rawId) === 32) || (strlen($rawId) === 64) || (strlen($rawId) === 40); //could check to make sure no symbols, but ...
        //note, an alternate approach could be for people to specify in the request, eg
        // accounts/md5_3F2504E04F8941D39A0C0305E82C3301
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // tell whether a string looks like a guid (or uuid) 
    //////////////////////////////////////////////////////////////////////////////////////
    public function is_guid($rawId = '')
    {
        return (strlen($rawId) === 36) && (substr_count($rawId,
                "-") === 4); //could check to make sure no symbols, but ...
        //note, an alternate approach could be for people to specify in the request, eg
        // accounts/guid_3F2504E0-4F89-41D3-9A0C-0305E82C3301
        //then a guid_ folder could be in the resources file system and have it's own set of
        //get post handlers.  random thought.
    }

    //////////////////////////////////////////////////////////////////////////////////////
    // takes a userType (AUTH_ALLOWED_TYPES) and an api_secret like 23kj23lkj4lj234j2l3
    // then it checks if there is currently a session with that usertype/secret.  If so
    // it returns the token for the current session.  Otherwised, it creates a new token
    // and writes a row in the api_sessions table, and returns the new token.
    // NOTE: previously token_field_name was the third parameter
    //////////////////////////////////////////////////////////////////////////////////////
    public function auth_init_session(
        $user_type = '',
        $api_secret = '',
        $expire_seconds = null,
        $token_field_name = 'api_secret'
    ) {

        //select from the table associated with this usertype that contains the secret, ie get the user's api_secret
        $queryResult = $this->db->select($this->AUTH_ALLOWED_TYPES[$user_type],
            array($token_field_name => "'" . addslashes(trim($api_secret)) . "'"));
        if ($queryResult[0]['id']) {
            //cancel/expire any current session (will "log other people out" if using same account)
            $wherecond = "`api_secret`='" . $api_secret . "' && (date_expire IS NULL or date_expire > '" . date('Y-m-d H:i:s') . "')";
            $this->db->update('api_sessions', array('date_expire' => date('Y-m-d H:i:s')), $wherecond);

            // if field 'userGroup' exists and not equals 0 than retriev user_type from table `api_user_groups`
            if (isset($queryResult[0]['userGroup']) && intval($queryResult[0]['userGroup'])) {
                $userGroupInfo = $this->get_userGroupInfo_by_id($queryResult[0]['userGroup']);
                $user_type = $userGroupInfo['code'];
            }

            //create api token and insert it, optionally with expire date           
            $api_token = $this->generate_api_token();
            $fields = array(
                'api_token' => $api_token,
                'api_secret' => $api_secret,
                'user_type' => $user_type,
                'date_start' => date('Y-m-d H:i:s', time()),
                'user_ip' => $this->user_ip(),
            );
            if ($expire_seconds) {
                $fields['date_expire'] = date('Y-m-d H:i:s', time() + $expire_seconds);
            }

            $this->LAST_SESSION_ID = $this->db->insert('api_sessions', $fields);

            $auth_result = array("token" => $api_token);
            if ($expire_seconds) {
                $auth_result['seconds'] = $expire_seconds;
            }

            // Add userType to output
            $auth_result['user_type'] = $user_type;

            return $auth_result;
        } else {
            return false;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////
    // generates a random alphanumeric token based on md5 of time and a shuffle
    // this token written, along with an api_secret into the api_sessions table,
    // and the token is sent to the user for subsequent requests, to identify the 
    // user and represent the "session".
    //////////////////////////////////////////////////////////////////////////////////
    function generate_api_token($length = 32)
    {
        $string2randomize = md5(time());

        $length = intval($length);
        if ($length < 1) {
            $length = 32;
        }

        return substr(str_shuffle($string2randomize), rand(0, (strlen($string2randomize) - $length)), $length);
    }

    //////////////////////////////////////////////////////////////////////////////////
    // Takes a user_type (AUTH_ALLOWED_TYPES) and a username and password.
    // Checks which table, based on user_type would contain these users, and checks
    // the corresponding field for username, and a field called password, and gets
    // the api_secret for that user, then makes a request to auth_init_session using
    // the user_type and api_secret.  
    // Returns the token returned by auth_init_session, or false
    //////////////////////////////////////////////////////////////////////////////////
    private function auth_init_session_by_login(
        $user_type = '',
        $login = '',
        $password = '',
        $expire_seconds = SESSION_DURATION
    ) {
        $this->db->selectFields = array('api_secret');
        $conditions = array(
            $this->AUTH_BY_LOGIN[$user_type] => "'" . addslashes(trim($login)) . "'",
            'password' => "'" . addslashes($this->hashPassword(trim($password))) . "'"
        );
        $queryResult = $this->db->select($this->AUTH_ALLOWED_TYPES[$user_type], $conditions);
        $api_secret = $queryResult[0]['api_secret'];
        if (!$api_secret) {
            return false;
        }

        return $this->auth_init_session($user_type, $api_secret, $expire_seconds);
    }

    //////////////////////////////////////////////////////////////////////////////////
    // Takes a record from the table `api_user_groups` by ID
    //////////////////////////////////////////////////////////////////////////////////
    private function get_userGroupInfo_by_id($groupId = 0)
    {
        $groupId = intval($groupId);
        if (!$groupId) {
            return null;
        }

        $this->db->selectFields = array('*');
        $conditions = array(
            'id' => $groupId
        );
        $queryResult = $this->db->select('api_user_groups', $conditions);
        if (isset($queryResult[0]) && is_array($queryResult[0])) {
            return $queryResult[0];
        } else {
            return array();
        }
    }

    //////////////////////////////////////////////////////////////////////////////////
    // Takes an ID Validator id (library_id) and an id number (eg library card id).
    // Checks which table, based on the ID Validator id, and checks the 
    // the corresponding field for user id, and gets the api_secret for that user, 
    // then makes a request to auth_init_session using the user_type and api_secret.  
    // Returns the token returned by auth_init_session, or false
    // Note, this deletes a "temporary session", that was created when we
    // authenticate with the ID Validator (eg sip server) and possibly other factors,
    // because the actual login occures on the second page.  If the ID Validator
    // (eg sip server) did validate the user's id, then the code can remove that 
    // temporary authorisation. So their seat was secured before we log in to kc.
    //////////////////////////////////////////////////////////////////////////////////
    private function auth_init_session_via_library($library_id = 0, $library_card_id = '')
    {
        $user_type = 'accountUser';

        $this->db->selectFields = array('api_secret');
        $conditions = array(
            'library_card_id' => "'" . addslashes(trim($library_card_id)) . "'",
            'library_id' => "'" . intval($library_id) . "'"
        );
        $queryResult = $this->db->select($this->AUTH_ALLOWED_TYPES[$user_type], $conditions);
        $api_secret = $queryResult[0]['api_secret'];
        if (!$api_secret) {
            return false;
        }

        $expire = SESSION_DURATION; //seconds token is active before needed refresh

        $auth_result = $this->auth_init_session($user_type, $api_secret, $expire);
        if (!$auth_result) {
            return false;
        }

        //while sip had authorized us before,
        //now we are logged in to knowledgecity, so we can clean up this temporary authorization
        $this->db->delete('library_sip_authorised',
            '`lid`=' . intval($library_id) . ' && `mid`=' . strval(($library_card_id)));

        return $auth_result;
    }


    /////////////////////////////////////////////////////////////////////////////////
    // "logs out", by deleting the session entry for the supplied token
    /////////////////////////////////////////////////////////////////////////////////
    private function auth_end_session($token = null)
    {
        if (is_null($token)) {
            $token = $this->TOKEN;
        }

        if ($token) {
            $session = $this->db->select('api_sessions',
                array(
                    'api_token' => "'" . $token . "'"
                ));
            $sessionID = $session[0]['id'];
            $date_expire = $session[0]['date_expire'];
        }


        if ($sessionID)// hey you dooooooooon't clear sessions
        {
            $this->db->update("`api_sessions`", array("date_expire" => date('Y-m-d H:i:s')), " `id`=" . $sessionID);
            //$this->db->update("`library_session`", ," `SID`=".$sessionID);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////
    // returns the user_type field in api_sessions for the current token, or ''
    /////////////////////////////////////////////////////////////////////////////////
    public function api_user_type()
    {
        static $return_cache = array();
        if ($this->isInternalROOTuser) {
            return "root";
        }
        if (!$this->TOKEN) {
            return '';
        }
        if (isset($return_cache[$this->TOKEN])) {
            return $return_cache[$this->TOKEN];
        }

        $session = $this->db->select('api_sessions',
            array(
                'api_token' => "'" . $this->TOKEN . "'",
                'user_ip' => "'" . $this->user_ip() . "'"
            ));

        $type = $session[0]['user_type'];

        $return_cache[$this->TOKEN] = $type;
        return $return_cache[$this->TOKEN];
    }

    /////////////////////////////////////////////////////////////////////////////////
    // for root users, return true
    // if no authentication constraints are passed, eg checkauth(), return true
    // Normally, an array of key=>value pairs is passed such as: 
    //   array(
    //     "siteAdmin"=>   "accounts/*/users/*",                           
    //     "accountAdmin"=>"accounts/0{:selfCompanyId:}/users/*",          
    //     "accountUser"=> "accounts/0{:selfCompanyId:}/users/0{:selfId:}" 
    //   );
    // this means that siteAdmin can access any account or user, accountAdmin can
    // access any user with his own company id, and accountUser can access only
    // the user with his own company id and user id.
    //
    // The way it actually process the array is just to select the pattern that
    // matches the current user type, and then if there are any tokens in the
    // pattern, it verifies that the values of those tokens exactly mathches
    // the value that was actually passed in the url at that position
    //
    // NOTE: check_auth is called in the exec_resource function so it can check
    //       the access and returned cached results, without including the resource
    //       code.  Currently each resource calls check auth additionally, but that
    //       may be changed later to only use by the access_levels field in the 
    //       api_resource_settings table
    //
    // returns true or false, and if false, it also calls complete() with the reason
    /////////////////////////////////////////////////////////////////////////////////

    function check_auth($access_levels = null, $strict = true)
    {
        //echo "access_levels required";print_r($access_levels);
        //echo "current user type: ".$this->api_user_type();

        // if access levels conditions is NULL then return everyone has access
        if (is_null($access_levels)) {
            return true;
        }

        // if isInternalROOTuser is true that means that this request is internal and made under root permitions (internal ROOT user)
        // this user always has access therefore just return true
        if ($this->isInternalROOTuser) {
            return true;
        }

        // if this is not inner ROOT user and token is defined then check if this token is valid and session is not expired
        if ($this->TOKEN && !$this->check_session_token($this->TOKEN, $this->user_ip())) {
            $this->complete(498, 'Token expired/invalid', '');
            return false;
        }

        // if this is not inner ROOT user and token is not set OR token is valid and session is not expired then check if the user is root and if so return true
        $userType = $this->api_user_type();//returns "" (empty string) if no token has been specified. this is a valid access level type
        if ($userType == 'root') {
            return true;
        }

        // if the user is not inner ROOT and not ROOT and session is valid and access levels conditions are passed but the access levels conditions is not an array then return an error
        if (!is_array($access_levels)) {
            $this->complete(403, 'Forbidden', 'Internal permissions error for: ' . $this->REQUEST_URI_STRING);
            return false;
        }

        //if the current usertype is not mentioned in the access levels, no access
        if (array_key_exists($this->api_user_type(), $access_levels)) {
            $accessPattern = explode('/', trim(trim($access_levels[$this->api_user_type()], '/')));
        } // eg turn "siteAdmin"   =>'accounts/*/users/' into $accessPattern = array('accounts','*','users')
        else {
            $this->complete(403, 'Forbidden',
                'You don\'t have permission to access this resource [' . $this->REQUEST_URI_STRING . '] on this server.');
            return false;
        }

        global $NESTED_RULES_TOKENS;//map of tokens to tables/fields
        //check that any tokens found match the corresponding id of the request
        foreach ($accessPattern as $i => $seg) {
            if ($seg[0] === '0')//this should only get triggered for a 0{:token:} .
            {
                //if we're dealing with a token in the pattern, check that the value
                //of that token matches the id supplied by the request
                $expectedId = $this->get_id($seg);
                $suppliedId = $this->get_id($this->REQUEST_URI[$i]);
                if ($this->REQUEST_URI[$i]['0'] !== '0') {
                    $this->complete(403, 'Forbidden',
                        'You don\'t have permission to access item requested [' . $this->REQUEST_URI_STRING . '] on this server. Expected id was not supplied');
                    return false;
                }
                //print_r($this->REQUEST_URI[$i]);  echo "supplied:($suppliedId), expected:($expectedId)";
                if ($suppliedId != $expectedId) {
                    $this->complete(403, 'Forbidden',
                        'You don\'t have permission to access item [' . $suppliedId . '] requested [' . $this->REQUEST_URI_STRING . '] on this server.');
                    return false;
                }
            }

            //if they access pattern requires an md5 or guid in part (eg we don't want to let them specify an internal id like 0578)
            //then make sure there's actually a guid or md5 in that location.  The resource will know/determine how to handle the actual ids
            if ($seg === "{:md5|guid:}") {
                $suppliedId = $this->get_id($this->REQUEST_URI[$i]);
                if (!$this->is_md5($suppliedId) && !$this->is_guid($suppliedId)) {
                    $this->complete(403, 'Forbidden',
                        'You don\'t have permission to access item requested [' . $this->REQUEST_URI_STRING . '] on this server. Expected id was not supplied');
                    return false;
                }
            }
        }

        return true;
    }


    /////////////////////////////////////////////////////////////////////////////
    // after having checked the routing rules and done any rewrites,
    // this function actually calls the appropriate php file endpoint
    // Specifically, it checks if the user should have access, then checks
    // cache.  If there's no access or not found, it outputs errors as response.
    // If they should have access and it's cached, it returns cached output.
    // If not cached, it includes the resource php file and outputs whatever was
    // output by the resource file, after saving it to cache.
    /////////////////////////////////////////////////////////////////////////////
    private function exec_resource()
    {
        //echo "\nexec_resource\n".$this->METHOD.' '.$this->REQUEST_URI_STRING;
        if (!$this->REQUEST_URI_STRING) {
            return $this->complete(400, 'Bad Request', 'Empty resource is not allowed');
        }

        $resource_file = array();
        foreach ($this->REQUEST_URI as $value) {
            //echo "a($value)";
            //check if id can be resolved. if so, replace with generic filesystem folder /_id_/
            if ($this->get_id($value) !== false) {//this assumes id's are never 0.  TODO: allow 0 and user !==false
                $value = '_id_';
            } else {
                global $NESTED_RULES_TOKENS;//map of tokens to tables/fields
                //echo "b($value)";
                if ($NESTED_RULES_TOKENS[trim($value,
                    '0')]
                )                       //all tokens of type 0{:selfCompanyId:} require you to be logged in and have token to look up that id value
                {                                                                //and have token to look up that id value

                    if ($this->TOKEN) {
                        return $this->complete(498, 'Token expired/invalid', '');
                    }   //note: if token expired, "api_sessions.date_last_use" will
                    else                                                            //be greater than date_expire, so we can see if there was
                    {
                        return $this->complete(499, 'Token required', '');
                    }          //a token use after the session expired.
                }
            }
            // $value = preg_replace("/^id-(.*)/i", '_id_', $value);
            $resource_file[] = $value;
        }
        $resource_file_path = trim(implode('/', $resource_file));
        $resource_path = RESOURCES_PATH . '/' . $resource_file_path;

        //echo "\n\n\nresource_path\n\n";echo ($resource_path);
        if ($resource_file_path && is_dir($resource_path)) {
            $resource_file = $resource_path . '/' . $this->METHOD . '.php';
            //echo "\nresource_file\n\n";echo ($resource_file);
            if (!is_file($resource_file)) {
                // if requested method, eg get, put is not allowed (doesn't exist) for this resource
                return $this->complete(405, 'Method Not Allowed',
                    'Requested method [' . $this->METHOD . '] for resource [' . $this->REQUEST_URI_STRING . '] is not allowed');
            } else {
                // retrieving settings of the resource

                $conditions = array(
                    'resource' => "'" . $resource_file_path . "'",
                    'method' => "'" . $this->METHOD . "'"
                );
                $resourceSettings = current($this->db->select('api_resource_settings', $conditions));
                $this->resourceSettingsArray = json_decode($resourceSettings['settingsJSON'], true);
                //echo "\n517 resourceSettingsArray for '$resource_file_path':".print_r($this->resourceSettingsArray,true);
                // 
                //if uncomment the follwoing code (and comment the code above) 
                //we can save JSON object of a resource settings in a file.
                //I decided to stick to the DataBase approach because it will allow us to develop some
                //web-based tool to manage the settings

                // $resource_config_file = $resource_path.'/'.$this->METHOD.'.conf.json';
                // if(is_file($resource_config_file))
                //     $this->resourceSettingsArray = json_decode(file_get_contents($resource_config_file), true);
                // if(!is_array($this->resourceSettingsArray)) $this->resourceSettingsArray = array();

                $this->cache_ttl = intval($this->resourceSettingsArray['cache']['ttl']);
                $this->cache_personal = ($this->resourceSettingsArray['cache']['personal'] ? true : false);
                $this->no_count_usage = ($this->resourceSettingsArray['no_count_usage'] ? true : false);

                //if we got resource settings from db, but no access levl is specified, default to null(full access). this is ok because the actual resource will call check_auth and specify auth. but this is not ideal, since the auth can be in both places
                if (is_array($this->resourceSettingsArray) && !is_array($this->resourceSettingsArray['access_levels'])) {
                    $this->resourceSettingsArray['access_levels'] = null;
                }

                $text = ''; // text to output
                //check the access requirements for the resource, and load cache or include/execute the resource
                if ($this->check_auth($this->resourceSettingsArray['access_levels'])) {
                    if ($this->LoadCache()) {
                        return;
                    }

                    ob_start(); // this is to avoid sending unexpected header information.
                    include($resource_file);
                    $this->complete($this->HTTP_CODE, $this->HTTP_STATUS);
                    $text = ob_get_contents();
                    ob_end_clean();
                }
                return trim($text);
            }

        } else { // if a file which got to handle the request does not exist return HTTP staatus 404
            return $this->complete(404, 'Not Found',
                'Requested resource [' . $this->REQUEST_URI_STRING . '] does not exist');
        }
    }

    //////////////////////////////////////////////////////////////////////////////
    // takes a URL and returns a php object represent the json that the resource
    // would normally return.  Used to call a resource internally without 
    // resorting to an http request.
    //////////////////////////////////////////////////////////////////////////////
    function self_resource($resourceURL = '', $method = 'GET', $as_root = false, $DATA = array())
    {
        $resourceURL = trim(trim($resourceURL), '/');
        if (!$resourceURL) {
            return array(
                "code" => 400,
                "status" => $this->DEFAULT_HTTP_CODES['400'],
                "response" => "Empty resource is not allowed"
            );
        }
        $allowed_methods = array('get', 'post', 'put', 'delete');
        if (!in_array(strtolower($method), $allowed_methods)) {
            $method = 'GET';
        }

        $DATA['_method'] = $method;
        $DATA['_output'] = 'json';

        if ($this->TOKEN) {
            $DATA['token'] = $this->TOKEN;
        }

        $subApi = new api($resourceURL, $DATA, true, 0, $as_root, $this);
        $retrun = json_decode($subApi->RESPONSE, true);

        // $subApi->db->pdo = null;
        // $subApi->db->sth = null;
        unset($subApi);

        return $retrun;
    }

    private function get_output($forseContent = false)
    {

        if (defined("OUTPUT_BODY") && !OUTPUT_BODY && !$forseContent) {
            return '';
        }

        if (!$this->HTTP_CODE) {
            $this->HTTP_CODE = RESULT_DEFAULT_STATUS_CODE;
        }
        if (!$this->HTTP_STATUS) {
            $this->HTTP_STATUS = $this->DEFAULT_HTTP_CODES[$this->HTTP_CODE];
        }

        $this->RESPONSE = $this->apply_pagination($this->RESPONSE, $this->LIMIT, $this->PAGE, true, false);

        $output_handler = OUTPUT_HANDLER_PATH . '/' . $this->OUTPUT_FORMAT . '.php';
        if (!is_file($output_handler)) {
            $output_handler = OUTPUT_HANDLER_PATH . '/' . RESULT_DEFAULT_OUTPUT_TYPE . '.php';
        }
        ob_start();
        include($output_handler);
        $this->OUTPUT = trim(ob_get_contents());
        ob_clean();

        return $this->OUTPUT;
    }

    private function LoadCache()
    {

        if (!isset($this->cache_ttl)) {
            $this->cache_ttl = CACHE_TIME;
        }
        if (!CACHE_ENABLE OR !$this->cache_ttl OR $this->ORDER_CONDITION['direction'] == 'RAND') {
            return false;
        }

        $cachefile_path = $this->get_cachefile_path();
        $this->cache_filetime = filemtime($cachefile_path);
        if (file_exists($cachefile_path) AND ((time() - $this->cache_filetime) <= $this->cache_ttl OR ($this->cache_ttl == -1))) // -1 => lifetime
        {
            try {
                $cache_contetn = file($cachefile_path);
                $this->HTTP_CODE = trim($cache_contetn[0]);
                $this->HTTP_STATUS = trim($cache_contetn[1]);
                $this->RESPONSE = json_decode(trim($cache_contetn[2]), true);
                $this->RESPONSE_PAGINATION = json_decode(trim($cache_contetn[3]), true);
                $this->PAGE_CACHED = true;
            } catch (Exception $e) {
                return false;
            }
        } else {
            $this->cache_filetime = time();
            $this->SAVE_CACHE = true;
            return false;
        }

        return true;
    }

    private function SaveCache()
    {
        if (!$this->SAVE_CACHE) {
            return;
        }

        try {
            $cache_file = fopen($this->get_cachefile_path(), "w+");
            fwrite($cache_file,
                $this->HTTP_CODE . "\n" . $this->HTTP_STATUS . "\n" . json_encode($this->RESPONSE) . "\n" . json_encode($this->RESPONSE_PAGINATION));
            fclose($cache_file);
        } catch (Exception $e) {

        }
    }

    public function complete($httpCode = '', $httpStatus = '', $userMessage = '')
    {
        if ($this->exec_complete) {
            return;
        }
        $this->exec_complete = true;

        if ($this->subResource) {
            ///if($httpCode != 200){
            $this->HTTP_CODE = $httpCode;
            $this->HTTP_STATUS = $httpStatus;
            $this->RESPONSE = $userMessage ? $userMessage : $this->get_output();
            //}
            return;// $this->get_output();
        }

        $httpCode = intval($httpCode);

        if (isset($this->DEFAULT_HTTP_CODES[$httpCode])) {
            $httpStatus = $this->DEFAULT_HTTP_CODES[$httpCode];
        }

        $http_code = $this->HTTP_CODE = $httpCode ? $httpCode : RESULT_DEFAULT_STATUS_CODE;
        $http_status = $this->HTTP_STATUS = $httpStatus ? $httpStatus : RESULT_DEFAULT_STATUS_TEXT;
        if ($userMessage) {
            $this->RESPONSE = $userMessage;
        }

        header("ETag: " . $this->get_cachefile_name());
        if ($this->PAGE_CACHED) {
            // $http_code = 304;
            // $http_status = 'Not Modified';
        }

        $sapi_type = php_sapi_name();
        if (substr($sapi_type, 0, 3) == 'cgi') {
            header("Status: " . $http_code . " " . $http_status);
        } else {
            header(HTTP_VERSION . " " . $http_code . " " . $http_status);
        }

        header('Cache-Control: ' . ($this->cache_personal ? 'private' : 'public') . ', max-age=' . $this->cache_ttl);

        $lastModifiedTime = $this->cache_filetime ? $this->cache_filetime : time();
        header("Last-Modified: " . gmdate('D, d M Y H:i:s \G\M\T', $lastModifiedTime));

        ///// EXECUTE HEADERS
        header("Content-Type:" . $this->CONTENT_TYPE . "; charset=" . $this->ENCODING);
        header("Access-Control-Allow-Origin: *");

        if (sizeof($this->HTTP_HEADERS) && is_array($this->HTTP_HEADERS)) {
            foreach ($this->HTTP_HEADERS as $command) {
                eval("header('" . $command . "');");
            }
        }

        // total time of execution 
        $execTime = time() + microtime() - $this->timeStart;
        header("TTE: " . $execTime);

        $output = $this->get_output();

        $this->SaveCache();

        return $output;
    }

    private function get_cachefile_path()
    {
        static $hash;
        if ($hash) {
            return $hash;
        }

        $filename = $this->get_cachefile_name();
        $hash = $this->CACHEFILE_PATH = CACHE_PATH . "/" . $filename . ".txt";

        return $hash;
    }

    private function get_cachefile_name()
    {
        static $filename;
        if ($filename) {
            return $filename;
        }

        //create MD5 hash from incoming DATA , URI and token
        $hash_array['data'] = $this->DATA;
        $hash_array['uri'] = $this->REQUEST_URI_STRING;
        $hash_array['token'] = $this->cache_personal ? $this->TOKEN : "";
        $hash_array['extend_fields'] = $this->EXTEND_FIELDS;
        $hash_array['offset'] = $this->OFFSET;
        $hash_array['limit'] = $this->LIMIT;
        $hash_array['page'] = $this->PAGE;
        $hash_array['order'] = $this->ORDER_CONDITION;

        $filename = $this->REQUEST_HASH = $this->METHOD . '.' . md5(serialize($hash_array));
        return $filename;
    }

    public function add_class($classname = '')
    {
        // prevent double objects
        if (is_object($this->class->$classname)) {
            return true;
        }

        //
        $class_file = CLASSES_PATH . '/' . $classname . '.php';
        if (!is_file($class_file)) {
            return false;
        }

        include_once $class_file;

        $this->class->$classname = new $classname();
        $this->class->$classname->pdo = $this->db->pdo;
        $this->class->$classname->parent = $this;

    }

    public function hookClass($classPath = '', $classSpecialName = null)
    {
        $className = strpos($classPath, '/') ? end(explode('/', $classPath)) : $classPath;
        $classSpecialName = is_null($classSpecialName) ? $className : $classSpecialName;

        // prevent double objects
        if (!is_object($this->class->$classSpecialName)) {

            $class_file = CLASSES_PATH . '/' . $classPath . '.php';
            if (is_file($class_file)) {
                include_once $class_file;
                $this->class->$classSpecialName = new $className();
                $this->class->$classSpecialName->parent = $this;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public function outputFields($outputFields = array(), $availableExtendFields = array(), $limitFields = array())
    {
        if ($this->api_user_type()) {
            $key = $this->api_user_type();
        } else {
            $key = '*';
        }

        $aef = isset($availableExtendFields[$key]) ? $availableExtendFields[$key] : $availableExtendFields['*'];

        if (!isset($outputFields[$key])) {
            $key = '*';
        }
        $outputFields = $outputFields[$key];

        if (is_array($this->EXTEND_FIELDS) && $aef) {
            foreach ($this->EXTEND_FIELDS as $field) {
                if (in_array($field, $aef)) {
                    $outputFields[] = $field;
                } elseif (array_key_exists($field, $aef)) {
                    if (!$aef[$field]) {
                        continue;
                    }
                    $outputFields[] = $aef[$field];
                }
            }
        }

        if (sizeof($limitFields)) {
            foreach ($limitFields as $k => $field) {
                $limitFields[$k] = $field; // make sure the field has `
            }
            $outputFields = array_intersect($outputFields, $limitFields);
        }

        foreach ($outputFields as $k => $f) {
            $outputFields[$k] = $this->db->dbFieldQuote($f);
        }

        return $outputFields;
    }

    public function orderCondition($OrderField = 'id', $OrderDir = 'ASC', $altOrderFields = array())
    {
        if ($this->ORDER_CONDITION['direction'] == 'RAND') {
            return 'RAND()';
        }
        if (
            in_array($this->ORDER_CONDITION['field'], $altOrderFields, true) ||
            ($this->ORDER_CONDITION['field'] == $OrderField && $OrderField)
        ) {
            $OrderField = $this->ORDER_CONDITION['field'];
            $OrderDir = $this->ORDER_CONDITION['direction'];
        } elseif (
            $this->ORDER_CONDITION['field'] &&
            array_key_exists($this->ORDER_CONDITION['field'], $altOrderFields, true)
        ) {
            $OrderField = $altOrderFields[$this->ORDER_CONDITION['field']];
            $OrderDir = $this->ORDER_CONDITION['direction'];
        }
        return $OrderDir == 'RAND' ? 'RAND()' : $this->db->dbFieldQuote($OrderField) . ' ' . $OrderDir;
    }

    public function prepareQueryCondition($add2conditionFields = array())
    {
        if (!is_array($add2conditionFields) || !sizeof($add2conditionFields)) {
            return array();
        }

        $condition = array();
        foreach ($add2conditionFields as $dbField => $postField) {

            if (substr($postField, 0, 1) == '~') {
                $postField = substr($postField, 1);
                $flex = true;
                $range = false;
            } elseif (substr($postField, 0, 1) == '-') {
                $postField = substr($postField, 1);
                $flex = false;
                $range = true;
            } else {
                $flex = false;
                $range = false;
            }

            if (is_numeric($dbField)) {
                $dbField = $postField;
            }

            if (isset($this->DATA[$postField])) {
                if (substr($this->DATA[$postField], 0, 1) == '~' && $flex) {
                    $condition[$dbField] = "LIKE,'%" . addslashes(substr($this->DATA[$postField], 1)) . "%'";
                } elseif ($range) {
                    list($range_from, $range_to) = explode("~", $this->DATA[$postField]);
                    $range_1 = $this->queryRangeCondition($range_from, $range_to);
                    if ($range_1) {
                        $condition[$dbField] = $range_1;
                    }
                } else {
                    $condition[$dbField] = "'" . addslashes($this->DATA[$postField]) . "'";
                }
            }
        }

        return $condition;
    }

    public function queryRangeCondition($start = '', $end = '', $inclusive = true)
    {
        if ($inclusive) {
            $ls = "<=";
            $ms = ">=";
        } else {
            $ls = "<";
            $ms = ">";
        }
        $return = '';

        $start = is_numeric($start) ? $start : ($start ? "'" . $start . "'" : '');
        $end = is_numeric($end) ? $end : ($end ? "'" . $end . "'" : '');
        if (
            (is_numeric($start) || !empty($start)) &&
            (is_numeric($end) || !empty($end))
        ) {
            $return = "BETWEEN," . $start . " AND " . $end;
        } elseif ($start != '') {
            $return = $ms . "," . $start;
        } elseif ($end != '') {
            $return = $ls . "," . $end;
        }

        return $return;
    }

    public function preparePostFields($acceptPostFields = array())
    {

        foreach ($acceptPostFields as $field => $value) {
            list($fieldKey, $fieldLabel) = explode(',', $field, 2);

            if (substr($fieldKey, 0, 1) == '*') {
                $fieldKey = substr($fieldKey, 1);


                if (!$value) {
                    if (!$fieldLabel) {
                        $fieldLabel = $fieldKey;
                    }
                    $this->HTTP_CODE = 412;
                    $this->RESPONSE['message'] = "Field '" . $fieldLabel . "' is required.";
                    return false;
                }
            } elseif (substr($fieldKey, 0, 1) == '!') {
                $fieldKey = substr($fieldKey, 1);

                if (is_null($value)) {
                    if (!$fieldLabel) {
                        $fieldLabel = $fieldKey;
                    }
                    $this->HTTP_CODE = 412;
                    $this->RESPONSE['message'] = "Field '" . $fieldLabel . "' has to be specified.";
                    return false;
                }
            }

            unset($acceptPostFields[$field]);
            $acceptPostFields[$fieldKey] = $value;

        }

        return $acceptPostFields;
    }

    public function hashPassword($rawPassword = '')
    {
        $hashPassword = $rawPassword;
        // $hashPassword = md5($rawPassword.SRV_APIKEY);

        return $hashPassword;
    }

    function get_client_ip_env()
    {

        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else {
            if (getenv('HTTP_X_FORWARDED_FOR')) {
                $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
            } else {
                if (getenv('HTTP_X_FORWARDED')) {
                    $ipaddress = getenv('HTTP_X_FORWARDED');
                } else {
                    if (getenv('HTTP_FORWARDED_FOR')) {
                        $ipaddress = getenv('HTTP_FORWARDED_FOR');
                    } else {
                        if (getenv('HTTP_FORWARDED')) {
                            $ipaddress = getenv('HTTP_FORWARDED');
                        } else {
                            if (getenv('REMOTE_ADDR')) {
                                $ipaddress = getenv('REMOTE_ADDR');
                            } else {
                                $ipaddress = 'UNKNOWN';
                            }
                        }
                    }
                }
            }
        }

        return $ipaddress;
    }

    function user_ip()
    {
        return $this->get_client_ip_env();
    }


    function apply_pagination($array = array(), $limit, $page, $save_paginator_meta = true, $strict = true)
    {
        static $applied;
        if ($applied && !$strict) {
            return $array;
        }
        $applied = true;

        if (is_null($limit)) {
            $limit = $this->LIMIT;
        }
        if (is_null($page)) {
            $page = $this->PAGE;
        }

        $limit = intval($limit);
        $page = intval($page);

        if (!(is_array($array) && sizeof($array) > 1 && $limit > 0)) {
            return $array;
        }

        $totalRecords = sizeof($array);
        $totalPages = ceil($totalRecords / $limit);
        if ($page > $totalPages) {
            $page = $totalPages;
        }

        $offset = ($page - 1) * $limit;

        if ($save_paginator_meta) {
            $this->RESPONSE_PAGINATION = array(
                'totalRecords' => $totalRecords,
                'perPage' => $limit,
                'totalPages' => $totalPages,
                'currentPage' => $page,
            );
        }

        $array = array_slice($array, $offset, $limit);

        return $array;
    }

    public function __destruct()
    {
        if (!$this->no_count_usage && $this->TOKEN) {
            $Fields = array('date_last_use' => date('Y-m-d H:i:s'));
            $this->db->update('api_sessions', $Fields,
                "`api_token`='" . $this->TOKEN . "' && `user_ip` = '" . $this->user_ip() . "'");
        }
    }


}