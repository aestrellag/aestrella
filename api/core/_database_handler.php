<?php

class db
{
    var $pdo = '';
    var $sth = '';
    var $selectFields = array();
    var $connectEstablished = false;
    var $nativeWhereCondition = '';
    var $pdoFetchMode = PDO::FETCH_ASSOC;
    var $cacheTTL = 0;
    var $cacheEnabled = false;
    var $cacheTableSums = array();
    var $cacheQueryTables = array();
    var $totals = array('sqlQueries' => 0, 'readFromCache' => 0);
    var $allUpdatedTables = array();
    var $currentDataBaseName = '';
    var $isNoLogQueries             = false;

    private $start                  = null;
    private $limit                  = null;
    private $is_total_count         = false;
    /**
     * Filters for SELECT
     * @var array
     */
    private $filters                = [];
    /**
     * Specification of filters for SELECT
     * @var array|null
     */
    private $filters_spec           = null;

    function __construct($type = '', $host = '', $db = '', $user = '', $pass = '')
    {
        try {
            $this->pdo = new PDO($type . ':host=' . $host . ';dbname=' . $db . ';charset=utf8', $user, $pass);
            $this->connectEstablished = true;

            if(defined("CACHE_TIME")) $this->cacheTTL = CACHE_TIME;
            if(defined("DB_CACHE_ENABLE")) $this->cacheEnabled = DB_CACHE_ENABLE;

            $this->currentDataBaseName = $db;

        } catch (PDOException $e) {
            die('DB Connection failed: ' . $e->getMessage());
        }
    }

    /**
     * select
     * @param string $sql SQL string
     * @param array $array Params
     * @param constant $fetchMode PDO Fetch mode
     * @return mixed
     */

    public function select(
        $table,
        $condition = array(),
        $orderCondition = '',
        $forseAddId = true,
        $groupByField = '',
        $fetchMode = ''
    ) {
        if($fetchMode)
            $this->pdoFetchMode = $fetchMode;

        // Conditions after WHERE ...
        $sql_condition = array();
        $accCompSigns = array("=", "!=", ">", "<", ">=", "<=", "IN", "NOT IN", "BETWEEN", "LIKE", "NOT LIKE", "IS");
        $insert_condition = true;
        foreach ($condition as $key => $val) {
            if (is_numeric($key)) {
                $sql_condition[] = $val;
                $insert_condition = true;
            } else {
                if (!$insert_condition) {
                    $sql_condition[] = '&&';
                }
                list($compareSign, $gotVal) = explode(',', $val, 2);
                $compareSign = trim($compareSign);
                if (!in_array($compareSign, $accCompSigns)) {
                    $gotVal = $val;
                    $compareSign = '=';
                }
                $sql_condition[] = $this->dbFieldQuote($key) . " " . $compareSign . " " . $gotVal . "";
                $insert_condition = false;
            }
        }
        // array_pop($sql_condition);
        $sql_condition = join(" ", $sql_condition);

        if (!$this->nativeWhereCondition) {
            if (!$sql_condition) {
                $sql_condition = 1;
            }
        } else {
            if (!$sql_condition) {
                $sql_condition = $this->nativeWhereCondition;
            } else {
                $sql_condition .= " ".$this->nativeWhereCondition;
            }
        }

        // Fields to select
        $selectFields = array();
        if (is_array($this->selectFields) && sizeof($this->selectFields)) {

            // force add field `id`
            if ($forseAddId) {
                if (is_string($forseAddId)) {
                    $field = $forseAddId . '.id';
                } else {
                    $field = 'id';
                }
                $this->selectFields[] = $this->dbFieldQuote($field);
            }

            foreach ($this->selectFields as $field) {
                $selectFields[] = $this->dbFieldQuote(addslashes($field));
            }
        } elseif (!is_array($this->selectFields)) {
            $selectFields[] = $this->dbFieldQuote(addslashes($this->selectFields));
        }
        $selectFields = implode(", ", $selectFields);
        if (!$selectFields) {
            $selectFields = '*';
        }

        // remove content of the $this->selectFields in order to prevent conflicts with further SELECT requests
        $this->selectFields = array();

        $sqlLimitCondition  = '';
        // calculate limit and offset
        if($this->start !== null){
            $sqlLimitCondition = ' LIMIT '.$this->start.', '.$this->limit;
            $is_limited = true;
        }else{
            $is_limited = false;
        }

        if (is_array($table)) {
            $table = addslashes(implode(", ", $table));
        } else {
            $table = addslashes($table);
        }

        $cacheStateOld = $this->cacheEnabled;
        if($this->is_total_count) //disable cache if need SQL_CALC_FOUND_ROWS
        {
            $selectFields = 'SQL_CALC_FOUND_ROWS '. $selectFields;
            $this->cacheEnabled = false;
        }

        $filters                    = $this->generateFilters();

        if(empty($sql_condition))
        {
            $sql_condition          = $filters;
        }
        elseif(!empty($filters))
        {
            $sql_condition          .= ' AND '.$filters;
        }

        $sql = "SELECT " . $selectFields . " FROM " . addslashes($table)
            . " WHERE " . $sql_condition
            . ($orderCondition ? ' ORDER BY ' . $orderCondition : '')
            . ($groupByField ? ' GROUP BY ' . $groupByField : '');

        if(!empty($sqlLimitCondition))
        {
            $sql .= ' '.$sqlLimitCondition;
        }

        // echo "query: $sql\n";
        // echo "start reading cache\n";
        // echo "read cache content: \n";
        // print_r($return);
        // echo "cache content is ".(is_null($return) ? "null\n" : "not null\n");
        $return = $this->do_read_cache($sql);
        if(is_null($return)){
            // echo 'no cache. Execute: '.$sql."\n";
            $msc = microtime(true);
            $this->exec_query($sql);
            $queryTTE = microtime(true)-$msc;

            $return = $this->fetchAll();
            $this->db_write_cache($sql, $return, $queryTTE);
        }

        /*
        if($is_limited){
            $totalRecords = sizeof($return);
            if(!$this->parent->PAGE) $this->parent->PAGE = 1;
            $offSet = ($this->parent->PAGE - 1) * $this->parent->LIMIT;
            $totalPages = ceil($totalRecords / $this->parent->LIMIT);

            $this->parent->RESPONSE_PAGINATION = array(
                'totalPages' => $totalPages,
                'totalRecords' => $totalRecords,
                'currentPage' => $this->parent->PAGE,
                'perPage' => $this->parent->LIMIT,
            );
            $return = array_slice($return, $offSet, $this->parent->LIMIT);
        }
        */

        $this->limit = null;
        $this->start = null;
        $this->is_total_count = false;
        $this->cacheEnabled = $cacheStateOld;

        $this->resetFilters();

        $this->nativeWhereCondition = '';
        return $return;

    }

    /**
     * insert
     * @param string $table A name of table
     * @param string $data Data array
     */
    public function insert($table, $data, $is_multiple = false)
    {
        $addfields = array();
        $c = 0;
        if ($is_multiple) {
            $c = count($data);
            foreach ($data as $v) {
                $add_fields = array();
                $tables = array();
                foreach ($v as $key => $val) {
                    $add_fields[] = !is_null($val) ? "'" . addslashes($val) . "'" : 'NULL';
                    $tables[] = $this->dbFieldQuote($key);
                }
                $add_fields = join(", ", $add_fields);
                $addfields[] = "(" . $add_fields . ")";
                // if(!$add_fields) return 0;
            }
        } else {
            $add_fields = array();
            $tables = array();
            foreach ($data as $key => $val) {
                $add_fields[] = !is_null($val) ? "'" . addslashes($val) . "'" : 'NULL';
                $tables[] = $this->dbFieldQuote($key);
            }
            $add_fields = join(", ", $add_fields);
            $addfields[] = "(" . $add_fields . ")";
            // if(!$add_fields) return 0;
        }
        $add_fields = join(", ", $addfields);
        $tables = sprintf("(%s)", join(", ", $tables));

        if ($add_fields) {
            $sql = 'INSERT INTO ' . $this->dbFieldQuote($table) . ' ' . $tables . ' VALUES ' . $add_fields;
        } else {
            $sql = "INSERT INTO " . $this->dbFieldQuote($table) . " SET `id` = NULL";
        }
        $execute = $this->exec_query($sql);

        // if was error - return false
        if (!$execute) {
            return false;
        }

        // if wasn't error get the last inserted ID
        $lastInsertId = $this->pdo->lastInsertId();

        // if the last inserted ID is 0 than there is no id field in the table. Return -1
        if (!$lastInsertId) {
            return -1;
        }

        if ($c > 0) { // multiple insert
            $return = array();
            for ($i = 0; $i <= $c - 1; $i++) {
                $return[] = $lastInsertId - 1;
            }
        } else {
            $return = $lastInsertId;
        }

        return $return;
    }

    /**
     * update
     * @param string $table Table's name
     * @param string $data An associative array
     * @param string $where WHERE query part
     */


    public function update($table, $data, $where)
    {
        ksort($data);

        $fieldDetails = array();
        foreach ($data as $key => $value) {
            $fieldDetails[] = $this->dbFieldQuote($key) . " = " . (!is_null($value) ? "'" . addslashes($value) . "'" : 'NULL');
        }
        $fieldDetails = join(", ", $fieldDetails);

        if ($fieldDetails) {
            $sql = "UPDATE $table SET $fieldDetails WHERE $where";
            $exec = $this->exec_query($sql);

            $this->updateCount = $this->rowCount();

        } else {
            $exec = false;
        }

        return $exec;

    }

    /**
     * delete
     *
     * @param string $table
     * @param string $where
     * @param integer $limit
     * @return integer Affected Rows
     */
    public function delete($table, $where)
    {
        $query = "DELETE FROM $table WHERE $where";
        return $this->exec_query($query);
    }

    function exec_query($query = '', $is_log = true)
    {
        if (!$query) {
            return false;
        }
        echo (defined("DEBUGMODE") && DEBUGMODE) ? "prepare: ".$query."\n\n" : '';
        $this->sth = $this->pdo->prepare($query);

        $msc = microtime(true);
        $res = $this->sth->execute() ? true : false;
        $queryTTE = microtime(true)-$msc;
        $this->totals['sqlQueries']++;

        // $this->sth->closeCursor();
        /* write SQL log */
        // $f = fopen(DOCUMENT_ROOT.'/cache/query_'.time().'_'.rand(0,1000).".txt", 'w');
        // fwrite($f, $query."\n\n------\n".$res);
        // fclose($f);

        //echo "\n$query\n";
        $queryAr = explode(" ", trim($query));
        $type = strtoupper(trim($queryAr[0]));
        if($type != 'SELECT'&&0){
            if(
                (
                    $type == 'INSERT' &&
                    trim(trim($queryAr[2]),'`') != 'api_sessions' &&
                    trim(trim($queryAr[2]),'`') != 'protected_videos_log' &&
                    trim(trim($queryAr[2]),'`') != 'api_sys_log'
                ) ||
                (
                    $type == 'UPDATE' &&
                    trim(trim($queryAr[1]),'`') != 'api_sessions'
                ) ||
                $type == 'DELETE'
            ){

                if($type == 'INSERT' || $type == 'DELETE'){
                    $tableName = $queryAr[2];
                }else{
                    $tableName = $queryAr[1];
                }
                $tableName = str_replace("`", "", $tableName);
                $tableName = str_replace("'", "", $tableName);
                $tableName = str_replace('"', "", $tableName);
                $tableName = trim($tableName);

                $sql = "INSERT INTO `sql_log` SET `timestamp`=".time().", `query`='".addslashes($query)."', `query_type` = '".$type."', `api_token` = '".UserHelper::Token()."', `table_name` = '".addslashes($tableName)."'";
                $e = $this->pdo->prepare($sql);
                $e->execute();

                $table = ($type == 'UPDATE' ? trim(trim($queryAr[1]),'`') : trim(trim($queryAr[2]),'`'));
                $this->allUpdatedTables[] = $table;
                $this->clear_cacheTableSum($table);
            }
        }else{

        }

        if($this->isNoLogQueries)
        {
            $is_log                 = false;
        }

        if($is_log)
        {
            $this->totals['sqlQueriesDetails'][] = array
            (
                'query' => $query,
                'type' => $type,
                'success' => $res,
                'tte' => $queryTTE,
                'timestamp' => time(),
                'fromCache' => false
            );
        }

        return $res;
    }

    function beginTransaction()
    {
        return $this->pdo->beginTransaction();
    }

    function commit()
    {
        return $this->pdo->commit();
    }

    function rollBack()
    {
        return $this->pdo->rollBack();
    }

    function quote($data)
    {
        return $this->pdo->quote($data);
    }

    function fetchAll()
    {
        return $this->sth->fetchAll($this->pdoFetchMode);
    }

    function fetch()
    {
        return $this->sth->fetch($this->pdoFetchMode);
    }

    function rowCount()
    {
        return $this->sth->rowCount();
    }

    public function dbFieldQuote($field = '')
    {
        $field = trim($field);

        if($field === '*')
        {
            return $field;
        }

        $patterns = array
        (
            "/min\(\`\S*\`\)/", "/max\(\`\S*\`\)/", "/count\(\`\S*\`\)/", "/distinct\`\S*\`/",
            '/unix_timestamp\(\`\S*\`\).*/'
        );
        foreach ($patterns as $pattern){
            if(preg_match ( $pattern, strtolower($field)))
                return $field;
        }

        $field = addslashes(str_replace("`", "", $field));
        $field = str_replace(".", "`.`", $field);
        if ($field != '*') {
            $field = "`" . $field . "`";
        }

        // fix for `alias`.*
        if(substr($field, -strlen('.`*`')) === '.`*`')
        {
            $field  = substr($field, 0, -strlen('.`*`')).'.*';
        }

        $field = str_replace(" as ", "` as `", $field);
        return $field;
    }

    public function where($fistValue, $compSing, $secondValue, $logicOperator = "&&", $isBlock = false)
    {
        $accCompSigns = array("=", "!=", ">", "<", ">=", "<=", "IN", "NOT IN", "LIKE", "NOT LIKE");
        $LogicalOperators = array("AND", "&&", "NOT", "!", "||", "OR", "XOR");
        $condition = "";

        if (in_array($compSing, $accCompSigns)) {

            switch ($compSing) {
                case "IN":
                case "NOT IN":

                    if (!is_array($secondValue)) {
                        $secondValue = explode(",", $secondValue);
                    }

                    $values = "('" . implode("', '", $secondValue) . "')";
                    $condition = $fistValue . " " . $compSing . " " . $values;
                    break;

                case "LIKE":
                case "NOT LIKE":
                default:
                    $secondValue = "'" . $secondValue . "'";
                    $condition = $fistValue . " " . $compSing . " " . $secondValue;
            }

            if ($isBlock) {
                $condition = "(" . $condition . ")";
            }

            if (in_array($logicOperator, $LogicalOperators)) {
                $condition .= " " . $logicOperator . " ";
            }

            return $condition;
        }
    }

    public function block($statment, $logicOperator = null)
    {

        $statment = "(" . $statment . ")";
        $LogicalOperators = array("AND", "&&", "NOT", "!", "||", "OR", "XOR");

        if (in_array($logicOperator, $LogicalOperators)) {
            $statment .= " " . $logicOperator . " ";
        }

        return $statment;
    }

    function db_write_cache($query = '', $result = array(), $queryTTE = null){
        if(!$this->cacheEnabled) return false;
        if($this->is_not_cachable($query)) return false;

        $cacheFilePath = $this->cache_get_filePath($query);

        $cache = array(
            'query' => $query,
            'result' => $result,
            'queryTTE' => $queryTTE,
        );

        $dirname = dirname($cacheFilePath);
        if(!is_dir($dirname)){
            mkdir($dirname, 0777);
        }

        $f = fopen($cacheFilePath, 'w');
        fwrite($f, json_encode($cache));
        fclose($f);

        $queryPeaksFilepath = CACHE_PATH.'/db/queryPeaks.json';
        $updateQueryPeaksFile = false;

        $queryPeaks = json_decode(file_get_contents($queryPeaksFilepath), true);
        if(!is_array($queryPeaks)) $queryPeaks = array('max' => array(), 'long' => array());
        $maxTime = floatval($queryPeaks['max']['queryTTE']);

        $cacheFilePath4qp = str_replace(DOCUMENT_ROOT,"",$cacheFilePath);
        if($queryTTE > $maxTime){
            $queryPeaks['max'] = array(
                'queryCacheFile' => $cacheFilePath4qp,
                'queryTTE' => $queryTTE,
                'timeStamp' => time(),
            );
            $updateQueryPeaksFile = true;
        }
        if($queryTTE > 0.5 && $queryTTE <= $maxTime){
            $queryPeaks['long'][] = array(
                'queryCacheFile' => $cacheFilePath4qp,
                'queryTTE' => $queryTTE,
                'timeStamp' => time(),
            );
            $updateQueryPeaksFile = true;
        }

        if($updateQueryPeaksFile){
            $f = fopen($queryPeaksFilepath, 'w');
            fwrite($f, json_encode($queryPeaks));
            fclose($f);
        }

        return true;
    }

    function do_read_cache($query = ''){
        // echo "reading cache for $query\n";
        if(!$this->cacheEnabled) return null;
        $queryMD5 = md5(strtolower($query));

        if(!$this->cacheTableSums[$queryMD5]){
            $queryTables = $this->get_cache_query_tables($query);

            // calculating query tables sum
            if(!sizeof($queryTables)) return null;
            if($this->is_not_cachable($query)) return null;

            $this->calculate_cache_table_sum($query);
        }
        $msc = microtime(true);

        $cacheFilePath = $this->cache_get_filePath($query);
        if(is_file($cacheFilePath) && (filemtime($cacheFilePath) + $this->cacheTTL) >= time()){
            $cacheContent = json_decode(file_get_contents($cacheFilePath), true);
            if(!is_array($cacheContent['result'])){
                return null;
            }else{
                $this->totals['readFromCache']++;

                $queryAr = explode(" ", $query);
                $type = strtoupper(trim($queryAr[0]));


                $queryTTE = microtime(true)-$msc;

                $this->totals['sqlQueriesDetails'][] = array(
                    'query' => $query,
                    'type' => $type,
                    'success' => true,
                    'tte' => $queryTTE,
                    'fromCache' => true
                );

                return $cacheContent['result'];
            }
        }else{
            return null;
        }

    }

    function get_cache_query_tables($query = ''){
        $query = strtolower($query);
        $queryMD5 = md5($query);

        if(isset($this->cacheQueryTables[$queryMD5])) return $this->cacheQueryTables[$queryMD5];

        preg_match_all("/from(.*)where/is", $query, $query_ar);
        $queryTables = explode(",", trim($query_ar[1][0]));
        foreach ($queryTables as $key => $table) {
            $table = explode(" ", trim($table));
            $queryTables[$key] = addslashes(preg_replace("/[`'\"\s]/", "", $table[0]));
        }
        asort($queryTables);

        $this->cacheQueryTables[$queryMD5] = $queryTables;
        return $queryTables;
    }

    function is_not_cachable($query = ''){
        $query = strtolower($query);
        $queryMD5 = md5($query);

        if(!isset($this->cacheQueryTables[$queryMD5])) $this->get_cache_query_tables($query);

        if(in_array('api_sessions', $this->cacheQueryTables[$queryMD5])) return true;
        else return false;
    }

    function calculate_cache_table_sum($query = ''){
        $query = strtolower($query);
        $queryMD5 = md5($query);

        if(isset($this->cacheTableSums[$queryMD5])) return $this->cacheTableSums[$queryMD5];
        if(!isset($this->cacheQueryTables[$queryMD5])) $this->get_cache_query_tables($query);

        $requestTable = implode(", ", $this->cacheQueryTables[$queryMD5]);
        $requestTableMD5 = md5($requestTable);
        if(isset($this->cacheTableSums[$requestTableMD5])){
            $this->cacheTableSums[$queryMD5] = $this->cacheTableSums[$requestTableMD5];
            return $this->cacheTableSums[$queryMD5];
        }

        $alreadyHaveTableSums = array();
        $requestTable = $this->cacheQueryTables[$queryMD5];

        foreach ($requestTable as $k => $table) {
            $sum = $this->get_cacheTableSum($table);
            if($sum > 0){
                $alreadyHaveTableSums[] = array('Table' => $table, 'Checksum' => $sum);
                unset($requestTable[$k]);
            }
        }
        $resultCheckSum = array();
        if(is_array($requestTable) && sizeof($requestTable)){
            $requestTableStr = implode(", ", $requestTable);
            if($this->cacheEnabled){
                $sql = "CHECKSUM TABLE ".$requestTableStr;
                $this->exec_query($sql);
                $resultCheckSum = $this->fetchAll();    
            }else{
                $resultCheckSum = 0;
            }
        }
        $tableSums = array_merge($resultCheckSum, $alreadyHaveTableSums);

        $this->cacheTableSums[$queryMD5] = 0;
        $this->cacheTableSums[$requestTableMD5] = 0;
        foreach ($tableSums as $table) {
            $this->cacheTableSums[$queryMD5] += $table['Checksum'];
            $this->cacheTableSums[$requestTableMD5] += $table['Checksum'];

            $trimmedTableName = str_replace($this->currentDataBaseName.".", "", $table['Table']);
            if(in_array($trimmedTableName, $requestTable)){
                $this->write_cacheTableSum($trimmedTableName, $table['Checksum']);
            }
        }
    }

    function cache_get_filePath($query = ''){
        $queryMD5 = md5(strtolower($query));
        // print_r($this->cacheTableSums);
        $cacheFileName = $queryMD5.'/'.$this->cacheTableSums[$queryMD5];
        $cacheFilePath = CACHE_PATH.'/db/'.$cacheFileName.'.json';
        return $cacheFilePath;
    }

    function getTotals($key = null){
        if(is_null($key)) return $this->totals;
        return $this->totals[$key];
    }

    function getTotalCount(){

        if(!$this->exec_query('SELECT FOUND_ROWS() as `total`'))
        {
            return null;
        }

        $result     = $this->fetchAll();

        return $result[0]['total'];
    }

    function get_allUpdatedTables(){
        return array_unique($this->allUpdatedTables);
    }

    function __destruct(){
        $allUpdatedTables = $this->get_allUpdatedTables();
        if(is_array($allUpdatedTables) && sizeof($allUpdatedTables)){
            $requestTable = implode(", ", $allUpdatedTables);

            if($this->cacheEnabled){
                $sql = "CHECKSUM TABLE ".$requestTable;
                $this->exec_query($sql);
                $tableSums = $this->fetchAll();

                foreach ($tableSums as $tableSum) {
                    $this->write_cacheTableSum($tableSum['Table'], $tableSum['Checksum']);
                }
            }
        }
    }

    function write_cacheTableSum($table, $sum){
        if(!$table) return false;
        $tableCheckSumCachePath = $this->get_cacheTableSumPath($table);

        $f = fopen($tableCheckSumCachePath, 'w');
        fwrite($f, $sum);
        fclose($f);
    }

    function clear_cacheTableSum($table){
        $tableCheckSumCachePath = $this->get_cacheTableSumPath($table);
        if(is_file($tableCheckSumCachePath)) unlink($tableCheckSumCachePath);
    }

    function get_cacheTableSumPath($table){
        return CACHE_PATH.'/db_tables_checksum/'.$this->currentDataBaseName.'.'.$table.'.json';
    }

    function get_cacheTableSum($table){
        if(!$table) return 0;
        $tableCheckSumCachePath = $this->get_cacheTableSumPath($table);

        if(is_file($tableCheckSumCachePath)) return file_get_contents($tableCheckSumCachePath);
        else return 0;
    }

    public function definePagination($start, $limit, $is_total_count = true)
    {
        $this->start                = $start;
        $this->limit                = $limit;
        $this->is_total_count       = $is_total_count;
    }

    public function applyFilters(array $filters, array $filters_spec)
    {
        $this->filters              = $filters;
        $this->filters_spec         = $filters_spec;

        return $this;
    }

    private function generateFilters()
    {
        if($this->filters_spec === null || empty($this->filters))
        {
            return '';
        }

        $filters                    = [];

        foreach($this->filters_spec as $field => $spec)
        {
            if(!array_key_exists($field, $this->filters))
            {
                continue;
            }

            if(isset($spec['search']) && is_array($spec['search']))
            {
                // build search expression: field1 LIKE "%text%" OR field2 LIKE "%text%"
                $value              = $this->quote('%'.$this->filters[$field].'%');
                $search             = [];

                foreach($spec['search'] as $item)
                {
                    $search[]       = "`$item` LIKE $value";
                }

                $filters[]          = '('.implode(' OR ', $search).')';
            }
            else
            {
                $value              = $this->quote($this->filters[$field]);

                $filters[]          = "`{$spec['field']}`=$value";
            }
        }

        if(empty($filters))
        {
            return '';
        }

        return implode(' AND ', $filters);
    }

    public function resetFilters()
    {
        $this->filters              = [];
        $this->filters_spec         = null;

        return $this;
    }

    public function free()
    {
        $this->sth->closeCursor();
        $this->sth                  = null;
    }
}
