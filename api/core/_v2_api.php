<?php

class api
{
    var $REQUEST_URI = array();
    var $DATA = ''; // array of incoming data
    var $OUTPUT_FORMAT = ''; // json by default
    var $METHOD = null; // request method
    var $EXTEND_FIELDS = array(); // extend output with these fields
    var $CONTENT_TYPE = ''; // content type
    var $ENCODING = ''; // encoding
    var $HTTP_HEADERS = array(); // additional headers commands
    var $HTTP_CODE = 200; // define default HTTP code as 200
    var $HTTP_STATUS = ''; // HTTP status
    var $HTTP_REFERER_HOST = '';  // where the request comes from, eg www.knowledgecity.com
    var $HTTP_REFERER_SCHEME = ''; //eg 'http'
    var $HTTP_REFERER_SERVER = '';  //eg 'https://www.knowledgecity.com'
    var $REQUEST_HASH = ''; // hash request (md5 of serialized DATA)
    var $OFFSET = 0;
    var $LIMIT = 0;
    var $START = null;
    var $TOTAL = null;
    var $PAGE = 1;
    var $ORDER_CONDITION = array(); // order condition. Two parameters: field and direction
    var $URL = null;
    var $no_count_usage = false; // do not write token usage time in api_session table
    var $RequestComplete = false;
    var $logger = null;
    var $DB = '';
    var $class = null;
    var $TIMESTART = 0; // time in seconds+milliseconds of begining of executing the script
    var $RESPONSE_PAGINATION = array();
    var $resource_file = '';

    public function __construct(
        $url = '',
        $inputData = array(),
        $subResource = true,
        $timeStart = 0,
        $as_root_user = false,
        $parent = null
    ) {
        global $DEFAULT_HTTP_CODES;

        $this->URL = $url;
        $this->class = new stdClass();
        $this->ENCODING = RESULT_DEFAULT_ENCODING;
        $this->CONTENT_TYPE = RESULT_DEFAULT_CONTENT_TYPE;
        $this->DEFAULT_HTTP_CODES = $DEFAULT_HTTP_CODES;
        $this->TIMESTART = $timeStart;
        $this->subResource = $subResource; // TODO
        UserHelper::IsRoot($as_root_user);

        //info about where request comes from (not reliable)
        if (isset($_SERVER['HTTP_REFERER']) && $parts = parse_url($_SERVER['HTTP_REFERER'])) {
            $this->HTTP_REFERER_SCHEME = $parts["scheme"];
            $this->HTTP_REFERER_HOST = $parts["host"];
            $this->HTTP_REFERER_SERVER = $parts["scheme"] . "://" . $parts["host"];
        }

        /////////// SYSTEM PARAMS //////////////////////////////////////////////////////////////////////////////////////
        $systemParams = array(
            "token",
            "_body",
            "_output",
            "_extend",
            "_method",
            "_offset",
            "_limit",
            "_page",
            "_start",
            "_order"
        );

        foreach ($systemParams as $paramName) {
            if (array_key_exists($paramName, $inputData)) {
                switch ($paramName) {
                    case "token":   // define and check token
                        UserHelper::Token($inputData[$paramName]);
                        break;
                    case "_body":

                        define("OUTPUT_BODY", filter_var($inputData['_body'], FILTER_VALIDATE_BOOLEAN));

                        break;
                    case "_output":// format of output data

                        $this->OUTPUT_FORMAT = $inputData['_output'] ? $inputData['_output'] : RESULT_DEFAULT_OUTPUT_TYPE;

                        break;
                    case "_extend":// extend output with the given fields

                        if (!empty($inputData['_extend'])) {
                            $this->EXTEND_FIELDS = explode(',', $inputData['_extend']);
                        } else {
                            $this->EXTEND_FIELDS = array();
                        }

                        break;
                    case "_method":

                        $allowed_methods = array('get', 'post', 'put', 'delete');
                        $inputData['_method'] = strtolower($inputData['_method']);

                        if (is_null($this->METHOD) && in_array($inputData['_method'], $allowed_methods)) {
                            $this->METHOD = $inputData['_method'];
                        }

                        break;
                    case "_offset":

                        $this->OFFSET = (int)$inputData['_offset'];

                        break;
                    case "_limit": // define LIMIT

                        $this->LIMIT = (int)$inputData['_limit'];

                        break;
                    case "_page":

                        $this->PAGE = (int)$inputData['_page'] > 1 ? (int)$inputData['_page'] : 1;

                        break;
                    case "_start":

                        $this->START = (int)$inputData['_start'];

                        break;

                    case "_order":

                        $order = explode(",", trim($inputData[$paramName]));
                        $this->ORDER_CONDITION['field'] = trim($order[0]);
                        $this->ORDER_CONDITION['direction'] = strtoupper($order[1]);
                        $allow_directions = array('ASC', 'DESC', 'RAND');
                        if (!in_array($this->ORDER_CONDITION['direction'], $allow_directions)) {
                            $this->ORDER_CONDITION['direction'] = $allow_directions[0];
                        }

                        break;
                }

                unset($inputData[$paramName]);
            } else { // Set Defalut
                switch ($paramName) {
                    case "_body":
                        define("OUTPUT_BODY", true);
                        break;
                    case "_method":
                        $this->METHOD = METHOD;
                        break;
                    case "_order":
                        $this->ORDER_CONDITION['direction'] = "ASC";
                        break;
                }
            }
        }

        $this->DATA = $inputData; // create array of incoming data
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if (!$subResource) {
            $this->db = DataBase::getInstance();
        } else {
            $this->db = $parent->db;
        }

        // run the resource
        $this->exec_resource();


    }

    public function getFilters()/*: array*/
    {
        if(!array_key_exists('_filters', $_REQUEST))
        {
            return [];
        }

        if(!is_array($_REQUEST['_filters']))
        {
            throw new  Exception('_filters must be an array', 412);
        }

        return $_REQUEST['_filters'];
    }

    public function hookClass($classPath = '', $classSpecialName = null)
    {
        $className = strpos($classPath, '/') ? end(explode('/', $classPath)) : $classPath;
        $classSpecialName = is_null($classSpecialName) ? $className : $classSpecialName;

        // prevent double objects
        if (!is_object($this->class->$classSpecialName)) {

            $class_file = CLASSES_PATH . '/' . $classPath . '.php';
            if (is_file($class_file)) {
                include_once $class_file;
                $this->class->$classSpecialName = new $className();
                $this->class->$classSpecialName->parent = $this;
            } else {
                throw new ErrorException('Couldn\'t hook class '.$className.' because it does not exist');
            }
        }

        return $this->class->$classSpecialName;
    }

    private function exec_resource()
    {
        $URI = UrlParser::pathArray();
        if (empty($URI)) {
            return $this->complete(400, 'Bad Request', 'Empty resource is not allowed');
        }


        $resource_file = array();
        foreach ($URI as $key => $value) {
            if (UrlParser::ID($key) !== false) {
                $value = '_id_';
            }
            $resource_file[] = $value;
        }

        // hack for virtual file
        if(isset($_GET['output']))
        {
            array_pop($resource_file);
            unset($_GET['output']);
            $this->DATA['output'] = true;
        }

        $resource_file_path = trim(implode('/', $resource_file));
        $resource_path = RESOURCES_PATH . '/' . $resource_file_path;
        $this->resource_file = $resource_file_path;

        if ($resource_file_path && is_dir($resource_path)) {
            $resource_file = $resource_path . '/' . strtolower($this->METHOD) . '.php';

            if (!is_file($resource_file)) {
                return $this->complete(405, 'Method Not Allowed',
                    'Requested method [' . $this->METHOD . '] for resource [' . UrlParser::path() . '] is not allowed');
            } else {
                ob_start(); // this is to avoid sending unexpected header information.
                include($resource_file);
                $this->complete($this->HTTP_CODE, $this->HTTP_STATUS);
                $text = ob_get_contents();
                ob_end_clean();
                return trim($this->complete($this->HTTP_CODE, $this->HTTP_STATUS));
            }
        } else {
            return $this->complete(404, 'Not Found', 'Requested resource [' . UrlParser::path() . '] does not exist');
        }
    }

    public function complete($httpCode = '', $httpStatus = '', $userMessage = '')
    {
        if ($this->RequestComplete) {
            return null;
        }
        $this->RequestComplete = true;

        if ($this->subResource) {
            $this->HTTP_CODE = $httpCode;
            $this->HTTP_STATUS = $httpStatus;
            if ($userMessage) {
                $this->RESPONSE = $userMessage;
            }
            return $this->get_output();
        }

        $httpCode = intval($httpCode);

        if (isset($this->DEFAULT_HTTP_CODES[$httpCode])) {
            $httpStatus = $this->DEFAULT_HTTP_CODES[$httpCode];
        }

        $http_code = $this->HTTP_CODE = $httpCode ? $httpCode : RESULT_DEFAULT_STATUS_CODE;
        $http_status = $this->HTTP_STATUS = $httpStatus ? $httpStatus : RESULT_DEFAULT_STATUS_TEXT;
        if ($userMessage) {
            $this->RESPONSE = $userMessage;
        }

        $sapi_type = php_sapi_name();
        if (substr($sapi_type, 0, 3) == 'cgi') {
            header("Status: " . $http_code . " " . $http_status);
        } else {
            header(HTTP_VERSION . " " . $http_code . " " . $http_status);
        }

        header("Last-Modified: " . gmdate('D, d M Y H:i:s \G\M\T', time()));
        header("Content-Type:" . $this->CONTENT_TYPE . "; charset=" . $this->ENCODING);
        header("Access-Control-Allow-Origin: *");

        if (sizeof($this->HTTP_HEADERS) && is_array($this->HTTP_HEADERS)) {
            foreach ($this->HTTP_HEADERS as $command) {
                eval("header('" . $command . "');");
            }
        }

        $output = $this->get_output();

        return $output;
    }

    public function outputFields($outputFields = array(), $availableExtendFields = array(), $limitFields = array())
    {
        if (UserHelper::UserType()) {
            $key = UserHelper::UserType();
        } else {
            $key = '*';
        }

        $aef = isset($availableExtendFields[$key]) ? $availableExtendFields[$key] : $availableExtendFields['*'];

        if (!isset($outputFields[$key])) {
            $key = '*';
        }
        $outputFields = $outputFields[$key];

        if (is_array($this->EXTEND_FIELDS) && $aef) {
            foreach ($this->EXTEND_FIELDS as $field) {
                if (in_array($field, $aef)) {
                    $outputFields[] = $field;
                } elseif (array_key_exists($field, $aef)) {
                    if (!$aef[$field]) {
                        continue;
                    }
                    $outputFields[] = $aef[$field];
                }
            }
        }

        if (sizeof($limitFields)) {
            foreach ($limitFields as $k => $field) {
                $limitFields[$k] = $field; // make sure the field has `
            }
            $outputFields = array_intersect($outputFields, $limitFields);
        }

        return $outputFields;
    }

    public function orderCondition($OrderField = 'id', $OrderDir = 'ASC', $altOrderFields = array())
    {
        if ($this->ORDER_CONDITION['direction'] == 'RAND') {
            return 'RAND()';
        }
        if (
            in_array($this->ORDER_CONDITION['field'], $altOrderFields, true) ||
            ($this->ORDER_CONDITION['field'] == $OrderField && $OrderField)
        ) {
            $OrderField = $this->ORDER_CONDITION['field'];
            $OrderDir = $this->ORDER_CONDITION['direction'];
        } elseif (
            $this->ORDER_CONDITION['field'] &&
            array_key_exists($this->ORDER_CONDITION['field'], $altOrderFields, true)
        ) {
            $OrderField = $altOrderFields[$this->ORDER_CONDITION['field']];
            $OrderDir = $this->ORDER_CONDITION['direction'];
        }
        return $OrderDir == 'RAND' ? 'RAND()' : $this->db->dbFieldQuote($OrderField) . ' ' . $OrderDir;
    }

    public function prepareQueryCondition($add2conditionFields = array())
    {
        if (!is_array($add2conditionFields) || !sizeof($add2conditionFields)) {
            return array();
        }

        $condition = array();
        foreach ($add2conditionFields as $dbField => $postField) {

            $prefix = substr($postField, 0, 1);
            $suffix = substr($postField, -2);

            if ($suffix == "[]") {
                $postField = substr($postField, 0, -2);
                if (empty($this->DATA[$postField])) {
                    continue;
                }
                $in = "'" . implode("','", $this->DATA[$postField]) . "'";
                $condition[$postField] = "IN,(" . $in . ")";
            } else {
                switch ($prefix) {
                    case '~':
                        $postField = substr($postField, 1);
                        $flex = true;
                        $range = false;
                        break;
                    case '-':
                        $postField = substr($postField, 1);
                        $flex = false;
                        $range = true;
                        break;
                    default:
                        $flex = false;
                        $range = false;
                        break;
                }

                if (is_numeric($dbField)) {
                    $dbField = $postField;
                }

                if (isset($this->DATA[$postField])) {
                    if (substr($this->DATA[$postField], 0, 1) == '~' && $flex) {
                        $condition[$dbField] = "LIKE,'%" . addslashes(substr($this->DATA[$postField], 1)) . "%'";
                    } elseif ($range) {
                        list($range_from, $range_to) = explode("~", $this->DATA[$postField]);
                        $range_1 = $this->queryRangeCondition($range_from, $range_to);
                        if ($range_1) {
                            $condition[$dbField] = $range_1;
                        }
                    } else {
                        $condition[$dbField] = "'" . addslashes($this->DATA[$postField]) . "'";
                    }
                }
            }
        }

        return $condition;
    }

    public function queryRangeCondition($start = '', $end = '', $inclusive = true)
    {
        if ($inclusive) {
            $ls = "<=";
            $ms = ">=";
        } else {
            $ls = "<";
            $ms = ">";
        }
        $ret = '';

        $start = is_numeric($start) ? $start : ($start ? "'" . $start . "'" : '');
        $end = is_numeric($end) ? $end : ($end ? "'" . $end . "'" : '');
        if (
            (is_numeric($start) || !empty($start)) &&
            (is_numeric($end) || !empty($end))
        ) {
            $ret = "BETWEEN," . $start . " AND " . $end;
        } elseif ($start != '') {
            $ret = $ms . "," . $start;
        } elseif ($end != '') {
            $ret = $ls . "," . $end;
        }

        return $ret;
    }

    public function preparePostFields($acceptPostFields = array())
    {

        foreach ($acceptPostFields as $field => $value) {
            list($fieldKey, $fieldLabel) = explode(',', $field, 2);

            if (substr($fieldKey, 0, 1) == '*') {
                $fieldKey = substr($fieldKey, 1);


                if (!$value) {
                    if (!$fieldLabel) {
                        $fieldLabel = $fieldKey;
                    }
                    $this->HTTP_CODE = 412;
                    $this->RESPONSE['message'] = "Field '" . $fieldLabel . "' is required.";
                    return false;
                }
            } elseif (substr($fieldKey, 0, 1) == '!') {
                $fieldKey = substr($fieldKey, 1);

                if (is_null($value)) {
                    if (!$fieldLabel) {
                        $fieldLabel = $fieldKey;
                    }
                    $this->HTTP_CODE = 412;
                    $this->RESPONSE['message'] = "Field '" . $fieldLabel . "' has to be specified.";
                    return false;
                }
            }

            unset($acceptPostFields[$field]);
            $acceptPostFields[$fieldKey] = $value;

        }

        return $acceptPostFields;
    }

    /**
     * Extract data from array
     * @param $fields
     * @param $data
     * @throws ErrorException
     * @return array|bool
     */
    public function prepareFields($fields, $data){

        $result                     = [];

        foreach ($fields as $targetKey) {

            if (substr($targetKey, 0, 1) == '*') {
                $targetKey = substr($targetKey, 1);
                if(!isset($data[$targetKey]) || strlen($data[$targetKey]) === 0) {
                    throw new ErrorException("Field '$targetKey' is required.", 412);
                    return false;
                }
            }

            //find extend options

            $keyOptions             = explode(':', $targetKey);
            $key                    = $keyOptions[0];

            if (isset($data[$key])) {
                $result[$key] = $data[$key];
            }else{

                if(count($keyOptions) > 1){
                    switch($keyOptions[1]){
                        case 'empty':
                            $result[$key] = '';
                            break;
                        case 'null':
                            $result[$key] = null;
                            break;
                    }
                }

            }
        }

        return $result;
    }

    public function Pagination($array = array(), $limit, $page, $save_paginator_meta = true, $strict = true)
    {
        static $applied;
        if ($applied && !$strict) {
            return $array;
        }
        $applied = true;

        if (is_null($limit)) {
            $limit = $this->LIMIT;
        }
        if (is_null($page)) {
            $page = $this->PAGE;
        }

        $limit = intval($limit);
        $page = intval($page);

        if($this->TOTAL === null)
        {
            return $array;
        }

        /*
        if (!(is_array($array) && sizeof($array) > 1 && $limit > 0)) {
            return $array;
        }
        */

        if($this->TOTAL !== null)
        {
            $totalRecords = $this->TOTAL;
        }
        else
        {
            $totalRecords = sizeof($array);
        }

        $totalPages = ceil($totalRecords / $limit);
        if ($page > $totalPages) {
            $page = $totalPages;
        }

        $offset = ($page - 1) * $limit;

        if ($save_paginator_meta) {
            $this->RESPONSE_PAGINATION = array(
                'totalRecords' => $totalRecords,
                'perPage' => $limit,
                'totalPages' => $totalPages,
                'start' => $this->START,
                'currentPage' => $page,
            );
        }

        $array = array_slice($array, $offset, $limit);

        return $array;
    }

    private function get_output($forseContent = false)
    {

        if (!OUTPUT_BODY && !$forseContent) {
            return '';
        }

        if (!$this->HTTP_CODE) {
            $this->HTTP_CODE = RESULT_DEFAULT_STATUS_CODE;
        }
        if (!$this->HTTP_STATUS) {
            $this->HTTP_STATUS = $this->DEFAULT_HTTP_CODES[$this->HTTP_CODE];
        }

        $this->RESPONSE = $this->Pagination($this->RESPONSE, $this->LIMIT, $this->PAGE, true, false);

        $output_handler = strtolower(OUTPUT_HANDLER_PATH . '/' . $this->OUTPUT_FORMAT . '.php');
        if (!is_file($output_handler)) {
            $output_handler = OUTPUT_HANDLER_PATH . '/' . RESULT_DEFAULT_OUTPUT_TYPE . '.php';
        }
        ob_start();
        include($output_handler);
        $this->OUTPUT = trim(ob_get_contents());
        ob_clean();

        return $this->OUTPUT;
    }

    public function accessLevel($lvl = null)
    {
        if( isset($this->DATA['api_secret']) && isset($this->DATA['accessType']) ) {

            $usedGUID = UserHelper::getUserGUIDbyApiSecret($this->DATA['api_secret']);

            if(!is_null($usedGUID)) {
                if(is_null($lvl))
                    return true;

                $userGroups = UserHelper::UserGroup($usedGUID);
                if($userGroups==false)
                    $userGroups=array();
                $userGroups[]=array('code' => 'student');// students are not 'real' group type in api_groups but some resources base access on it

                foreach ($userGroups as $userGroup) {

                    if( $userGroup['code'] == $this->DATA['accessType']) {
                        return true;
                    }
                }

            }
        }

        $login = UserHelper::AccessLevel($lvl);
        if ($login) {
            UserHelper::ExtendSession(UserHelper::Token(), SESSION_TTL, UserHelper::GetUserIP());
            return true;
        } else {
            $error = UserHelper::Error();
            if (!empty($error)) {
                $this->HTTP_CODE = UserHelper::Error()['code'];
                $this->RESPONSE['message'] = UserHelper::Error()['message'];
            }
            return false;
        }

    }

    public function createAccessByGroupEnv()
    {
        // get session
        $session                    = UserHelper::ValidToken(UserHelper::Token());

        // get access groups
        $query                      = "SELECT * 
FROM `admin_access_by_groups` WHERE api_users_id = '{$session['api_users_id']}'";

        if(!DataBase::getInstance()->exec_query($query))
        {
            throw new Exception('createAccessByGroupEnv failed', 500);
        }

        $result                     = DataBase::getInstance()->fetchAll();

        if(empty($result))
        {
            $this->ACCESS_BY_GROUPS =
            [
                'api_users_id'      =>  $session['api_users_id'],
                'access_mode'       => 'all',
                'groups'            => [],
                'original_groups'   => []
            ];

            return;
        }

        $result                     = $result[0];

        $result['groups']           = json_decode($result['groups'], true);

        if(!is_array($result['groups']))
        {
            $result['groups']       = [];
        }

        $result['original_groups']  = $result['groups'];

        $get_parent_group           = null;

        $get_parent_group           = function ($id) use(&$get_parent_group)
        {
            $query                  = "SELECT g.* FROM `student_groups` as g WHERE g.parent_id = '$id'";

            if(!DataBase::getInstance()->exec_query($query))
            {
                throw new Exception('DataBase exception', 500);
            }

            $result                 = DataBase::getInstance()->fetchAll();
            $groups                 = [];
            $parents                = [];

            foreach($result as $group)
            {
                $groups[]           = $group['id'];
                $parents            = array_merge($parents, $get_parent_group($group['id']));
            }

            return array_merge($groups, $parents);
        };

        $parents                    = [];

        foreach($result['groups'] as $group)
        {
            $parents                = array_merge($parents, $get_parent_group($group));
        }

        $result['groups']           = array_merge($result['groups'], $parents);

        $result['groups']           = array_unique($result['groups']);

        // get account_id by user_api
        $query                      = "SELECT * FROM `accounts_admins` WHERE api_users_id = '{$session['api_users_id']}'";

        if(!DataBase::getInstance()->exec_query($query))
        {
            throw new Exception('DataBase exception', 500);
        }

        $account                    = DataBase::getInstance()->fetchAll();

        if(empty($account))
        {
            throw new Exception('account_id is not found', 500);
        }

        $result['account_id']       = $account[0]['account_id'];

        $this->ACCESS_BY_GROUPS     = $result;
    }

    public function getAccessByGroupEnv(): array
    {
        if(!property_exists($this, 'ACCESS_BY_GROUPS'))
        {
            $this->createAccessByGroupEnv();
        }

        return $this->ACCESS_BY_GROUPS;
    }

    public function checkAccessByGroup($group_id)
    {
        $access_by_group            = $this->getAccessByGroupEnv();

        if(empty($access_by_group) || $access_by_group['access_mode'] === 'all')
        {
            return true;
        }
        elseif($access_by_group['access_mode'] === 'include')
        {
            return in_array($group_id, $access_by_group['groups']);
        }
        elseif($access_by_group['access_mode'] === 'exclude')
        {
            return !in_array($group_id, $access_by_group['groups']);
        }
        else
        {
            throw new Exception('access_mode is failed', 500);
        }
    }

    public function genSQLForAccessByGroup($prefix)
    {
        $access_by_group            = $this->getAccessByGroupEnv();

        if(empty($access_by_group) || $access_by_group['access_mode'] === 'all')
        {
            return '';
        }
        elseif($access_by_group['access_mode'] === 'exclude' && empty($access_by_group['groups']))
        {
            return '';
        }
        elseif($access_by_group['access_mode'] === 'include' && empty($access_by_group['groups']))
        {
            return "$prefix IN ('0')";
        }

        $groups                     = '"'.implode('","', $access_by_group['groups']).'"';

        if($access_by_group['access_mode'] === 'include')
        {
            return "$prefix IN ($groups)";
        }
        elseif($access_by_group['access_mode'] === 'exclude')
        {
            $groups                 = "SELECT id FROM `student_groups` 
WHERE id NOT IN ($groups) AND account_id='{$access_by_group['account_id']}'";

            return "$prefix IN ($groups)";
        }
        else
        {
            throw new Exception('access_mode is failed', 500);
        }
    }

    public function addGroupToAccess($group_id)
    {
        $access_by_group            = $this->getAccessByGroupEnv();

        if(!empty($access_by_group) && $access_by_group['access_mode'] !== 'include')
        {
            return;
        }

        $this->hookClass('v2/users');

        $access_by_group['original_groups'][] = $group_id;

        $this->class->users->updateAdminGroups
        (
            $access_by_group['api_users_id'],
            ['groups'            => $access_by_group['original_groups']]
        );
    }

    //////////////////////////////////////////////////////////////////////////////
    // takes a URL and returns a php object represent the json that the resource
    // would normally return.  Used to call a resource internally without
    // resorting to an http request.
    //////////////////////////////////////////////////////////////////////////////
    public function self_resource($resourceURL = '', $method = 'GET', $as_root = false, $DATA = array())
    {
        $resourceURL = trim(trim($resourceURL), '/');
        if (!$resourceURL) {
            return array(
                "code" => 400,
                "status" => $this->DEFAULT_HTTP_CODES['400'],
                "response" => "Empty resource is not allowed"
            );
        }

        UrlParser::startSubResource($resourceURL);

        $allowed_methods = array('get', 'post', 'put', 'delete');
        if (!in_array(strtolower($method), $allowed_methods)) {
            $method = 'GET';
        }

        $DATA['_method'] = $method;
        $DATA['_output'] = 'json';

        if (UserHelper::Token()) {
            $DATA['token'] = UserHelper::Token();
        }

        $subApi = new api($resourceURL, $DATA, true, 0, $as_root, $this);
        $_return = json_decode($subApi->OUTPUT, true);

        unset($subApi);
        UrlParser::endSubResource();
        return $_return;
    }

    public function runBackground($classNamefnName, ...$params)
    {
        // from a class, call this like:
        //
        //    $this->parent->runBackground("myclass->myMethod", $accountId, $other, $info);
        //
        // parameters are serialized and written to file, and background processor will
        // read/unserialize the file, hook the class, run via api wrapper, and mark the
        // parameters file done with number of seconds it took.

        $classNamefnName = str_replace('->', '.', $classNamefnName);
        $params = serialize($params);
        $t = time();
        $jobId = UserHelper::CreateGUID();
        $paramsFile = LOG_PATH."/bg/$t.$classNamefnName.job.$jobId";
        file_put_contents($paramsFile, $params);
        $php = PHP_EXEC;
        shell_exec($php." -f ". DOCUMENT_ROOT. "/background_fn.php $paramsFile > /dev/null 2>/dev/null &");

        return $jobId;
    }

    public function isDoneBackground($classNamefnName, $jobId)
    {
        $classNamefnName            = str_replace('->', '.', $classNamefnName);
        $jobFile                    = LOG_PATH."/bg/*$classNamefnName.job.$jobId*.done";

        // Find file
        return count(glob($jobFile)) > 0;
    }

    public function __destruct()
    {
        if (!$this->no_count_usage && $this->TOKEN) {
            $Fields = array('date_last_use' => date('Y-m-d H:i:s'));
            $this->db->update('api_sessions', $Fields,
                "`api_token`='" . addslashes($this->TOKEN) . "'");
        }
        if (!$this->subResource) {
            $date = date("Y-m-d H:i:s", $this->TIMESTART);

            if (is_null($this->RESPONSE)) {
                $response = 'NULL';
            } elseif (is_bool($this->RESPONSE)) {
                $response = $this->RESPONSE ? 'true' : 'false';
            } elseif (is_array($this->RESPONSE)) {
                $response = json_encode($this->RESPONSE);
            } elseif (empty($this->RESPONSE)) {
                $response = '';
            } else {
                $response = $this->RESPONSE;
            }

            $tte = (time() + microtime() - $this->TIMESTART);
            header("TTE: " . $tte); // total time of execution
            header("RequestID: " . API_REQUEST_GUID); // API reques id from api_sys_log table
            Logger::Write(
                UserHelper::LastSessionGUID(),
                strtoupper($this->METHOD),
                $this->URL,
                $this->DATA,
                $date,
                $tte,
                $this->HTTP_CODE,
                $response,
                $this->resource_file
                );
        }
    }

    /*
        ////////////////////////////////////////////////////////////////////////////////////
        // takes an array representing a url request, and checks the $NESTED_RULES to see
        // if there's a match.  So a url request like 'user/progress' might get rewritten
        // to 'countries/0{:myCountry}/citizens/{:myGovernmentID}/communityservice/progress'.
        // Any id numbers in the request, eg users/017/purchase/05  will be collected in
        // order and mapped to the matched rule by looking for tokens like {id0} {id1} {id2}
        // and replacing them with the id in the request.  Any 'tokens' in the long-form
        // url like "{:selfCountry:}" will be checked to be valid by the get_id function.
        // If no matching rule was found, then it just returns the url (as array) as is.
        // If a matching rule was found, it returns a new array representing a request
        // with the long-form url, and tokens where tokens were found, and with specific
        // ids in their new location.  In other words, any id numbers specified in the
        // original request will appear as id numbers (eg 045) in the result, but the
        // 'tokens' will still be tokens, not id numbers.
        // Example:
        //      input:   array('progress','02015')
        //      request: progress/02015
        //      rule:    progress/_id_>users/{:me:}/progresss/{id0}
        //      result:  users/{:me:}/progress/02015
        //      return:  array('users','{:me:}','progress','02015')
        ////////////////////////////////////////////////////////////////////////////////////
        private function check_routing($routArray = array())
        {
            global $NESTED_RULES; // this array is in sys.config.php file
            $gotIds = array();
            $normolisedRoutArray = array();
            //note, tokens like {:selfId:} should not appear in actual requests
            //so any ids found here will be actual ids like 034 or 0BUS1067
            //TODO: make it skip over tokens so long-form url's can be used (eg including tokens)
            foreach (
                as $k => $v) {
                $gotId = $this->get_id($v);
                if ($gotId !== false) {
                    $normolisedRoutArray[$k] = '_id_';
                    $gotIds[] = $gotId;
                } else {
                    $normolisedRoutArray[$k] = $v;
                }
            }
            //the new uri, with _id_  instead of number ids (so we can find the file)
            //example: account/023 became account/_id_
            $requestedURI = implode("/", $normolisedRoutArray);
            //echo "\nrequestedURI"; print_r($requestedURI);

            //now we rewrite any short form urls to long form
            //example:  me/progress/_id_ becomes
            //          accounts/0{:selfCompanyId:}/users/0{:selfId:}/progress/{id0}
            //and then: accounts/0{:selfCompanyId:}/users/0{:selfId:}/progress/0BUS1067
            //so that it looks as though the made the long form request, inluding the id's where needed
            foreach ($NESTED_RULES as $singleRule) {
                list($key, $value) = explode(NESTED_KEYVALUE_DELIMITER, $singleRule);
                $key = trim($key, '/');
                $value = trim($value, '/');

                if ($requestedURI == $key) {//if the request matches a rule
                    foreach ($gotIds as $idKey => $idValue) {
                        $value = str_replace("{id" . $idKey . "}", $idValue, $value);
                    }
                    $value = explode(NESTED_VALUE_DELIMITER, $value);
                    $routArray = $value;
                } else {
                }
            }
            //after the above, something like
            //  accounts/0{:selfCompanyId:}/users/0{:selfId:}/progress/{id0}
            //will look like:
            //  accounts/0{:selfCompanyId:}/users/0{:selfId:}/progress/0BUS1067

            return $routArray;
        }

    */

    /**
     * Method out exception
     *
     * @param       Exception       $e
     *
     * @return      null
     */
    public function outException(Exception $e)
    {
        $msg                        = $e->getMessage();

        if(!empty($msg))
        {
            $this->RESPONSE         = ['message' => $msg];
        }

        if(defined('DEBUG_EXCEPTION'))
        {
            $this->RESPONSE['trace'] = $e->getTrace();
            $this->RESPONSE['line'] = $e->getLine();
            $this->RESPONSE['file'] = $e->getFile();
        }

        $this->HTTP_CODE            = $e->getCode();
        $this->HTTP_STATUS          = 'Error';

        return null;
    }
}
