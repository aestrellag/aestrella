<?php


class DataBase
{
    private static $_instance = null;

    private function __construct()
    { //Prevent any oustide instantiation of this class
    }

    private function __clone()
    {
    } //Prevent any copy of this object


    public static function getInstance() // Single globally accessible static method
    {
        if (!is_object(self::$_instance)) {  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new db(
                defined("DB_V2_TYPE") ? DB_V2_TYPE : DB_TYPE,
                defined("DB_V2_HOST") ? DB_V2_HOST : DB_HOST,
                defined("DB_V2_NAME") ? DB_V2_NAME : DB_NAME,
                defined("DB_V2_USER") ? DB_V2_USER : DB_USER,
                defined("DB_V2_PASS") ? DB_V2_PASS : DB_PASS
                );
        }
        return self::$_instance;
    }

    public static function select(
        $table,
        $condition = array(),
        $orderCondition = '',
        $forseAddId = true,
        $groupByField = '',
        $fetchMode = PDO::FETCH_ASSOC
    ) {
        return DataBase::getInstance()->select($table, $condition, $orderCondition, $forseAddId, $groupByField, $fetchMode);
    }

    public static function insert($table, $data, $is_multiple = false)
    {
        return DataBase::getInstance()->insert($table, $data, $is_multiple);
    }

    public static function update($table, $data, $where)
    {
        return DataBase::getInstance()->update($table, $data, $where);
    }

    public static function delete($table, $where)
    {
        return DataBase::getInstance()->delete($table, $where);
    }
    public static function query($query)
    {
        return DataBase::getInstance()->exec_query($query);
    }


    public static function selectFields($selectFields)
    {
        return DataBase::getInstance()->selectFields = $selectFields;
    }

    public static function addNativeWhereCondition($condition)
    {
        return DataBase::getInstance()->nativeWhereCondition = $condition;
    }

    public static function getTotals($key = null){
        return DataBase::getInstance()->getTotals($key);
    }

    public static function applyPagination($api)
    {
        DataBase::getInstance()->definePagination($api->START, $api->LIMIT);
    }

    public static function applyFilters($api, array $filters_spec)
    {
        DataBase::getInstance()->applyFilters($api->getFilters(), $filters_spec);
    }

    public static function getTotalCount()
    {
        return DataBase::getInstance()->getTotalCount();
    }
}
