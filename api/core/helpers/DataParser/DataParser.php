<?php


class DataParser
{
    private static $_instance = null;

    private $_data = array();

    private function __construct()
    { //Prevent any oustide instantiation of this class
        try {
            parse_str(file_get_contents("php://input"), $POSTDATA);
            $this->_data = array_merge($_GET, $_POST, $POSTDATA);
            
            // sanitize token
            if(isset($this->_data['token']))
                $this->_data['token'] = preg_replace('/[^A-Za-z0-9\-\s]/', '', $this->_data['token']);

        } catch (Exception $e) {
            echo "Error while Request parameters Parsing";
        }
    }

    private function __clone()
    {
    } //Prevent any copy of this object

    private function _dataArray()
    {
        return $this->_data;
    }

    function _startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    function _endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    public static function getInstance() // Single globally accessible static method
    {
        if (!is_object(self::$_instance)) {
            self::$_instance = new DataParser();
        }
        return self::$_instance;
    }

    public static function getArray()
    {
        return DataParser::getInstance()->_dataArray();
    }

    public static function getParam($key)
    {
        return self::getArray($key);
    }

    public static function startsWith($haystack, $needle)
    {
        return DataParser::getInstance()->_startsWith($haystack, $needle);
    }

    public static function endsWith($haystack, $needle)
    {
        return DataParser::getInstance()->_endsWith($haystack, $needle);
    }
}