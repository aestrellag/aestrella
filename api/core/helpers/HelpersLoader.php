<?php

$helpers = array(
    "DataBase" => "DataBase",
    "UrlParser" => "UrlParser",
    "DataParser" => "DataParser",
    "UserHelper" => "UserHelper",
    "Logger" => "Logger",
);

foreach ($helpers as $helperClass) {
    include_once($helperClass . "/" . $helperClass . ".php");
}
