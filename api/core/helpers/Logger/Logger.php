<?php


class Logger
{
    private static $_instance = null;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function _write($session, $method, $resource, $params, $date, $time, $code, $text, $resourceFile)
    {
        if(!defined("DB_V2_LOGDB") || !DB_V2_LOGDB) return false;
        $sqls = array();

        if($resource == 'v2/auth'){
            if(isset($params['password'])) $params['password'] = '***';
        }
        
        $api_log_guid = API_REQUEST_GUID;
        $fields = array(
            "id" => $api_log_guid,
            "api_sessions_id" => $session ? (string)$session : NULL,
            "method" => strval($method),
            "resource" => strval($resource),
            "params" => json_encode($params),
            "date" => strval($date),
            "execution_time" => $time,
            "response_code" => strval($code),
            "response_text" => strval($text),
            "user_ip" => UserHelper::GetUserIP(),
            "SQLexecuted" => DataBase::getTotals('sqlQueries'),
            "SQLreadFromCache" => DataBase::getTotals('readFromCache'),
            "resource_file" => strval($resourceFile)
        );
        $session = $session ? (string)$session : NULL;
        $sqls[] = $sql = "UPDATE `".DB_V2_LOGDB."`.`api_sys_log` SET `api_sessions_id` = '".strval($session)."', `method` = '".strval($method)."', `resource` = '".strval($resource)."', `params` = '".addslashes(json_encode($params))."', `date` = '".strval($date)."', `execution_time` = '".$time."', `response_code` = '".strval($code)."', `response_text` = '".strval(addslashes($text))."', `user_ip` = '".UserHelper::GetUserIP()."', `SQLexecuted` = '".DataBase::getTotals('sqlQueries')."', `SQLreadFromCache` = '".DataBase::getTotals('readFromCache')."', `resource_file` = '".strval($resourceFile)."' WHERE `id` = '".$api_log_guid."'";
        // $insert_id = DataBase::insert('`'.DB_V2_LOGDB.'`.`api_sys_log`', $fields);
        
        // $sqls[] = "INSERT INTO `".DB_V2_LOGDB."`.`api_sys_log` (`id`,`api_sessions_id`,`method`,`resource`,`params`,`date`,`execution_time`,`response_code`,`response_text`,`user_ip`,`SQLexecuted`,`SQLreadFromCache`,`resource_file`) VALUES ('".$api_log_guid."','".($session ? (string)$session : NULL)."','".strval($method)."','".strval($resource)."','".addslashes(json_encode($params))."','".strval($date)."','".$time."','".strval($code)."','".addslashes(strval($text))."','".UserHelper::GetUserIP()."','".DataBase::getTotals('sqlQueries')."','".DataBase::getTotals('readFromCache')."','".strval($resourceFile)."')";
        
        // write SQL log
        $cache = DataBase::getTotals();
        $cache = $cache['sqlQueriesDetails'];
        // $cacheFilePath = LOG_PATH.'/resources_db_queries/'.$api_log_guid.'.json';
        $inserBlocks = array();
        foreach ($cache as $order => $value) {
            $inserBlocks[] = "('".$api_log_guid."', '".$order."', '".strval(trim($value['type']))."', '".strval(addslashes(trim($value['query'])))."', '".intval($value['success'])."', ".floatval($value['tte']).", ".floatval($value['timestamp']).", '".intval($value['fromCache'])."')";
        }

        $sqls[] = $sql = "INSERT INTO `".DB_V2_LOGDB."`.`api_resources_sql_queries` (`api_log_id`, `order`, `type`, `query`, `success`, `tte`,`timestamp`, `fromCache`) VALUES ".implode(", ", $inserBlocks);
        // DataBase::query($sql);
        Logger::writeApiLogFile(implode("\n----\n",$sqls));
        // $logFilePath = LOG_PATH."/api_log_queries/".API_REQUEST_GUID.".sql";
        // // echo $logFilePath;
        // file_put_contents($logFilePath, implode("\n----\n",$sqls));
            
        return $api_log_guid;
    }

    function writeApiLogFile($sql){
        $logFilePath = LOG_PATH."/api_log_queries/".API_REQUEST_GUID.".sql";
        file_put_contents($logFilePath, $sql, FILE_APPEND);
    }

    public static function getInstance()
    {
        if (!is_object(self::$_instance)) {
            self::$_instance = new Logger();
        }
        return self::$_instance;
    }

    public static function Write($session, $method, $resource, $params, $date, $time, $code, $text, $resourceFile)
    {
        return Logger::getInstance()->_write($session, $method, $resource, $params, $date, $time, $code, $text, $resourceFile);
    }
}
