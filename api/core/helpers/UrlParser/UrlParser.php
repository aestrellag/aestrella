<?php


class UrlParser
{
    private static $_instance = null;

    private $parsedQuery = null;
    private $fullPath = null;
    private $parsedPath = array();

    private function __construct()
    { //Prevent any oustide instantiation of this class
        $this->_init($_SERVER['REQUEST_URI']);
    }

    private function __clone()
    {
    } //Prevent any copy of this object

    private function _path($key)
    {
        return isset($this->parsedPath[$key]) ? $this->parsedPath[$key] : false;
    }

    private function _init($URI){

        try {
            $this->parsedQuery = parse_url($URI);
            $this->fullPath = trim($this->parsedQuery['path'], "/");
            $subRoot = trim(SUB_ROOT, "/");
            $this->fullPath = trim(preg_replace("/^".$subRoot."(.*)/i", "\\1", $this->fullPath), "/");
            $this->parsedPath = explode('/', $this->fullPath);
        } catch (Exception $e) {
            echo "Error while URL Parsing";
        }
    }

    public function getID($key)
    {
        $ID = trim($this->_path($key));
        if(UserHelper::ValidGUID($ID) || UserHelper::ValidHashID($ID)){
            return $ID;
        } elseif (is_numeric($ID) && is_int($ID * 1)) {
            return ''.($ID * 1);
        } elseif (preg_match("/^0(.*)/i", $ID)) {
            return preg_replace("/^0(.*)/i", '\\1', $ID);
        } 
        return false;
    }

    public static function getInstance() // Single globally accessible static method
    {
        if (!is_object(self::$_instance)) {  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
            self::$_instance = new UrlParser();
        }
        return self::$_instance;
    }

    public static function ID($key)
    {
        return UrlParser::getInstance()->getID($key);
    }

    public static function makeID($key)
    {
        return '0' . $key;
    }

    public static function path($key = null)
    {
        return is_null($key) ? UrlParser::getInstance()->fullPath : UrlParser::getInstance()->_path($key);
    }

    public static function pathArray()
    {
        return UrlParser::getInstance()->parsedPath;
    }

    public static function startSubResource($resourceURL = '')
    {
        return UrlParser::getInstance()->_init($resourceURL);
    }

    public static function endSubResource()
    {
        return UrlParser::getInstance()->_init($_SERVER['REQUEST_URI']);
    }

}