<?php


class UserHelper
{
    private static $_instance = null;

    private $LAST_SESSION_GUID = '';
    private $ID = null;
    private $TOKEN = null;
    private $GROUPS = false;
    private $isROOT = false;
    private $isAuthorized = false;
    private $error = array();
    private $validTokenCheckups = array();

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * Generating 60 character crypted passwordd
     * @param string $rawPassword
     * @return bool|string
     */
    private function _cryptPassword($rawPassword = '')
    {
        $crypPassword = password_hash($rawPassword, PASSWORD_BCRYPT);
        return $crypPassword;
    }

    /**
     * Verifying hashed password
     *
     * @param array|string  $userObj
     * @param string $password
     *
     * @return bool
     */
    private function _verifyPassword($userObj = [], $password = '')
    {
        $hash                       = '';

        // find hash
        if(is_array($userObj) && array_key_exists('password', $userObj))
        {
            $hash                   = $userObj['password'];
        }
        elseif(is_string($userObj))
        {
            $hash                   = $userObj;
        }
        else
        {
            return false;
        }

        // detect hash type
        if(strpos($hash, '$') === false)
        {
            // if md5
            return $hash === md5($password);
        }
        else
        {
            // if password_hash
            return password_verify($password, $hash);
        }
    }

    /**
     * Verifying hashed password Creates a password hash with one-way BCRYPT algo. from string
     * @param string $Pwd
     * @param boolean $hashed
     * @return bool
     */
    private function _hashPassword($pwd = '', $hash = false)
    {
        if ($hash) {
            $pwd = md5($pwd);
        }

        return password_hash($pwd, PASSWORD_BCRYPT);;
    }


    /**
     * Parse User IP
     * @return string
     */
    private function _getUserIP()
    {
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else {
            if (getenv('HTTP_X_FORWARDED_FOR')) {
                $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
            } else {
                if (getenv('HTTP_X_FORWARDED')) {
                    $ipaddress = getenv('HTTP_X_FORWARDED');
                } else {
                    if (getenv('HTTP_FORWARDED_FOR')) {
                        $ipaddress = getenv('HTTP_FORWARDED_FOR');
                    } else {
                        if (getenv('HTTP_FORWARDED')) {
                            $ipaddress = getenv('HTTP_FORWARDED');
                        } else {
                            if (getenv('REMOTE_ADDR')) {
                                $ipaddress = getenv('REMOTE_ADDR');
                            } else {
                                $ipaddress = 'UNKNOWN';
                            }
                        }
                    }
                }
            }
        }

        $ipaddress = explode(",", $ipaddress);
        return trim($ipaddress[0]);
    }

    /**
     * check that token is not expired. returns true valid and not expired, or false
     * @param $token
     * @param $ip
     * @return bool
     */
    private function _validToken($token = '', $ip = null)
    {
        if (!$token) {
            return false;
        }

        if (is_null($ip)) {
            $ip = $this->_getUserIP();
        }

        $cacheKey = md5($token.$ip);

        if(isset($this->validTokenCheckups[$cacheKey])){
            $session = $this->validTokenCheckups[$cacheKey];
        }else{

            $time                   = gmdate('Y-m-d H:i:s', time());

            DataBase::getInstance()->exec_query("SELECT * FROM api_sessions 
WHERE api_token = '".addslashes($token)."' AND date_expire >= CAST('$time' as DATETIME)");

            $session = current(DataBase::getInstance()->fetchAll());

            $this->validTokenCheckups[$cacheKey] = $session;
        }

        if (!$session) {
            return false;
        }

        return $session;

    }

    /**
     * generates a random alphanumeric token based on md5 of time and a shuffle
     * @param int $length
     * @return string
     */
    private function _generateApiToken($length = 32)
    {
        $string2randomize = md5(time());

        $length = intval($length);
        if ($length < 1) {
            $length = 32;
        }

        return substr(str_shuffle($string2randomize), rand(0, (strlen($string2randomize) - $length)), $length);
    }

    /**
     * extend the expiration if token is not yet expired. return new seconds left
     * @param $token
     * @param $ip
     * @param string $extension_seconds
     * @return int
     */
    private function _extendSession($token, $extension_seconds = SESSION_TTL, $ip)
    {
        $session = $this->_validToken($token, $ip);
        if (empty($session)) {
            return false;
            //return 0;
        }

        $this->ID = $session['api_users_id'];
        $this->TOKEN = $session['api_token'];

        $Fields = array(
            'date_expire' => gmdate('Y-m-d H:i:s', time() + max($extension_seconds, 0)) // 0 for now (expire, eg log out).
        );

        DataBase::update('api_sessions', $Fields, "`api_token`='" . addslashes($token) . "'");

        return strtotime($Fields['date_expire'])  - strtotime(date('Y-m-d H:i:s'));
    }

    private function _initSession($userObj = array(), $token = '')
    {
        if (!(empty($userObj) || empty($token))) {

            //cancel/expire any current session (will "log other people out" if using same account)//////////////////////////////////////////
            $wherecond = "`api_users_id`='" . $userObj['id'] . "' && (date_expire IS NULL or date_expire > '" . gmdate('Y-m-d H:i:s') . "') && `user_type` = '" . $userObj['userType']."'";//
            DataBase::update('api_sessions', array('date_expire' => gmdate('Y-m-d H:i:s')), $wherecond);                                     //
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //create api token and insert it, with expire date
            $sessionID = UserHelper::CreateGUID();
            $fields = array(
                "id" => $sessionID,
                'api_users_id' => $userObj['id'],
                'api_token' => $token,
                'date_start' => gmdate('Y-m-d H:i:s', time()),
                'date_last_use' => gmdate('Y-m-d H:i:s', time()),
                'date_expire' => gmdate('Y-m-d H:i:s', time() + SESSION_TTL),
                'user_type' => $userObj['userType'],
                'user_ip' => $this->getUserIP(),
                'http_user_agent' => $_SERVER ['HTTP_USER_AGENT']
            );

            if (DataBase::insert('api_sessions', $fields)) {
                $this->LAST_SESSION_GUID = $sessionID;
                $this->ID = $userObj['id'];
                $this->TOKEN = addslashes($token);

                $cacheKey = md5($token.$this->getUserIP());
                unset($this->validTokenCheckups[$cacheKey]);

                return $fields;
            } else {
                throw new Exception('DataBase error with api_sessions', 500);
            }
        } else {
            return false;
        }
    }

    private function _createSession($userObj = array())
    {
        $token = $this->generateApiToken();
        if (!(empty($userObj) || empty($token))) {

            $userInfo = $this->_userInfo($userObj['userType'], $userObj['id'], true);
            if (!$userInfo) {
                return false;
            }

            // if disabled
            if(array_key_exists('active', $userObj) && $userObj['active'] != 1)
            {
                throw new Exception('USER_DISABLED', 402);
            }

            if($userObj['userType'] === 'student')
            {
                // for removed students
                if(!empty($userInfo['account_id_for_removed']))
                {
                    throw new Exception('USER_REMOVED', 404);
                }

                // Check for Limit of students sign-on
                $settings       = $this->_get_account_settings($userInfo['account_id']);

                if(!empty($settings['disable_inactive']))
                {
                    $this->_check_inactive_student($userInfo, $userObj, $settings['disable_inactive']);
                }

                if(!empty($settings['limit_sign_on']))
                {
                    $this->_check_limit_sign_on($userInfo['account_id'], $settings['limit_sign_on'], $userInfo['id']);
                }
            }

            $session = $this->initSession($userObj, $token);
            if (!$session) {
                return false;
            }

            $userInfo['groups'] = $this->_userGroup($userObj['id']);
            $result = array(
                "token" => $token,
                "seconds" => SESSION_TTL,
                "user_type" => $userObj['userType'],
                "user" => $userInfo,
            );

            return $result;
        } else {
            return false;
        }
    }

    private function _apiUserAccess($apiUserId)
    {
        $accessArray = array();
        if(!$apiUserId) return $accessArray;

        if(!empty(current(DataBase::select("`api_user_root`", ['api_users_id' => "'".addslashes($apiUserId)."'"]))))
            $accessArray[] = 'api_user_root';

        if(!empty(current(DataBase::select("`accounts_admins`", ['api_users_id' => "'".addslashes($apiUserId)."'"]))))
            $accessArray[] = 'accounts_admins';


        return $accessArray;
    }

    private function _get_account_settings($accountGUID)
    {
        DataBase::selectFields(array('settings'));

        $account                    = current(DataBase::select("`accounts`", ['id' => "'".addslashes($accountGUID)."'"]));

        if (empty($account)) {
            return [];
        }

        if(empty($account['settings']))
        {
            return [];
        }

        $result                     = json_decode($account['settings'], true);

        if(!is_array($result))
        {
            return [];
        }

        return $result;
    }

    private function _check_limit_sign_on($accountID, $limit, $student_id)
    {
        $now                        = gmdate('Y-m-d H:i:s');

        $query                      = "SELECT COUNT(DISTINCT s.id) as `count` 
FROM `api_sessions` as ss
INNER JOIN students as s ON(s.api_users_id = ss.api_users_id)
WHERE s.account_id = '$accountID' 
AND (ss.date_expire IS NULL OR ss.date_expire > '$now') AND ss.user_type = 'student'
AND s.id != '$student_id'";

        if(!DataBase::getInstance()->exec_query($query))
        {
            throw new Exception('DataBase error', 500);
        }

        $result                     = DataBase::getInstance()->fetchAll();

        if(empty($result) || empty($result[0]['count']))
        {
            return;
        }

        if($result[0]['count'] >= $limit)
        {
            throw new Exception('The limit of number sign-on of same account on the system exhausted', 403);
        }
    }

    private function _check_inactive_student(array $student, array $profile, $limit_days)
    {
        $time                       = strtotime("-$limit_days day");
        $date                       = gmdate('Y-m-d H:i:s', $time);

        $query                      = "SELECT COUNT(DISTINCT ss.api_users_id) as `count` 
FROM `api_sessions` as ss
WHERE  ss.api_users_id = '{$profile['id']}'
AND ss.date_last_use > '$date' AND ss.user_type = 'student'";

        if(!DataBase::getInstance()->exec_query($query))
        {
            throw new Exception('DataBase error', 500);
        }

        $result                     = DataBase::getInstance()->fetchAll();

        // If not found any activity
        if(!empty($result) && !empty($result[0]['count']) && $result[0]['count'] > 0)
        {
            return;
        }

        if(empty($student['activate_date']))
        {
            $now                    = gmdate('Y-m-d H:i:s');
            DataBase::getInstance()->exec_query("UPDATE `students` SET `activate_date` = '$now' WHERE `id` = '{$student['id']}'");
            return;
        }

        if(strtotime($student['activate_date']) > $time)
        {
            return;
        }

        // disable student
        DataBase::getInstance()->exec_query("UPDATE `students` SET `active` = 0 WHERE `id` = '{$student['id']}'");

        throw new Exception('USER_DISABLED', 402);
    }

    /**
     * logs out by token
     * @param null $token
     * @return bool
     */
    private function _logOut($token = null)
    {
        if (is_null($token)) {
            return false;
        }

        $session = current(DataBase::select('api_sessions', array(
            'api_token' => "'" . addslashes($token) . "'"
        )));

        if ($session['id']) {
            $r = DataBase::update("`api_sessions`", array("date_expire" => gmdate('Y-m-d H:i:s', time())),
                " `id`='" . $session['id']."'");
             $cacheKey = md5($token.$this->getUserIP());
            unset($this->validTokenCheckups[$cacheKey]);
        }

        if ($r) {
            return true;
        } else {
            return false;
        }
    }

    private function _userGroup($userGUID = '')
    {

        if ($this->_validGUID($userGUID)) {
            $userGroupa = DataBase::select('api_users_groups', array(
                'api_users_id' => "'" . $userGUID . "'"
            ));
            if (empty($userGroupa)) {
                return false;
            }

            $groupID = array();
            foreach ($userGroupa as $userGroup) {
                $groupID[] = $userGroup['api_groups_id'];
            }

            $condition = array(
                'id' => "IN,('" . implode("','", $groupID) . "')",
            );

            $groups = DataBase::select('api_groups', $condition, '', false);

            if (in_array('root', $groups)) {
                $this->isROOT = true;
            }

            $this->GROUPS = $groups;

            return $groups;
        }
        return false;
    }

    private function _userInfo($userType = '', $guid = '', $showGUID = false)
    {
        global $USER_TYPES_TABLES;

        if (array_key_exists($userType, $USER_TYPES_TABLES) && $this->_validGUID($guid)) {

            $condition['api_users_id'] = "'" . $guid . "'";
            DataBase::selectFields($USER_TYPES_TABLES[$userType]['fields']);

            $info = current(DataBase::select($USER_TYPES_TABLES[$userType]['table'], $condition));
            if (!empty($info)) {
                if (!$userType == 'root') {
                    $this->isROOT = true;
                }

                if (!$showGUID) {
                    unset($info['id']);
                }
                return $info;
            }
        }
        return null;
    }

    public function getUserInfo($api_user_id = null)
    {
        if (is_null($this->TOKEN))
        {
            $this->error['code']    = 499;
            $this->error['message'] = 'Token required';

            return null;
        }

        if(is_null($api_user_id)){
            $session                = current
            (
                DataBase::select('`api_sessions`', [
                        'api_token' => "'" . addslashes($this->TOKEN) . "'"
                    ])
            );
        }else{
            $api_user_id            = DataBase::getInstance()->quote($api_user_id);
            $session                = current
            (
                DataBase::select('`api_sessions`', [
                        'api_users_id' => $api_user_id
                    ])
            );
        }

        if(empty($session['api_users_id']))
        {
            $this->error['code']    = 498;
            $this->error['message'] = 'Token expired/invalid';

            return null;
        }

        return $this->_userInfo($session['user_type'], $session['api_users_id']);
    }

    private function _getCurrentUserInfo()
    {
        if (is_null($this->TOKEN))
        {
            $this->error['code']    = 499;
            $this->error['message'] = 'Token required';

            return null;
        }

        $session                    = current
        (
            DataBase::select('`api_sessions`',
            array(
                'api_token'         => "'" . addslashes($this->TOKEN) . "'"
            ))
        );

        if(empty($session['api_users_id']))
        {
            $this->error['code']    = 498;
            $this->error['message'] = 'Token expired/invalid';

            return null;
        }

        return $this->_userInfo($session['user_type'], $session['api_users_id']);
    }

    private function _getCurrentUserName()
    {
        $api_guid                   = $this->_getCurrentUserApiGUID();

        $result                     = current(DataBase::select('`api_users`',
                    [
                        'id'         => "'" . $api_guid . "'"
                    ]));

        if(!is_array($result))
        {
            return null;
        }

        return $result['username'];
    }

    private function _accessLevel($access_levels = null)
    {

        if (!is_null($access_levels)) {
            $session = UserHelper::ValidToken(UserHelper::Token());
            if (!is_null(UserHelper::Token())) {
                if ($session) {
                    UserHelper::isAuthorized(true);
                    if($session['user_type'] == 'root')
                        $this->isROOT = true;
                } else {
                    $this->error['code'] = 498;
                    $this->error['message'] = 'Token expired/invalid';
                    return false;
                }
            } else {
                $this->error['code'] = 499;
                $this->error['message'] = 'Token required';
                return false;
            }

            if (!$this->isROOT) {
                // if the user is not ROOT and if access levels conditions is NULL then return everyone has access OR
                // access levels conditions are passed but the access levels conditions is not an array then return an error.
                if (is_array($access_levels) && empty($access_levels)) {
                    $this->error['code'] = 403;
                    $this->error['message'] = "You don't have permission to access this resource [" . UrlParser::path() . "] on this server.";
                    return false;
                }

                //if the current usertype is not mentioned in the access levels, no access
                return in_array($session['user_type'], $access_levels);

            } else {
                return true;
            }
            return false;

        }else{
            return true;
        }

    }

    private function _userType(){
        if ($this->isROOT) {
            return "root";
        } else {
            if (is_null($this->TOKEN)) {
                return '';
            }

            $cacheKey = md5($this->TOKEN.$this->_getUserIP());

            if(isset($this->validTokenCheckups[$cacheKey])){
                $session = $this->validTokenCheckups[$cacheKey];
            }else{
                $session = current(DataBase::select('`api_sessions`',
                array(
                    'api_token' => "'" . addslashes($this->TOKEN) . "'"
                )));
            }

            $type = $session['user_type'];
            return $type;
        }
    }

    private function _createGUID()
    {
        return strtolower(sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535),
            mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535),
            mt_rand(0, 65535)));
    }

    private function _validGUID($guid)
    {
        return preg_match('/^\{?[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}\}?$/', $guid);
    }

    private function _validHashID($hashid)
    {
        if(strlen($hashid) < 32) #for speed no regex needed
            return false;

        $valid = array('/^[a-f0-9]{32}$/', '/^[a-f0-9]{40}$/', '/^[a-f0-9]{64}$/');
        foreach ($valid as $reg)
            if(preg_match($reg, $hashid))
                return true;

        return false;
    }

    private function _getCurrentUserApiGUID()
    {
        if (is_null($this->TOKEN))
        {
            $this->error['code']    = 499;
            $this->error['message'] = 'Token required';

            return null;
        }

        $session                    = current
        (
            DataBase::select('`api_sessions`',
            array(
                'api_token'         => "'" . addslashes($this->TOKEN) . "'"
            ))
        );

        if(empty($session['api_users_id']))
        {
            $this->error['code']    = 498;
            $this->error['message'] = 'Token expired/invalid';

            return null;
        }

        return $session['api_users_id'];
    }

    private function _getUserGUIDbyApiSecret($apiSecret)
    {
        $user                    = current
        (
            DataBase::select('`api_users`',
                array(
                    'api_secret'         => "'" . $apiSecret . "'"
                ))
        );

        if(empty($user['id']))
        {
            return null;
        }

        return $user['id'];
    }

    public static function getInstance()
    {
        if (!is_object(self::$_instance)) {
            self::$_instance = new UserHelper();
        }
        return self::$_instance;
    }

    public static function CryptPassword($rawPassword)
    {
        return UserHelper::getInstance()->_cryptPassword($rawPassword);
    }

    public static function CryptNationalId($national_id)
    {
        if($national_id === '')
        {
            return '';
        }

        return md5($national_id);
    }

    public static function SanitizeNationalId($national_id): string
    {
        return preg_replace('/\W/i', '', $national_id);
    }

    public static function VerifyPassword($userObj, $hashedPwd)
    {
        return UserHelper::getInstance()->_verifyPassword($userObj, $hashedPwd);
    }

    public static function HashPassword($pwd, $hash)
    {
        return UserHelper::getInstance()->_hashPassword($pwd, $hash);
    }

    public static function GetUserIP()
    {
        return UserHelper::getInstance()->_getUserIP();
    }

    public static function ValidToken($token, $ip = null)
    {
        return UserHelper::getInstance()->_validToken($token, $ip);
    }

    public static function GenerateApiToken($length = 32)
    {
        return UserHelper::getInstance()->_generateApiToken($length);
    }

    public static function ExtendSession($token = '', $extension_seconds = SESSION_TTL, $ip = '')
    {
        if (empty($ip)) {
            $ip = self::GetUserIP();
        }
        return UserHelper::getInstance()->_extendSession($token, $extension_seconds, $ip);
    }

    public static function InitSession($userObj, $token)
    {
        return UserHelper::getInstance()->_initSession($userObj, $token);
    }

    public static function CreateSession($userObj)
    {
        return UserHelper::getInstance()->_createSession($userObj);
    }
    public static function ApiUserAccess($apiUserId)
    {
        return UserHelper::getInstance()->_apiUserAccess($apiUserId);
    }

    public static function LogOut($token = '')
    {
        return UserHelper::getInstance()->_logOut($token);
    }

    public static function UserGroup($userGUID = '')
    {
        return UserHelper::getInstance()->_userGroup($userGUID);
    }

    public static function AccessLevel($access_levels = null)
    {
        return UserHelper::getInstance()->_accessLevel($access_levels);
    }

    public static function Info($userType = '', $guid = '', $showGUID = false)
    {
        return UserHelper::getInstance()->_userInfo($userType, $guid, $showGUID);
    }

    public static function CreateGUID()
    {
        return UserHelper::getInstance()->_createGUID();
    }

    public static function ValidGUID($guid = '')
    {
        return UserHelper::getInstance()->_validGUID($guid);
    }

    public static function ValidHashID($hashid = '')
    {
        return UserHelper::getInstance()->_validHashID($hashid);
    }

    public static function getCurrentUserApiGUID()
    {
        return UserHelper::getInstance()->_getCurrentUserApiGUID();
    }

    public static function getCurrentUserName()
    {
        return UserHelper::getInstance()->_getCurrentUserName();
    }

    public static function IsRoot($bool = null)
    {
        if (is_null($bool)) {
            return UserHelper::getInstance()->isROOT;
        } else {
            UserHelper::getInstance()->isROOT = (boolean)$bool;
        }
    }

    public static function Error()
    {
        return UserHelper::getInstance()->error;
    }

    public static function LastSessionGUID($id = null)
    {
        if (is_null($id)) {
            if(UserHelper::getInstance()->LAST_SESSION_GUID)
                return UserHelper::getInstance()->LAST_SESSION_GUID;
            else{
                $sessionArray = UserHelper::getInstance()->_validToken(UserHelper::Token());
                return $sessionArray['id'];
            }
        } else {
            UserHelper::getInstance()->LAST_SESSION_GUID = (string)$id;
        }
    }

    public static function Token($token = null)
    {
        if (is_null($token)) {
            return UserHelper::getInstance()->TOKEN;
        } else {
            UserHelper::getInstance()->TOKEN = (string)$token;
        }
    }

    public static function isAuthorized($isAuthorized = null)
    {
        if (is_null($isAuthorized)) {
            return UserHelper::getInstance()->isAuthorized;
        } else {
            UserHelper::getInstance()->isAuthorized = filter_var($isAuthorized, FILTER_VALIDATE_BOOLEAN);
        }
    }

    public static function UserType($type = null)
    {
        if(!is_null($type))
            return UserHelper::getInstance()->_userType() == (string)$type;
        return UserHelper::getInstance()->_userType();
    }

    public static function getUserGUIDbyApiSecret($apiSecret = '')
    {
        return UserHelper::getInstance()->_getUserGUIDbyApiSecret($apiSecret);
    }

    public static function checkAccessToGroup($needleGroup, $api_user_id = null)
    {

        if(!is_array($needleGroup))
        {
            $needleGroup            = explode(',', $needleGroup);
        }

        if(is_null($api_user_id))
        {
            $api_user_id            = self::getInstance()->_getCurrentUserApiGUID();
        }

        $groups                     = self::UserGroup($api_user_id);

        foreach ($groups as $group)
        {
            if(in_array($group['code'], $needleGroup) === true)
            {
                return true;
            }
        }

        return false;

    }

    /**
     * Check access user to manager portal
     * @param Api $api object instance of api class
     * @param $portal_id
     * @param null $api_user_id
     * @return bool
     */
    public static function checkUserPortalManager($api, $portal_id, $api_user_id = null)
    {

        $portalConfig           = $api->hookClass('v2/portals')->getPortalConfig($portal_id);
        $defaultAccountId       = $portalConfig['accountId'] ?? false;

        if(is_null($api_user_id)){
            $api_user_id        = self::getCurrentUserApiGUID();
        }

        $userInfo               = self::getInstance()->getUserInfo($api_user_id);

        if($defaultAccountId && $defaultAccountId === $userInfo['account_id']) {

            if (UserHelper::checkAccessToGroup('master,superAdmin', $api_user_id)) {
                return true;
            }
        }

        return false;

    }

    public function checkAccessToAccount($accountId){

        //root have access to any account
        if($this->accessLevel(['root'])){
            return true;
        }

        $currentUserInfo            = $this->_getCurrentUserInfo();

        if($currentUserInfo['account_id'] === $accountId){
            return true;
        }

        return false;
    }
}
