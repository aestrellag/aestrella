<?php
error_reporting(E_ERROR);
header("Access-Control-Allow-Origin: *");
$timeStart = time() + microtime();

if (file_exists('override.config.php'))//takes < 1 ms, but for max speed you can make local modification to just include file without check
{
    require_once('override.config.php');
    //echo "override is in the house";
}
else{

    echo "no override";
}

try
{

    require_once('autoload.php');
    require_once('config.php');
    require_once('sys.config.php');
    require_once(SYS_CLASSES_PATH . '/_database_handler.php');
    require_once(SYS_CLASSES_PATH . '/helpers/HelpersLoader.php');

    if(defined("DB_V2_LOGDB") && DB_V2_LOGDB){
        define("API_REQUEST_GUID", UserHelper::CreateGUID());
        $sql = "INSERT INTO `".DB_V2_LOGDB."`.`api_sys_log` (`id`,`method`,`resource`,`params`,`date`,`response_code`,`user_ip`) VALUES ('".API_REQUEST_GUID."','".strval($_SERVER['REQUEST_METHOD'])."','".strval(UrlParser::path())."','".addslashes(json_encode(DataParser::getArray()))."','".date("Y-d-m H:i:s",time())."','0','".UserHelper::GetUserIP()."')";
        Logger::writeApiLogFile($sql."\n----\n");
    }

    $version = UrlParser::path(0);
    if (array_key_exists($version, $API_VERSION)) {
        require_once(SYS_CLASSES_PATH . '/' . $API_VERSION[$version]);
    } else {
        require_once(SYS_CLASSES_PATH . '/_api.php');
    }

    $api = new api(UrlParser::path(), DataParser::getArray(), false, $timeStart, false, null);

    echo $api->OUTPUT;

}
catch(\Throwable $exception)
{
    if(defined('DEBUG_EXCEPTION'))
    {
        $data                       = [
            'message'               => $exception->getMessage(),
            'line'                  => $exception->getLine(),
            'file'                  => $exception->getFile(),
            'trace'                 => $exception->getTrace()
        ];

        $response                   = ['code' => 500, 'status' => 'Unhandled exception', 'exception' => $data];

        header("Status: 500 Server unhandled exception");
        header("Content-Type:" . $api->CONTENT_TYPE . "; charset=" . $api->ENCODING);
        header("Access-Control-Allow-Origin: *");
        echo json_encode($data, JSON_PRETTY_PRINT);
    }
}