<?php
//
//      GET /v2/courses/:courseID/chapters
//

if ($this->accessLevel(null)) { // note: accessLevel() means anyone.  accessLevel(array()) means no one, except root

    // hook up needed classes ///////
    $this->hookClass('v2/chapters');//
    /////////////////////////////////

    // get ID's from the URL //////
    $courseID = UrlParser::ID(2);//
    ///////////////////////////////

    // get response data /////////////////////////////////////////////////////////////////////
    $this->RESPONSE = $this->class->chapters->getCourseChaptersByID($courseID, $this->DATA);//
    $this->HTTP_CODE = 200;
    if(is_null($this->RESPONSE)){
    	$this->HTTP_CODE = 404;
    }
    //////////////////////////////////////////////////////////////////////////////////////////
}
