<?php
//https://knowledgecity.atlassian.net/wiki/display/KCAPI/GET+All+Courses+v2
// authors: Samvel Budumyan
//      GET /v2/courses
//

if ($this->accessLevel(null)) { // note: accessLevel() means anyone.  accessLevel(array()) means no one, except root

    // hook up needed classes ///////
    $this->hookClass('v2/courses');//
    /////////////////////////////////
    
    // get response data /////////////////////////////////////////////////
    $this->RESPONSE = $this->class->courses->getAllCourses($this->DATA);//
    $this->HTTP_CODE = 200;
    //////////////////////////////////////////////////////////////////////

}