<?php
date_default_timezone_set('America/Los_Angeles');
date_default_timezone_set('UTC');

@define("NESTED_KEYVALUE_DELIMITER", ">");
@define("NESTED_KEY_DELIMITER", "/");
@define("NESTED_VALUE_DELIMITER", "/");

if (!isset($NESTED_RULES)) {
    $NESTED_RULES = file('core/nestedrules.txt');
}

//////////////////////////////////////////////////////////////////////
// map of tokens used in nested rules to the table
// that has the matching api_secret, and the field
// containing the id value that the token refers to
// For Example on {:selfCompanyId:} we don't use
// accounting_accounts.id because this would not have
// the --> api_secret <-- that is unique to the user.
// There can be multiple admins. They are just users
// marked with is_admin
//////////////////////////////////////////////////////////////////////
if (!isset($NESTED_RULES_TOKENS)) {
    $NESTED_RULES_TOKENS = array(
        '{:selfId:}' => 'lms_users.id',
        '{:selfCompanyId:}' => 'lms_users.company_idFK',
        '{:selfSubscriptionId:}' => 'lms_users.subscription_idFK',
        '{:repId:}' => 'accounting_reps.id',
        '{:affiliateId:}' => 'accounting_reps.company_id',
        '{:libraryId:}' => 'library_info.LID'
    );
}

//////////////////////////////////////////////////////////////////////
// the user's token will match a session with one of these
// user types.  The type is written into the session when
// they log in.  They log in specifying "account", "affiliate",
// "siteAdmin". The authentication code may or may not transform the
// userType to be more specific, eg accountUser or accountAdmin.
// The value is the table that should contain the api_secret matching
// the session's api_secret
//////////////////////////////////////////////////////////////////////
if (!isset($AUTH_ALLOWED_TYPES)) {
    $AUTH_ALLOWED_TYPES = array(
        'accountUser' => 'lms_users',
        'accountAdmin' => 'lms_users',
        'affiliateUser' => 'accounting_reps',
        'affiliateAdmin' => 'accounting_reps',
        'siteAdmin' => 'api_users',
        'libraryAdmin' => 'library_info',
        'productAdmin' => 'api_users',
        'root' => 'api_users',
        'general_api_user' => 'api_users',
    );
}

////////
// V2
////////

if (!isset($USER_TYPES_TABLES)) {
    $USER_TYPES_TABLES = array(
        'root' => array("table" => "api_user_root","fields" => array("first_name","last_name")),
        'affiliateUser' => array("table" => "rep_account_users","fields" => array("first_name","last_name")),
        'accountAdmin' => array("table" => "accounts_admins","fields" => array("first_name","last_name","title","account_id")),
        'student' => array("table" => "students","fields" => array("id","account_id","account_id_for_removed","first_name","last_name","email","gender","national_id","national_id_end","employee_id","company","department","position","phone_number","lang","extra","activate_date","first_login","is_test","isTna","isNotice")),
    );
    $USER_TYPES_TABLES['accountUser'] = $USER_TYPES_TABLES['student'];
}

//////////////////////////////////////////////////////////////////////
// for each usertype being requested, this specifies the field
// containing the "username". It is assumed there's a field called
// "password" in all cases
//////////////////////////////////////////////////////////////////////
if (!isset($AUTH_BY_LOGIN)) {
    $AUTH_BY_LOGIN = array(
        'accountUser' => 'email',
        'accountAdmin' => 'email',
        'affiliateUser' => 'email',
        'affiliateAdmin' => 'email',
        'siteAdmin' => 'username',
        'libraryAdmin' => 'contact_email',
        'productAdmin' => 'email',
        'root' => 'username',
        'general_api_user' => 'username',
    );
}


@define("KNCEMAIL_FEEDBACK", "CourseFeedback-US@knowledgecity.com");
@define("KNCEMAIL_CONTACTUS", "Contact-US@knowledgecity.com");
@define("KNCEMAIL_CUSTOMERSERVICE", "CustomerService@knowledgecity.com");
@define("KNCEMAIL_INFO", "info@knowledgecity.com");
@define("QUIZ_PASS_SCORE", 80);
@define("QUIZ_SEC_PER_QUESTION", 60);
@define("QUIZ_SEC_PER_QUIZ", 600);
@define("QUIZ_SEC_GRACE_PERIOD", 60);

@define("TNA_COURSE_ASSIGN_TOTAL", 10);
@define("TNA_COURSE_ASSIGN_BUS", 4);
@define("TNA_COURSE_ASSIGN_CMP", 2);
@define("TNA_COURSE_ASSIGN_SAF", 1);

// subscriptions
@define("SUBSCRIPTION_PRICE_MONTH", 14.99);
@define("SUBSCRIPTION_PRICE_YEAR", 119.88);
@define("EXTRA_END_DATE_DAYS", 7); //

@define("TRIAL_DURATION", 11);
@define("TRIAL_LANDING_DURATION", 90);
@define("SESSION_DURATION", 840); //seconds before token expires if not refreshed

@define("SUBSCRIBE_COUNTRIES", "US,MX,AM");


//@define("PAYPAL_SANDBOX_MODE", true);
//@define('PAYPAL_SANDBOX_USERNAME', 'james.m_api1.knowledgecity.com');
//@define('PAYPAL_SANDBOX_PASSWORD', '4P53PHRUX9YRHBGL');
//@define('PAYPAL_SANDBOX_SIGNATURE', 'AwTHF4vxwMHcFRHRB-Ac9kBCAdhrA-NU0jj1telZy1jsNlozzzn.dFdt');

// sandbox credit card
// cc   4032030738106765
// type VISA
// exp  102020

//vasily's old (gone) sandbox
//@define('PAYPAL_SANDBOX_USERNAME', 'vav802-facilitator_api1.gmail.com');
//@define('PAYPAL_SANDBOX_PASSWORD', '1396863619');
//@define('PAYPAL_SANDBOX_SIGNATURE', 'ANMKeZQc8QdfNjAtybJtcJn00SyZAn0YNifd8yY0Lc.2XejXWXMfjGJG');

//these are not real live credentials.  Define your live credentials in override.config.php
@define('PAYPAL_LIVE_USERNAME', 'eventreceipts_api1.knowledgeocity.com');
@define('PAYPAL_LIVE_PASSWORD', 'W5JD55YFZ8DG8EHM');
@define('PAYPAL_LIVE_SIGNATURE', 'AFcWxV21C7fd0v3bYYYRCpSSRl31AVVVUew9fV6ZvQPF2KUGAlmzkQEC');
//https://developer.paypal.com/docs/classic/api/errorcodes/
